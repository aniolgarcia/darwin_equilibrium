#ifndef _DARWIN_KINEMATICS_H
#define _DARWIN_KINEMATICS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx.h"

#define DARWIN_PELVIS_LENGTH               29.0 //mm
#define DARWIN_LEG_SIDE_OFFSET             37.0 //mm
#define DARWIN_THIGH_LENGTH                93.0 //mm
#define DARWIN_CALF_LENGTH                 93.0 //mm
#define DARWIN_ANKLE_LENGTH                33.5 //mm
#define DARWIN_LEG_LENGTH                  (DARWIN_THIGH_LENGTH + DARWIN_CALF_LENGTH + DARWIN_ANKLE_LENGTH) //mm (THIGH_LENGTH + CALF_LENGTH + ANKLE_LENGTH)

// public functions
uint8_t darwin_leg_ik(float *out, float x, float y, float z, float a, float b, float c);

#ifdef __cplusplus
}
#endif

#endif
