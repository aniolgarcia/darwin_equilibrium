#ifndef _DARWIN_MATH_H
#define _DARWIN_MATH_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx.h"

#define   PI    3.14159

typedef struct{
  float x;
  float y;
  float z;
}TPoint3D;

// point public functions
void point3d_init(TPoint3D *point);
void point3d_load(TPoint3D *point, float x, float y, float z);
void point3d_copy(TPoint3D *point_dst, TPoint3D *point_src);

typedef struct {
  float x;
  float y;
  float z;
}TVector3D;

// vector public functions
void vector3d_init(TVector3D *vector);
void vector3d_load(TVector3D *vector, float x, float y, float z);
void vector3d_copy(TVector3D *vector_dst, TVector3D *vector_src);
float vector3d_length(TVector3D *vector);

enum{
  m00=0,
  m01,
  m02,
  m03,
  m10,
  m11,
  m12,
  m13,
  m20,
  m21,
  m22,
  m23,
  m30,
  m31,
  m32,
  m33,
};

typedef struct{
  float coef[16];
}TMatrix3D;

// matrix public functions
void matrix3d_init(TMatrix3D *matrix);
void matrix3d_copy(TMatrix3D *matrix_dst, TMatrix3D *matrix_src);
void matrix3d_identity(TMatrix3D *matrix);
uint8_t matrix3d_inverse(TMatrix3D *org, TMatrix3D *inv);
void matrix3d_set_transform(TMatrix3D *matrix, TPoint3D *point, TVector3D *vector);
void matrix3d_mult(TMatrix3D *dst,TMatrix3D *a,TMatrix3D *b);

#ifdef __cplusplus
}
#endif

#endif
