#include "darwin_kinematics.h"
#include "darwin_math.h"
#include <math.h>

uint8_t darwin_leg_ik(float *out, float x, float y, float z, float a, float b, float c) //agafem la posicio i orientació (en radians?)
{
  TMatrix3D Tad, Tda, Tcd, Tdc, Tac; //Declarem matrius 4x4
  TVector3D vec,orientation; //Declarem vectors de R3
  TPoint3D position; //Declarem un punt de R3
  float _Rac, _Acos, _Atan, _k, _l, _m, _n, _s, _c, _theta;

  vector3d_load(&orientation,a * 180.0 / PI, b * 180.0 / PI, c * 180.0 / PI);  
  point3d_load(&position,x, y, z - DARWIN_LEG_LENGTH);
  matrix3d_set_transform(&Tad,&position,&orientation);

  vec.x = x + Tad.coef[2] * DARWIN_ANKLE_LENGTH;
  vec.y = y + Tad.coef[6] * DARWIN_ANKLE_LENGTH;
  vec.z = (z - DARWIN_LEG_LENGTH) + Tad.coef[10] * DARWIN_ANKLE_LENGTH;

  // Get Knee
  _Rac = vector3d_length(&vec);
  _Acos = acos((_Rac * _Rac - DARWIN_THIGH_LENGTH * DARWIN_THIGH_LENGTH - DARWIN_CALF_LENGTH * DARWIN_CALF_LENGTH) / (2 * DARWIN_THIGH_LENGTH * DARWIN_CALF_LENGTH));
  if(isnan(_Acos) == 1)
    return 0x00;;
  *(out + 3) = _Acos;

  // Get Ankle Roll
  Tda = Tad;
  if(matrix3d_inverse(&Tda,&Tda) == 0x00)
    return 0x00;
  _k = sqrt(Tda.coef[7] * Tda.coef[7] + Tda.coef[11] * Tda.coef[11]);
  _l = sqrt(Tda.coef[7] * Tda.coef[7] + (Tda.coef[11] - DARWIN_ANKLE_LENGTH) * (Tda.coef[11] - DARWIN_ANKLE_LENGTH));
  _m = (_k * _k - _l * _l - DARWIN_ANKLE_LENGTH * DARWIN_ANKLE_LENGTH) / (2 * _l * DARWIN_ANKLE_LENGTH);
  if(_m > 1.0)
    _m = 1.0;
  else if(_m < -1.0)
    _m = -1.0;
  _Acos = acos(_m);
  if(isnan(_Acos) == 1)
    return 0x00;
  if(Tda.coef[7] < 0.0)
    *(out + 5) = -_Acos;
  else
    *(out + 5) = _Acos;
  // Get Hip Yaw
  vector3d_load(&orientation,*(out + 5) * 180.0 / PI, 0, 0);
  point3d_load(&position,0, 0, -DARWIN_ANKLE_LENGTH);
  matrix3d_set_transform(&Tcd,&position,&orientation);
  Tdc = Tcd;
  if(matrix3d_inverse(&Tdc,&Tdc) == 0x00)
    return 0x00;
  matrix3d_mult(&Tac,&Tad,&Tdc);
  _Atan = atan2(-Tac.coef[1] , Tac.coef[5]);
  if(isinf(_Atan) == 1)
    return 0x00;
  *(out) = _Atan;

  // Get Hip Roll
  _Atan = atan2(Tac.coef[9], -Tac.coef[1] * sin(*(out)) + Tac.coef[5] * cos(*(out)));
  if(isinf(_Atan) == 1)
    return 0x00;
  *(out + 1) = _Atan;

  // Get Hip Pitch and Ankle Pitch
  _Atan = atan2(Tac.coef[2] * cos(*(out)) + Tac.coef[6] * sin(*(out)), Tac.coef[0] * cos(*(out)) + Tac.coef[4] * sin(*(out)));
  if(isinf(_Atan) == 1)
    return 0x00;
  _theta = _Atan;
  _k = sin(*(out + 3)) * DARWIN_CALF_LENGTH;
  _l = -DARWIN_THIGH_LENGTH - cos(*(out + 3)) * DARWIN_CALF_LENGTH;
  _m = cos(*(out)) * vec.x + sin(*(out)) * vec.y;
  _n = cos(*(out + 1)) * vec.z + sin(*(out)) * sin(*(out + 1)) * vec.x - cos(*(out)) * sin(*(out + 1)) * vec.y;
  _s = (_k * _n + _l * _m) / (_k * _k + _l * _l);
  _c = (_n - _k * _s) / _l;
  _Atan = atan2(_s, _c);
  if(isinf(_Atan) == 1)
    return 0x00;
  *(out + 2) = _Atan;
  *(out + 4) = _theta - *(out + 3) - *(out + 2);

  return 0x01;
}
