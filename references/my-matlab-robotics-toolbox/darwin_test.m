%%
clc;
clear all;
close all;

%% Graphic
g = ncgr_graphic();

%% Darwin Right Leg

global N_DOFS;
N_DOFS = 6;

theta = [0 0 0 0 0 0];
alpha = [0 -pi/2 -pi/2 pi/2 0 pi/2]; %començant pel 1
offset = [0 0 -pi/2 0 0 0]; 
a = [0 0 0 -0.93 -0.93 0]; %comença a 0
d = [0 0 0 0 0 0];
type = ['r','r','r','r','r','r'];
base = [0; 0; 0];
lb = [-pi; -pi; -pi; -pi; -pi; -pi];
ub = [pi; pi; pi; pi; pi; pi];

dar = cgr_create(theta, d, a, alpha, offset, type, base, ub, lb);
dar = cgr_self_update(dar, [0; 0; 0; 0; 1.2; 0]); %comença a 
g = ncgr_plot(g, dar);
