/*
 * xrot.h
 *
 * Code generation for function 'xrot'
 *
 */

#ifndef XROT_H
#define XROT_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void b_xrot(real_T x[12], int32_T ix0, int32_T iy0, real_T c, real_T s);
extern void xrot(real_T x[9], int32_T ix0, int32_T iy0, real_T c, real_T s);

#endif

/* End of code generation (xrot.h) */
