/*
 * cgr_ikine1.c
 *
 * Code generation for function 'cgr_ikine1'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "norm.h"
#include "pinv.h"
#include "cgr_create_mex_data.h"

/* Variable Definitions */
static emlrtRSInfo b_emlrtRSI = { 19,  /* lineNo */
  "cgr_ikine1",                        /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_ikine1.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 21,  /* lineNo */
  "cgr_ikine1",                        /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_ikine1.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 30,  /* lineNo */
  "cgr_ikine1",                        /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_ikine1.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 34,  /* lineNo */
  "cgr_ikine1",                        /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_ikine1.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 35,  /* lineNo */
  "cgr_ikine1",                        /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_ikine1.m"/* pathName */
};

/* Function Definitions */
void cgr_ikine1(const emlrtStack *sp, const struct0_T *r, const real_T p[3],
                real_T treshold, real_T max_iter, real_T q[4], real_T
                *iter_taken, real_T *err)
{
  int32_T i;
  struct0_T b_r;
  real_T T[64];
  real_T x[3];
  real_T c_r[4];
  real_T jac[12];
  int32_T exitg1;
  real_T a[12];
  real_T delta_x[3];
  int32_T k;
  real_T d0;
  real_T unusedU0[9];
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;

  /*  Using pseudo-inverse method */
  /*  https://groups.csail.mit.edu/drl/journal_club/papers/033005/buss-2004.pdf */
  /*  See Equ. 7. */
  /*  r is the sructure of the robot. */
  /*  p (3x1) is the target cartesian position; */
  /*  */
  /*  THIS DOES NOT CHANGE STRUCTURE OF THE ROBOT! */
  /*  Get current pose. */
  for (i = 0; i < 4; i++) {
    q[i] = r->qc[i];
  }

  b_r = *r;
  st.site = &b_emlrtRSI;
  cgr_fkine(&st, &b_r, r->qc, T);
  for (i = 0; i < 3; i++) {
    x[i] = T[60 + i];
  }

  for (i = 0; i < 4; i++) {
    c_r[i] = r->qc[i];
  }

  st.site = &c_emlrtRSI;
  cgr_jac(&st, r, c_r, jac);
  *iter_taken = 1.0;
  do {
    exitg1 = 0;
    (*iter_taken)++;
    for (i = 0; i < 3; i++) {
      delta_x[i] = p[i] - x[i];
    }

    st.site = &d_emlrtRSI;
    pinv(&st, jac, a);
    for (k = 0; k < 4; k++) {
      d0 = 0.0;
      for (i = 0; i < 3; i++) {
        d0 += a[k + (i << 2)] * delta_x[i];
      }

      c_r[k] = muDoubleScalarMax(q[k] + d0, r->lb[k]);
    }

    for (k = 0; k < 4; k++) {
      q[k] = muDoubleScalarMin(r->ub[k], c_r[k]);
    }

    /*  This is a saturation function. */
    st.site = &e_emlrtRSI;
    cgr_fkine_ee(&st, r, q, unusedU0, x);
    for (i = 0; i < 4; i++) {
      c_r[i] = q[i];
    }

    st.site = &f_emlrtRSI;
    cgr_jac(&st, r, c_r, jac);
    *err = norm(delta_x);
    if (*err < treshold) {
      exitg1 = 1;
    } else {
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }

      if (*iter_taken > max_iter) {
        exitg1 = 1;
      }
    }
  } while (exitg1 == 0);

  /* fprintf('**cgr_ikine1** breaks after %i iterations with errror %f.\n', k, err); */
}

/* End of code generation (cgr_ikine1.c) */
