/*
 * cgr_fkine.h
 *
 * Code generation for function 'cgr_fkine'
 *
 */

#ifndef CGR_FKINE_H
#define CGR_FKINE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void cgr_fkine(const emlrtStack *sp, struct0_T *r, const real_T q[4],
                      real_T T[64]);

#endif

/* End of code generation (cgr_fkine.h) */
