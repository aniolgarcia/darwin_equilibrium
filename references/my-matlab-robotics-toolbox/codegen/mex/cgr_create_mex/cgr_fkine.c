/*
 * cgr_fkine.c
 *
 * Code generation for function 'cgr_fkine'
 *
 */

/* Include files */
#include <string.h>
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "cgr_create_mex_data.h"

/* Function Definitions */
void cgr_fkine(const emlrtStack *sp, struct0_T *r, const real_T q[4], real_T T
               [64])
{
  real_T temp[16];
  int32_T k;
  real_T ct;
  real_T st;
  real_T ca;
  int32_T i0;
  real_T sa;
  real_T b_ct[16];
  static const int8_T iv1[4] = { 0, 0, 0, 1 };

  int32_T i1;
  real_T b_temp[16];
  int32_T i2;

  /*  This functions calculate tip position and orientation of all links from */
  /*  given joint values. */
  /*  THIS DOES NOT CHANGE STRUCTURE OF THE ROBOT! */
  /*  */
  /*  r is the structure of the serial robot. */
  /*  q is the joint values (radians or meters). */
  /*  T contains the homogenous transformation matrix of each link starting */
  /*  from the base T(:,:,1) to the end-effector T(:,:,n+1). */
  /*  */
  /*  This gives the same result as the RVC toolbox. */
  /*  */
  /*  Contact: manurung.auralius@gmail.com */
  /*  */
  /*  References: */
  /*  https://www.cs.duke.edu/brd/Teaching/Bio/asmb/current/Papers/chap3-forward-kinematics.pdf */
  /*  See page 75 */
  memset(&temp[0], 0, sizeof(real_T) << 4);
  for (k = 0; k < 4; k++) {
    temp[k + (k << 2)] = 1.0;
  }

  k = 0;
  while (k < 4) {
    if (r->type[k] == 'r') {
      r->theta[k] = q[k];
    } else {
      if (r->type[k] == 'p') {
        r->d[k] = q[k];
      }
    }

    k++;
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  k = 0;
  while (k < 4) {
    ct = muDoubleScalarCos(r->theta[k] + r->offset[k]);
    st = muDoubleScalarSin(r->theta[k] + r->offset[k]);
    ca = muDoubleScalarCos(r->alpha[k]);
    sa = muDoubleScalarSin(r->alpha[k]);
    b_ct[0] = ct;
    b_ct[4] = -st * ca;
    b_ct[8] = st * sa;
    b_ct[12] = r->a[k] * ct;
    b_ct[1] = st;
    b_ct[5] = ct * ca;
    b_ct[9] = -ct * sa;
    b_ct[13] = r->a[k] * st;
    b_ct[2] = 0.0;
    b_ct[6] = sa;
    b_ct[10] = ca;
    b_ct[14] = r->d[k];
    for (i0 = 0; i0 < 4; i0++) {
      b_ct[3 + (i0 << 2)] = iv1[i0];
    }

    for (i0 = 0; i0 < 4; i0++) {
      for (i1 = 0; i1 < 4; i1++) {
        b_temp[i0 + (i1 << 2)] = 0.0;
        for (i2 = 0; i2 < 4; i2++) {
          b_temp[i0 + (i1 << 2)] += temp[i0 + (i2 << 2)] * b_ct[i2 + (i1 << 2)];
        }
      }
    }

    for (i0 = 0; i0 < 4; i0++) {
      for (i1 = 0; i1 < 4; i1++) {
        temp[i1 + (i0 << 2)] = b_temp[i1 + (i0 << 2)];
        T[(i1 + (i0 << 2)) + (k << 4)] = temp[i1 + (i0 << 2)];
      }
    }

    k++;
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  k = 0;
  while (k < 4) {
    for (i0 = 0; i0 < 3; i0++) {
      T[12 + (i0 + (k << 4))] += r->base[i0];
    }

    k++;
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }
}

/* End of code generation (cgr_fkine.c) */
