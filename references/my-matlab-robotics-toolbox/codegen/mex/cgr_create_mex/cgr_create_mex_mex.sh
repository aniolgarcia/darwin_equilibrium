MATLAB="/home/aniol/Matlab"
Arch=glnxa64
ENTRYPOINT=mexFunction
MAPFILE=$ENTRYPOINT'.map'
PREFDIR="/home/aniol/.matlab/R2018a"
OPTSFILE_NAME="./setEnv.sh"
. $OPTSFILE_NAME
COMPILER=$CC
. $OPTSFILE_NAME
echo "# Make settings for cgr_create_mex" > cgr_create_mex_mex.mki
echo "CC=$CC" >> cgr_create_mex_mex.mki
echo "CFLAGS=$CFLAGS" >> cgr_create_mex_mex.mki
echo "CLIBS=$CLIBS" >> cgr_create_mex_mex.mki
echo "COPTIMFLAGS=$COPTIMFLAGS" >> cgr_create_mex_mex.mki
echo "CDEBUGFLAGS=$CDEBUGFLAGS" >> cgr_create_mex_mex.mki
echo "CXX=$CXX" >> cgr_create_mex_mex.mki
echo "CXXFLAGS=$CXXFLAGS" >> cgr_create_mex_mex.mki
echo "CXXLIBS=$CXXLIBS" >> cgr_create_mex_mex.mki
echo "CXXOPTIMFLAGS=$CXXOPTIMFLAGS" >> cgr_create_mex_mex.mki
echo "CXXDEBUGFLAGS=$CXXDEBUGFLAGS" >> cgr_create_mex_mex.mki
echo "LDFLAGS=$LDFLAGS" >> cgr_create_mex_mex.mki
echo "LDOPTIMFLAGS=$LDOPTIMFLAGS" >> cgr_create_mex_mex.mki
echo "LDDEBUGFLAGS=$LDDEBUGFLAGS" >> cgr_create_mex_mex.mki
echo "Arch=$Arch" >> cgr_create_mex_mex.mki
echo "LD=$LD" >> cgr_create_mex_mex.mki
echo OMPFLAGS= >> cgr_create_mex_mex.mki
echo OMPLINKFLAGS= >> cgr_create_mex_mex.mki
echo "EMC_COMPILER=gcc" >> cgr_create_mex_mex.mki
echo "EMC_CONFIG=optim" >> cgr_create_mex_mex.mki
"/home/aniol/Matlab/bin/glnxa64/gmake" -j 1 -B -f cgr_create_mex_mex.mk
