/*
 * xswap.h
 *
 * Code generation for function 'xswap'
 *
 */

#ifndef XSWAP_H
#define XSWAP_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void b_xswap(real_T x[12], int32_T ix0, int32_T iy0);
extern void xswap(real_T x[9], int32_T ix0, int32_T iy0);

#endif

/* End of code generation (xswap.h) */
