/*
 * cgr_ikine2.c
 *
 * Code generation for function 'cgr_ikine2'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "norm.h"
#include "warning.h"
#include "xgetrf.h"
#include "pinv.h"
#include "cgr_create_mex_data.h"

/* Variable Definitions */
static emlrtRSInfo xc_emlrtRSI = { 21, /* lineNo */
  "cgr_ikine2",                        /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_ikine2.m"/* pathName */
};

static emlrtRSInfo yc_emlrtRSI = { 23, /* lineNo */
  "cgr_ikine2",                        /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_ikine2.m"/* pathName */
};

static emlrtRSInfo ad_emlrtRSI = { 36, /* lineNo */
  "cgr_ikine2",                        /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_ikine2.m"/* pathName */
};

static emlrtRSInfo bd_emlrtRSI = { 38, /* lineNo */
  "cgr_ikine2",                        /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_ikine2.m"/* pathName */
};

static emlrtRSInfo cd_emlrtRSI = { 45, /* lineNo */
  "cgr_ikine2",                        /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_ikine2.m"/* pathName */
};

static emlrtRSInfo dd_emlrtRSI = { 46, /* lineNo */
  "cgr_ikine2",                        /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_ikine2.m"/* pathName */
};

static emlrtRSInfo ed_emlrtRSI = { 40, /* lineNo */
  "mpower",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/lib/matlab/ops/mpower.m"/* pathName */
};

static emlrtRSInfo fd_emlrtRSI = { 49, /* lineNo */
  "power",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/lib/matlab/ops/power.m"/* pathName */
};

static emlrtRSInfo gd_emlrtRSI = { 1,  /* lineNo */
  "mldivide",                          /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/lib/matlab/ops/mldivide.p"/* pathName */
};

static emlrtRSInfo hd_emlrtRSI = { 42, /* lineNo */
  "lusolve",                           /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/lusolve.m"/* pathName */
};

static emlrtRSInfo id_emlrtRSI = { 103,/* lineNo */
  "lusolve",                           /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/lusolve.m"/* pathName */
};

static emlrtRSInfo jd_emlrtRSI = { 101,/* lineNo */
  "lusolve",                           /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/lusolve.m"/* pathName */
};

static emlrtRSInfo kd_emlrtRSI = { 144,/* lineNo */
  "lusolve",                           /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/lusolve.m"/* pathName */
};

static emlrtRSInfo ld_emlrtRSI = { 146,/* lineNo */
  "lusolve",                           /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/lusolve.m"/* pathName */
};

static emlrtRSInfo xd_emlrtRSI = { 76, /* lineNo */
  "lusolve",                           /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/lusolve.m"/* pathName */
};

/* Function Definitions */
void cgr_ikine2(const emlrtStack *sp, const struct0_T *r, const real_T p[3],
                real_T lambda, real_T treshold, real_T max_iter, real_T q[4],
                real_T *iter_taken, real_T *err)
{
  int32_T i;
  struct0_T b_r;
  real_T T[64];
  int32_T jBcol;
  real_T x[3];
  real_T c_r[4];
  real_T jac[12];
  int32_T exitg1;
  real_T delta_x[3];
  real_T K[12];
  int32_T k;
  real_T temp;
  real_T d1;
  real_T unusedU0[9];
  int8_T I[16];
  real_T A[16];
  int32_T ipiv[4];
  int32_T ip;
  int32_T kAcol;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;

  /*  Using damped least square method */
  /*  http://math.ucsd.edu/~sbuss/ResearchWeb/ikmethods/iksurvey.pdf */
  /*  See Equ. 11. */
  /*  r is the sructure of the robot. */
  /*  p (3x1) is the target cartesian position; */
  /*  */
  /*  THIS DOES NOT CHANGE STRUCTURE OF THE ROBOT! */
  /*  Get current pose. */
  for (i = 0; i < 4; i++) {
    q[i] = r->qc[i];
  }

  b_r = *r;
  st.site = &xc_emlrtRSI;
  cgr_fkine(&st, &b_r, r->qc, T);
  for (jBcol = 0; jBcol < 3; jBcol++) {
    x[jBcol] = T[60 + jBcol];
  }

  for (i = 0; i < 4; i++) {
    c_r[i] = r->qc[i];
  }

  st.site = &yc_emlrtRSI;
  cgr_jac(&st, r, c_r, jac);
  *iter_taken = 1.0;
  do {
    exitg1 = 0;
    (*iter_taken)++;
    for (i = 0; i < 3; i++) {
      delta_x[i] = p[i] - x[i];
    }

    /*  Keep in mind then inverse operation fails when matrix is not full */
    /*  rank, instead we will use pinv, Therefore, when lambda = 0, this */
    /*  method is the same as the pseudo-inverse method. */
    if (lambda > 0.0) {
      st.site = &ad_emlrtRSI;
      b_st.site = &ed_emlrtRSI;
      c_st.site = &fd_emlrtRSI;
      temp = lambda * lambda;
      for (jBcol = 0; jBcol < 16; jBcol++) {
        I[jBcol] = 0;
      }

      for (k = 0; k < 4; k++) {
        I[k + (k << 2)] = 1;
      }

      st.site = &ad_emlrtRSI;
      for (jBcol = 0; jBcol < 3; jBcol++) {
        for (i = 0; i < 4; i++) {
          K[i + (jBcol << 2)] = jac[jBcol + 3 * i];
        }
      }

      b_st.site = &gd_emlrtRSI;
      c_st.site = &hd_emlrtRSI;
      for (jBcol = 0; jBcol < 4; jBcol++) {
        for (i = 0; i < 4; i++) {
          d1 = 0.0;
          for (ip = 0; ip < 3; ip++) {
            d1 += jac[ip + 3 * jBcol] * jac[ip + 3 * i];
          }

          A[jBcol + (i << 2)] = d1 + temp * (real_T)I[jBcol + (i << 2)];
        }
      }

      d_st.site = &jd_emlrtRSI;
      xgetrf(&d_st, A, ipiv, &i);
      if (i > 0) {
        d_st.site = &id_emlrtRSI;
        e_st.site = &xd_emlrtRSI;
        warning(&e_st);
      }

      for (i = 0; i < 3; i++) {
        if (ipiv[i] != i + 1) {
          ip = ipiv[i] - 1;
          for (jBcol = 0; jBcol < 3; jBcol++) {
            temp = K[i + (jBcol << 2)];
            K[i + (jBcol << 2)] = K[ip + (jBcol << 2)];
            K[ip + (jBcol << 2)] = temp;
          }
        }
      }

      d_st.site = &kd_emlrtRSI;
      for (ip = 0; ip < 3; ip++) {
        jBcol = ip << 2;
        for (k = 0; k < 4; k++) {
          kAcol = k << 2;
          if (K[k + jBcol] != 0.0) {
            for (i = k + 1; i + 1 < 5; i++) {
              K[i + jBcol] -= K[k + jBcol] * A[i + kAcol];
            }
          }
        }
      }

      d_st.site = &ld_emlrtRSI;
      for (ip = 0; ip < 3; ip++) {
        jBcol = ip << 2;
        for (k = 3; k >= 0; k--) {
          kAcol = k << 2;
          if (K[k + jBcol] != 0.0) {
            K[k + jBcol] /= A[k + kAcol];
            for (i = 0; i < k; i++) {
              K[i + jBcol] -= K[k + jBcol] * A[i + kAcol];
            }
          }
        }
      }
    } else {
      st.site = &bd_emlrtRSI;
      pinv(&st, jac, K);
    }

    for (k = 0; k < 4; k++) {
      d1 = 0.0;
      for (jBcol = 0; jBcol < 3; jBcol++) {
        d1 += K[k + (jBcol << 2)] * delta_x[jBcol];
      }

      c_r[k] = muDoubleScalarMax(q[k] + d1, r->lb[k]);
    }

    for (k = 0; k < 4; k++) {
      q[k] = muDoubleScalarMin(r->ub[k], c_r[k]);
    }

    /*  This is a saturation function. */
    st.site = &cd_emlrtRSI;
    cgr_fkine_ee(&st, r, q, unusedU0, x);
    for (i = 0; i < 4; i++) {
      c_r[i] = q[i];
    }

    st.site = &dd_emlrtRSI;
    cgr_jac(&st, r, c_r, jac);
    *err = norm(delta_x);
    if (*err < treshold) {
      exitg1 = 1;
    } else {
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }

      if (*iter_taken > max_iter) {
        exitg1 = 1;
      }
    }
  } while (exitg1 == 0);

  /* fprintf('**cgr_ikine2** breaks after %i iterations with errror %f.\n', iter_taken, err); */
}

/* End of code generation (cgr_ikine2.c) */
