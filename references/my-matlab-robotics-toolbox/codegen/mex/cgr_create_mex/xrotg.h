/*
 * xrotg.h
 *
 * Code generation for function 'xrotg'
 *
 */

#ifndef XROTG_H
#define XROTG_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void xrotg(real_T *a, real_T *b, real_T *c, real_T *s);

#endif

/* End of code generation (xrotg.h) */
