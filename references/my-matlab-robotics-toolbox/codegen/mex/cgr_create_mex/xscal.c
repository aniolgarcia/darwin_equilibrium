/*
 * xscal.c
 *
 * Code generation for function 'xscal'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "xscal.h"
#include "eml_int_forloop_overflow_check.h"
#include "cgr_create_mex_data.h"

/* Variable Definitions */
static emlrtRSInfo jc_emlrtRSI = { 31, /* lineNo */
  "xscal",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+blas/xscal.m"/* pathName */
};

static emlrtRSInfo kc_emlrtRSI = { 18, /* lineNo */
  "xscal",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xscal.m"/* pathName */
};

/* Function Definitions */
void b_xscal(const emlrtStack *sp, real_T a, real_T x[9], int32_T ix0)
{
  int32_T k;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &jc_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  b_st.site = &kc_emlrtRSI;
  if ((!(ix0 > ix0 + 2)) && (ix0 + 2 > 2147483646)) {
    c_st.site = &gc_emlrtRSI;
    check_forloop_overflow_error(&c_st, true);
  }

  for (k = ix0; k <= ix0 + 2; k++) {
    x[k - 1] *= a;
  }
}

void xscal(const emlrtStack *sp, real_T a, real_T x[12], int32_T ix0)
{
  int32_T k;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &jc_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  b_st.site = &kc_emlrtRSI;
  if ((!(ix0 > ix0 + 3)) && (ix0 + 3 > 2147483646)) {
    c_st.site = &gc_emlrtRSI;
    check_forloop_overflow_error(&c_st, true);
  }

  for (k = ix0; k <= ix0 + 3; k++) {
    x[k - 1] *= a;
  }
}

/* End of code generation (xscal.c) */
