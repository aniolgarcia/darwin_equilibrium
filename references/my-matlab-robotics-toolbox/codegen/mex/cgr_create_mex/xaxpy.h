/*
 * xaxpy.h
 *
 * Code generation for function 'xaxpy'
 *
 */

#ifndef XAXPY_H
#define XAXPY_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void b_xaxpy(int32_T n, real_T a, const real_T x[12], int32_T ix0, real_T
                    y[4], int32_T iy0);
extern void c_xaxpy(int32_T n, real_T a, const real_T x[4], int32_T ix0, real_T
                    y[12], int32_T iy0);
extern void d_xaxpy(int32_T n, real_T a, int32_T ix0, real_T y[9], int32_T iy0);
extern void xaxpy(int32_T n, real_T a, int32_T ix0, real_T y[12], int32_T iy0);

#endif

/* End of code generation (xaxpy.h) */
