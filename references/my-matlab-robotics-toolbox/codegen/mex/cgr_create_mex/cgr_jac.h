/*
 * cgr_jac.h
 *
 * Code generation for function 'cgr_jac'
 *
 */

#ifndef CGR_JAC_H
#define CGR_JAC_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void cgr_jac(const emlrtStack *sp, const struct0_T *r, real_T q[4],
                    real_T jac[12]);

#endif

/* End of code generation (cgr_jac.h) */
