/*
 * cgr_self_update.c
 *
 * Code generation for function 'cgr_self_update'
 *
 */

/* Include files */
#include <string.h>
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"

/* Variable Definitions */
static emlrtRSInfo ce_emlrtRSI = { 7,  /* lineNo */
  "cgr_self_update",                   /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_self_update.m"/* pathName */
};

static emlrtRSInfo de_emlrtRSI = { 8,  /* lineNo */
  "cgr_self_update",                   /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_self_update.m"/* pathName */
};

/* Function Definitions */
void cgr_self_update(const emlrtStack *sp, struct0_T *r, const real_T qc[4],
                     const real_T base[3])
{
  int32_T i;
  struct0_T b_r;
  struct0_T c_r;
  struct0_T d_r;
  real_T b_qc[4];
  real_T dv0[12];
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  for (i = 0; i < 3; i++) {
    r->base[i] = base[i];
  }

  for (i = 0; i < 4; i++) {
    r->qc[i] = qc[i];
  }

  b_r = *r;
  c_r = b_r;
  d_r = c_r;
  st.site = &ce_emlrtRSI;
  cgr_fkine(&st, &d_r, qc, r->T);
  for (i = 0; i < 4; i++) {
    b_qc[i] = qc[i];
  }

  st.site = &de_emlrtRSI;
  cgr_jac(&st, r, b_qc, dv0);
  memcpy(&r->jac[0], &dv0[0], 12U * sizeof(real_T));
}

/* End of code generation (cgr_self_update.c) */
