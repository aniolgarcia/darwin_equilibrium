/*
 * cgr_create_mex_initialize.h
 *
 * Code generation for function 'cgr_create_mex_initialize'
 *
 */

#ifndef CGR_CREATE_MEX_INITIALIZE_H
#define CGR_CREATE_MEX_INITIALIZE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void cgr_create_mex_initialize(void);

#endif

/* End of code generation (cgr_create_mex_initialize.h) */
