/*
 * svd.h
 *
 * Code generation for function 'svd'
 *
 */

#ifndef SVD_H
#define SVD_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void svd(const emlrtStack *sp, const real_T A[12], real_T U[12], real_T
                s[3], real_T V[9]);

#endif

/* End of code generation (svd.h) */
