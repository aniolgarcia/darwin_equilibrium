/*
 * cgr_jac.c
 *
 * Code generation for function 'cgr_jac'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "cgr_create_mex_data.h"

/* Variable Definitions */
static emlrtRSInfo g_emlrtRSI = { 15,  /* lineNo */
  "cgr_jac",                           /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_jac.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 24,  /* lineNo */
  "cgr_jac",                           /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_jac.m"/* pathName */
};

/* Function Definitions */
void cgr_jac(const emlrtStack *sp, const struct0_T *r, real_T q[4], real_T jac
             [12])
{
  real_T unusedU0[9];
  real_T f0[3];
  int32_T i;
  real_T qc0[4];
  int32_T b_i;
  real_T f1[3];
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;

  /*  Compute jacobian of a serial robot numerically, works for both prismatic */
  /*  and revolute joint */
  /*  THIS DOES NOT CHANGE STRUCTURE OF THE ROBOT! */
  /*  */
  /*  r is the structure of the serial robot. */
  /*  q is the joint values (radians or meters). */
  /*  Caclulate f0, when no perturbation happens */
  st.site = &g_emlrtRSI;
  cgr_fkine_ee(&st, r, q, unusedU0, f0);

  /*  Do perturbation */
  for (i = 0; i < 4; i++) {
    qc0[i] = q[i];
  }

  i = 0;
  while (i < 4) {
    for (b_i = 0; b_i < 4; b_i++) {
      q[b_i] = qc0[b_i];
    }

    q[i] = qc0[i] + 1.0E-6;
    st.site = &h_emlrtRSI;
    cgr_fkine_ee(&st, r, q, unusedU0, f1);
    for (b_i = 0; b_i < 3; b_i++) {
      jac[b_i + 3 * i] = (f1[b_i] - f0[b_i]) * 1.0E+6;
    }

    i++;
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }
}

/* End of code generation (cgr_jac.c) */
