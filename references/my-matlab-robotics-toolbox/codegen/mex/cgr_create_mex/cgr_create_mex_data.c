/*
 * cgr_create_mex_data.c
 *
 * Code generation for function 'cgr_create_mex_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "cgr_create_mex_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131466U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "cgr_create_mex",                    /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

emlrtRSInfo s_emlrtRSI = { 78,         /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo t_emlrtRSI = { 85,         /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo v_emlrtRSI = { 93,         /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo w_emlrtRSI = { 101,        /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo y_emlrtRSI = { 116,        /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo ab_emlrtRSI = { 125,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo bb_emlrtRSI = { 128,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo cb_emlrtRSI = { 131,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo db_emlrtRSI = { 133,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo eb_emlrtRSI = { 137,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo fb_emlrtRSI = { 143,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo gb_emlrtRSI = { 177,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo ib_emlrtRSI = { 183,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo jb_emlrtRSI = { 186,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo kb_emlrtRSI = { 190,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo lb_emlrtRSI = { 208,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo nb_emlrtRSI = { 214,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo rb_emlrtRSI = { 278,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo sb_emlrtRSI = { 295,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo tb_emlrtRSI = { 328,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo ub_emlrtRSI = { 330,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo vb_emlrtRSI = { 347,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo wb_emlrtRSI = { 349,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo yb_emlrtRSI = { 387,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo ac_emlrtRSI = { 390,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo bc_emlrtRSI = { 403,       /* lineNo */
  "xzsvdc",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzsvdc.m"/* pathName */
};

emlrtRSInfo gc_emlrtRSI = { 21,        /* lineNo */
  "eml_int_forloop_overflow_check",    /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/lib/matlab/eml/eml_int_forloop_overflow_check.m"/* pathName */
};

emlrtRSInfo hc_emlrtRSI = { 21,        /* lineNo */
  "scaleVectorByRecip",                /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/scaleVectorByRecip.m"/* pathName */
};

emlrtRSInfo ic_emlrtRSI = { 23,        /* lineNo */
  "scaleVectorByRecip",                /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/scaleVectorByRecip.m"/* pathName */
};

emlrtRSInfo pc_emlrtRSI = { 47,        /* lineNo */
  "xaxpy",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+blas/xaxpy.m"/* pathName */
};

emlrtRSInfo qc_emlrtRSI = { 32,        /* lineNo */
  "xaxpy",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xaxpy.m"/* pathName */
};

emlrtRSInfo rc_emlrtRSI = { 26,        /* lineNo */
  "xrotg",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+blas/xrotg.m"/* pathName */
};

emlrtRSInfo sc_emlrtRSI = { 27,        /* lineNo */
  "xrotg",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+blas/xrotg.m"/* pathName */
};

emlrtRSInfo yd_emlrtRSI = { 59,        /* lineNo */
  "xtrsm",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+blas/xtrsm.m"/* pathName */
};

emlrtRSInfo ae_emlrtRSI = { 71,        /* lineNo */
  "xtrsm",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xtrsm.m"/* pathName */
};

emlrtRSInfo be_emlrtRSI = { 51,        /* lineNo */
  "xtrsm",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xtrsm.m"/* pathName */
};

const uint32_T uv0[4] = { 1991239461U, 2080304469U, 2365065779U, 3186330812U };

/* End of code generation (cgr_create_mex_data.c) */
