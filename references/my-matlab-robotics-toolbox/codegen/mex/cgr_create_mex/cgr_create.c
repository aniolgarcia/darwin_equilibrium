/*
 * cgr_create.c
 *
 * Code generation for function 'cgr_create'
 *
 */

/* Include files */
#include <string.h>
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"

/* Function Definitions */
void cgr_create(const real_T theta[4], const real_T d[4], const real_T a[4],
                const real_T alpha[4], const real_T offset[4], const char_T
                type[4], const real_T base[3], const real_T ub[4], const real_T
                lb[4], struct0_T *robot)
{
  int32_T i;
  int32_T ibtile;
  int32_T jcol;
  int32_T ibmat;
  int32_T k;

  /*  Create a robot structure */
  for (i = 0; i < 3; i++) {
    robot->base[i] = base[i];
  }

  for (i = 0; i < 4; i++) {
    robot->theta[i] = theta[i];
    robot->d[i] = d[i];
    robot->a[i] = a[i];
    robot->alpha[i] = alpha[i];
    robot->offset[i] = offset[i];
    robot->qc[i] = 0.0;
    robot->type[i] = type[i];
  }

  memset(&robot->jac[0], 0, 12U * sizeof(real_T));
  for (i = 0; i < 4; i++) {
    ibtile = i << 4;
    for (jcol = 0; jcol < 4; jcol++) {
      ibmat = ibtile + (jcol << 2);
      for (k = 0; k < 4; k++) {
        robot->T[ibmat + k] = 0.0;
      }
    }

    robot->ub[i] = ub[i];
    robot->lb[i] = lb[i];
  }
}

/* End of code generation (cgr_create.c) */
