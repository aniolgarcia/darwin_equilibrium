/*
 * pinv.h
 *
 * Code generation for function 'pinv'
 *
 */

#ifndef PINV_H
#define PINV_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void pinv(const emlrtStack *sp, const real_T A[12], real_T X[12]);

#endif

/* End of code generation (pinv.h) */
