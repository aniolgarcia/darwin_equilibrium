/*
 * cgr_fkine_ee.h
 *
 * Code generation for function 'cgr_fkine_ee'
 *
 */

#ifndef CGR_FKINE_EE_H
#define CGR_FKINE_EE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void cgr_fkine_ee(const emlrtStack *sp, const struct0_T *r, const real_T
  q[4], real_T R[9], real_T p[3]);

#endif

/* End of code generation (cgr_fkine_ee.h) */
