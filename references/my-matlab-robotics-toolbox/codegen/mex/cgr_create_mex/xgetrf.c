/*
 * xgetrf.c
 *
 * Code generation for function 'xgetrf'
 *
 */

/* Include files */
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "xgetrf.h"
#include "eml_int_forloop_overflow_check.h"
#include "cgr_create_mex_data.h"

/* Variable Definitions */
static emlrtRSInfo md_emlrtRSI = { 30, /* lineNo */
  "xgetrf",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+lapack/xgetrf.m"/* pathName */
};

static emlrtRSInfo nd_emlrtRSI = { 36, /* lineNo */
  "xzgetrf",                           /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzgetrf.m"/* pathName */
};

static emlrtRSInfo od_emlrtRSI = { 50, /* lineNo */
  "xzgetrf",                           /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzgetrf.m"/* pathName */
};

static emlrtRSInfo pd_emlrtRSI = { 58, /* lineNo */
  "xzgetrf",                           /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+reflapack/xzgetrf.m"/* pathName */
};

static emlrtRSInfo qd_emlrtRSI = { 23, /* lineNo */
  "ixamax",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+blas/ixamax.m"/* pathName */
};

static emlrtRSInfo rd_emlrtRSI = { 24, /* lineNo */
  "ixamax",                            /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/ixamax.m"/* pathName */
};

static emlrtRSInfo sd_emlrtRSI = { 45, /* lineNo */
  "xgeru",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+blas/xgeru.m"/* pathName */
};

static emlrtRSInfo td_emlrtRSI = { 45, /* lineNo */
  "xger",                              /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+blas/xger.m"/* pathName */
};

static emlrtRSInfo ud_emlrtRSI = { 15, /* lineNo */
  "xger",                              /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xger.m"/* pathName */
};

static emlrtRSInfo vd_emlrtRSI = { 41, /* lineNo */
  "xgerx",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xgerx.m"/* pathName */
};

static emlrtRSInfo wd_emlrtRSI = { 54, /* lineNo */
  "xgerx",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xgerx.m"/* pathName */
};

/* Function Definitions */
void xgetrf(const emlrtStack *sp, real_T A[16], int32_T ipiv[4], int32_T *info)
{
  int32_T iy;
  int32_T j;
  int32_T c;
  int32_T ix;
  real_T smax;
  int32_T jy;
  real_T s;
  int32_T b;
  int32_T b_j;
  int32_T ijA;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  emlrtStack f_st;
  emlrtStack g_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &md_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  f_st.prev = &e_st;
  f_st.tls = e_st.tls;
  g_st.prev = &f_st;
  g_st.tls = f_st.tls;
  for (iy = 0; iy < 4; iy++) {
    ipiv[iy] = 1 + iy;
  }

  *info = 0;
  for (j = 0; j < 3; j++) {
    c = j * 5;
    b_st.site = &nd_emlrtRSI;
    c_st.site = &qd_emlrtRSI;
    iy = 0;
    ix = c;
    smax = muDoubleScalarAbs(A[c]);
    d_st.site = &rd_emlrtRSI;
    for (jy = 2; jy <= 4 - j; jy++) {
      ix++;
      s = muDoubleScalarAbs(A[ix]);
      if (s > smax) {
        iy = jy - 1;
        smax = s;
      }
    }

    if (A[c + iy] != 0.0) {
      if (iy != 0) {
        ipiv[j] = (j + iy) + 1;
        ix = j;
        iy += j;
        for (jy = 0; jy < 4; jy++) {
          smax = A[ix];
          A[ix] = A[iy];
          A[iy] = smax;
          ix += 4;
          iy += 4;
        }
      }

      b = (c - j) + 4;
      b_st.site = &od_emlrtRSI;
      for (iy = c + 1; iy < b; iy++) {
        A[iy] /= A[c];
      }
    } else {
      *info = j + 1;
    }

    b_st.site = &pd_emlrtRSI;
    c_st.site = &sd_emlrtRSI;
    d_st.site = &td_emlrtRSI;
    e_st.site = &ud_emlrtRSI;
    iy = c;
    jy = c + 4;
    f_st.site = &vd_emlrtRSI;
    for (b_j = 1; b_j <= 3 - j; b_j++) {
      smax = A[jy];
      if (A[jy] != 0.0) {
        ix = c + 1;
        b = (iy - j) + 8;
        f_st.site = &wd_emlrtRSI;
        if ((!(iy + 6 > b)) && (b > 2147483646)) {
          g_st.site = &gc_emlrtRSI;
          check_forloop_overflow_error(&g_st, true);
        }

        for (ijA = iy + 5; ijA < b; ijA++) {
          A[ijA] += A[ix] * -smax;
          ix++;
        }
      }

      jy += 4;
      iy += 4;
    }
  }

  if ((*info == 0) && (!(A[15] != 0.0))) {
    *info = 4;
  }
}

/* End of code generation (xgetrf.c) */
