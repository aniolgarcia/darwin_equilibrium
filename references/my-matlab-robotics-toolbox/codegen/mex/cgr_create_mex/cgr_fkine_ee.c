/*
 * cgr_fkine_ee.c
 *
 * Code generation for function 'cgr_fkine_ee'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 19,    /* lineNo */
  "cgr_fkine_ee",                      /* fcnName */
  "/home/aniol/code/darwin_equilibrium/my-matlab-robotics-toolbox/cgr_fkine_ee.m"/* pathName */
};

/* Function Definitions */
void cgr_fkine_ee(const emlrtStack *sp, const struct0_T *r, const real_T q[4],
                  real_T R[9], real_T p[3])
{
  struct0_T b_r;
  real_T T[64];
  int32_T i3;
  int32_T i4;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;

  /*  This functions calculate tip position and orientation of the last link */
  /*  from given joint values. */
  /*  THIS DOES NOT CHANGE STRUCTURE OF THE ROBOT! */
  /*  */
  /*  r is the structure of the serial robot. */
  /*  q is the joint values (radians or meters). */
  /*  T contains the homogenous transformation matrix of each link starting */
  /*  from the base T(:,:,1) to the end-effector T(:,:,n+1). */
  /*  */
  /*  This gives the same result as the RVC toolbox. */
  /*  */
  /*  Contact: manurung.auralius@gmail.com */
  /*  */
  /*  References: */
  /*  https://www.cs.duke.edu/brd/Teaching/Bio/asmb/current/Papers/chap3-forward-kinematics.pdf */
  /*  See page 75 */
  b_r = *r;
  st.site = &emlrtRSI;
  cgr_fkine(&st, &b_r, q, T);
  for (i3 = 0; i3 < 3; i3++) {
    for (i4 = 0; i4 < 3; i4++) {
      R[i4 + 3 * i3] = T[48 + (i4 + (i3 << 2))];
    }

    p[i3] = T[60 + i3];
  }
}

/* End of code generation (cgr_fkine_ee.c) */
