/*
 * _coder_cgr_create_mex_api.h
 *
 * Code generation for function '_coder_cgr_create_mex_api'
 *
 */

#ifndef _CODER_CGR_CREATE_MEX_API_H
#define _CODER_CGR_CREATE_MEX_API_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void cgr_create_api(const mxArray * const prhs[9], int32_T nlhs, const
  mxArray *plhs[1]);
extern void cgr_fkine_api(const mxArray * const prhs[2], int32_T nlhs, const
  mxArray *plhs[1]);
extern void cgr_fkine_ee_api(const mxArray * const prhs[2], int32_T nlhs, const
  mxArray *plhs[2]);
extern void cgr_ikine1_api(const mxArray * const prhs[4], int32_T nlhs, const
  mxArray *plhs[3]);
extern void cgr_ikine2_api(const mxArray * const prhs[5], int32_T nlhs, const
  mxArray *plhs[3]);
extern void cgr_jac_api(const mxArray * const prhs[2], int32_T nlhs, const
  mxArray *plhs[1]);
extern void cgr_self_update_api(const mxArray * const prhs[3], int32_T nlhs,
  const mxArray *plhs[1]);

#endif

/* End of code generation (_coder_cgr_create_mex_api.h) */
