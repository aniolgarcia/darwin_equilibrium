/*
 * _coder_cgr_create_mex_api.c
 *
 * Code generation for function '_coder_cgr_create_mex_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "_coder_cgr_create_mex_api.h"
#include "cgr_create_mex_data.h"

/* Variable Definitions */
static const int32_T iv0[3] = { 4, 4, 4 };

/* Function Declarations */
static void ab_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[64]);
static real_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[4];
static const mxArray *b_emlrt_marshallOut(const real_T u[4]);
static real_T bb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId);
static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *type, const
  char_T *identifier, char_T y[4]);
static const mxArray *c_emlrt_marshallOut(const real_T u[64]);
static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, char_T y[4]);
static const mxArray *d_emlrt_marshallOut(const real_T u[9]);
static real_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *base,
  const char_T *identifier))[3];
static const mxArray *e_emlrt_marshallOut(const real_T u[3]);
static real_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *theta,
  const char_T *identifier))[4];
static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const struct0_T *u);
static real_T (*f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[3];
static const mxArray *f_emlrt_marshallOut(const real_T u[4]);
static real_T (*g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *ub,
  const char_T *identifier))[4];
static const mxArray *g_emlrt_marshallOut(const real_T u);
static real_T (*h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[4];
static const mxArray *h_emlrt_marshallOut(const real_T u[12]);
static void i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *r, const
  char_T *identifier, struct0_T *y);
static void j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct0_T *y);
static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[3]);
static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[4]);
static void m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[4]);
static void n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[12]);
static void o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[64]);
static real_T p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *treshold,
  const char_T *identifier);
static real_T q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static real_T (*r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[4];
static void s_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, char_T ret[4]);
static real_T (*t_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[3];
static real_T (*u_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[4];
static void v_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[3]);
static void w_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[4]);
static void x_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[4]);
static void y_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[12]);

/* Function Definitions */
static void ab_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[64])
{
  int32_T i12;
  int32_T i13;
  int32_T i14;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 3U, iv0);
  for (i12 = 0; i12 < 4; i12++) {
    for (i13 = 0; i13 < 4; i13++) {
      for (i14 = 0; i14 < 4; i14++) {
        ret[(i14 + (i13 << 2)) + (i12 << 4)] = (*(real_T (*)[64])emlrtMxGetData
          (src))[(i14 + (i13 << 2)) + (i12 << 4)];
      }
    }
  }

  emlrtDestroyArray(&src);
}

static real_T (*b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[4]
{
  real_T (*y)[4];
  y = r_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static const mxArray *b_emlrt_marshallOut(const real_T u[4])
{
  const mxArray *y;
  const mxArray *m2;
  static const int32_T iv11[2] = { 1, 4 };

  real_T *pData;
  int32_T i6;
  int32_T i;
  y = NULL;
  m2 = emlrtCreateNumericArray(2, iv11, mxDOUBLE_CLASS, mxREAL);
  pData = emlrtMxGetPr(m2);
  i6 = 0;
  for (i = 0; i < 4; i++) {
    pData[i6] = u[i];
    i6++;
  }

  emlrtAssign(&y, m2);
  return y;
}

static real_T bb_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *type, const
  char_T *identifier, char_T y[4])
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  d_emlrt_marshallIn(sp, emlrtAlias(type), &thisId, y);
  emlrtDestroyArray(&type);
}

static const mxArray *c_emlrt_marshallOut(const real_T u[64])
{
  const mxArray *y;
  const mxArray *m3;
  static const int32_T iv12[3] = { 0, 0, 0 };

  y = NULL;
  m3 = emlrtCreateNumericArray(3, iv12, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m3, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m3, iv0, 3);
  emlrtAssign(&y, m3);
  return y;
}

static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, char_T y[4])
{
  s_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static const mxArray *d_emlrt_marshallOut(const real_T u[9])
{
  const mxArray *y;
  const mxArray *m4;
  static const int32_T iv13[2] = { 0, 0 };

  static const int32_T iv14[2] = { 3, 3 };

  y = NULL;
  m4 = emlrtCreateNumericArray(2, iv13, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m4, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m4, iv14, 2);
  emlrtAssign(&y, m4);
  return y;
}

static real_T (*e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *base,
  const char_T *identifier))[3]
{
  real_T (*y)[3];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = f_emlrt_marshallIn(sp, emlrtAlias(base), &thisId);
  emlrtDestroyArray(&base);
  return y;
}
  static const mxArray *e_emlrt_marshallOut(const real_T u[3])
{
  const mxArray *y;
  const mxArray *m5;
  static const int32_T iv15[1] = { 0 };

  static const int32_T iv16[1] = { 3 };

  y = NULL;
  m5 = emlrtCreateNumericArray(1, iv15, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m5, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m5, iv16, 1);
  emlrtAssign(&y, m5);
  return y;
}

static real_T (*emlrt_marshallIn(const emlrtStack *sp, const mxArray *theta,
  const char_T *identifier))[4]
{
  real_T (*y)[4];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = b_emlrt_marshallIn(sp, emlrtAlias(theta), &thisId);
  emlrtDestroyArray(&theta);
  return y;
}
  static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const struct0_T *
  u)
{
  const mxArray *y;
  static const char * sv0[12] = { "base", "theta", "d", "a", "alpha", "offset",
    "qc", "type", "jac", "T", "ub", "lb" };

  const mxArray *b_y;
  const mxArray *m1;
  static const int32_T iv5[1] = { 3 };

  real_T *pData;
  int32_T i5;
  int32_T i;
  static const int32_T iv6[1] = { 4 };

  char_T b_u[4];
  static const int32_T iv7[2] = { 1, 4 };

  static const int32_T iv8[2] = { 3, 4 };

  int32_T b_i;
  int32_T c_i;
  static const int32_T iv9[1] = { 4 };

  static const int32_T iv10[1] = { 4 };

  y = NULL;
  emlrtAssign(&y, emlrtCreateStructMatrix(1, 1, 12, sv0));
  b_y = NULL;
  m1 = emlrtCreateNumericArray(1, iv5, mxDOUBLE_CLASS, mxREAL);
  pData = emlrtMxGetPr(m1);
  i5 = 0;
  for (i = 0; i < 3; i++) {
    pData[i5] = u->base[i];
    i5++;
  }

  emlrtAssign(&b_y, m1);
  emlrtSetFieldR2017b(y, 0, "base", b_y, 0);
  emlrtSetFieldR2017b(y, 0, "theta", b_emlrt_marshallOut(u->theta), 1);
  emlrtSetFieldR2017b(y, 0, "d", b_emlrt_marshallOut(u->d), 2);
  emlrtSetFieldR2017b(y, 0, "a", b_emlrt_marshallOut(u->a), 3);
  emlrtSetFieldR2017b(y, 0, "alpha", b_emlrt_marshallOut(u->alpha), 4);
  emlrtSetFieldR2017b(y, 0, "offset", b_emlrt_marshallOut(u->offset), 5);
  b_y = NULL;
  m1 = emlrtCreateNumericArray(1, iv6, mxDOUBLE_CLASS, mxREAL);
  pData = emlrtMxGetPr(m1);
  i5 = 0;
  for (i = 0; i < 4; i++) {
    pData[i5] = u->qc[i];
    i5++;
  }

  emlrtAssign(&b_y, m1);
  emlrtSetFieldR2017b(y, 0, "qc", b_y, 6);
  for (i5 = 0; i5 < 4; i5++) {
    b_u[i5] = u->type[i5];
  }

  b_y = NULL;
  m1 = emlrtCreateCharArray(2, iv7);
  emlrtInitCharArrayR2013a(sp, 4, m1, &b_u[0]);
  emlrtAssign(&b_y, m1);
  emlrtSetFieldR2017b(y, 0, "type", b_y, 7);
  b_y = NULL;
  m1 = emlrtCreateNumericArray(2, iv8, mxDOUBLE_CLASS, mxREAL);
  pData = emlrtMxGetPr(m1);
  i5 = 0;
  for (i = 0; i < 4; i++) {
    for (b_i = 0; b_i < 3; b_i++) {
      pData[i5] = u->jac[b_i + 3 * i];
      i5++;
    }
  }

  emlrtAssign(&b_y, m1);
  emlrtSetFieldR2017b(y, 0, "jac", b_y, 8);
  b_y = NULL;
  m1 = emlrtCreateNumericArray(3, iv0, mxDOUBLE_CLASS, mxREAL);
  pData = emlrtMxGetPr(m1);
  i5 = 0;
  for (i = 0; i < 4; i++) {
    for (b_i = 0; b_i < 4; b_i++) {
      for (c_i = 0; c_i < 4; c_i++) {
        pData[i5] = u->T[(c_i + (b_i << 2)) + (i << 4)];
        i5++;
      }
    }
  }

  emlrtAssign(&b_y, m1);
  emlrtSetFieldR2017b(y, 0, "T", b_y, 9);
  b_y = NULL;
  m1 = emlrtCreateNumericArray(1, iv9, mxDOUBLE_CLASS, mxREAL);
  pData = emlrtMxGetPr(m1);
  i5 = 0;
  for (i = 0; i < 4; i++) {
    pData[i5] = u->ub[i];
    i5++;
  }

  emlrtAssign(&b_y, m1);
  emlrtSetFieldR2017b(y, 0, "ub", b_y, 10);
  b_y = NULL;
  m1 = emlrtCreateNumericArray(1, iv10, mxDOUBLE_CLASS, mxREAL);
  pData = emlrtMxGetPr(m1);
  i5 = 0;
  for (i = 0; i < 4; i++) {
    pData[i5] = u->lb[i];
    i5++;
  }

  emlrtAssign(&b_y, m1);
  emlrtSetFieldR2017b(y, 0, "lb", b_y, 11);
  return y;
}

static real_T (*f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[3]
{
  real_T (*y)[3];
  y = t_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static const mxArray *f_emlrt_marshallOut(const real_T u[4])
{
  const mxArray *y;
  const mxArray *m6;
  static const int32_T iv17[1] = { 0 };

  static const int32_T iv18[1] = { 4 };

  y = NULL;
  m6 = emlrtCreateNumericArray(1, iv17, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m6, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m6, iv18, 1);
  emlrtAssign(&y, m6);
  return y;
}

static real_T (*g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *ub,
  const char_T *identifier))[4]
{
  real_T (*y)[4];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = h_emlrt_marshallIn(sp, emlrtAlias(ub), &thisId);
  emlrtDestroyArray(&ub);
  return y;
}
  static const mxArray *g_emlrt_marshallOut(const real_T u)
{
  const mxArray *y;
  const mxArray *m7;
  y = NULL;
  m7 = emlrtCreateDoubleScalar(u);
  emlrtAssign(&y, m7);
  return y;
}

static real_T (*h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId))[4]
{
  real_T (*y)[4];
  y = u_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static const mxArray *h_emlrt_marshallOut(const real_T u[12])
{
  const mxArray *y;
  const mxArray *m8;
  static const int32_T iv19[2] = { 0, 0 };

  static const int32_T iv20[2] = { 3, 4 };

  y = NULL;
  m8 = emlrtCreateNumericArray(2, iv19, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m8, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m8, iv20, 2);
  emlrtAssign(&y, m8);
  return y;
}

static void i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *r, const
  char_T *identifier, struct0_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  j_emlrt_marshallIn(sp, emlrtAlias(r), &thisId, y);
  emlrtDestroyArray(&r);
}

static void j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, struct0_T *y)
{
  emlrtMsgIdentifier thisId;
  static const char * fieldNames[12] = { "base", "theta", "d", "a", "alpha",
    "offset", "qc", "type", "jac", "T", "ub", "lb" };

  static const int32_T dims = 0;
  thisId.fParent = parentId;
  thisId.bParentIsCell = false;
  emlrtCheckStructR2012b(sp, parentId, u, 12, fieldNames, 0U, &dims);
  thisId.fIdentifier = "base";
  k_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 0, "base")),
                     &thisId, y->base);
  thisId.fIdentifier = "theta";
  l_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 1, "theta")),
                     &thisId, y->theta);
  thisId.fIdentifier = "d";
  l_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 2, "d")),
                     &thisId, y->d);
  thisId.fIdentifier = "a";
  l_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 3, "a")),
                     &thisId, y->a);
  thisId.fIdentifier = "alpha";
  l_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 4, "alpha")),
                     &thisId, y->alpha);
  thisId.fIdentifier = "offset";
  l_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 5, "offset")),
                     &thisId, y->offset);
  thisId.fIdentifier = "qc";
  m_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 6, "qc")),
                     &thisId, y->qc);
  thisId.fIdentifier = "type";
  d_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 7, "type")),
                     &thisId, y->type);
  thisId.fIdentifier = "jac";
  n_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 8, "jac")),
                     &thisId, y->jac);
  thisId.fIdentifier = "T";
  o_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 9, "T")),
                     &thisId, y->T);
  thisId.fIdentifier = "ub";
  m_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 10, "ub")),
                     &thisId, y->ub);
  thisId.fIdentifier = "lb";
  m_emlrt_marshallIn(sp, emlrtAlias(emlrtGetFieldR2017b(sp, u, 0, 11, "lb")),
                     &thisId, y->lb);
  emlrtDestroyArray(&u);
}

static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[3])
{
  v_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[4])
{
  w_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[4])
{
  x_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[12])
{
  y_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, real_T y[64])
{
  ab_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static real_T p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *treshold,
  const char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = q_emlrt_marshallIn(sp, emlrtAlias(treshold), &thisId);
  emlrtDestroyArray(&treshold);
  return y;
}

static real_T q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = bb_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static real_T (*r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[4]
{
  real_T (*ret)[4];
  static const int32_T dims[2] = { 1, 4 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims);
  ret = (real_T (*)[4])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
  static void s_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, char_T ret[4])
{
  static const int32_T dims[2] = { 1, 4 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "char", false, 2U, dims);
  emlrtImportCharArrayR2015b(sp, src, &ret[0], 4);
  emlrtDestroyArray(&src);
}

static real_T (*t_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[3]
{
  real_T (*ret)[3];
  static const int32_T dims[1] = { 3 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  ret = (real_T (*)[3])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
  static real_T (*u_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[4]
{
  real_T (*ret)[4];
  static const int32_T dims[1] = { 4 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  ret = (real_T (*)[4])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static void v_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[3])
{
  static const int32_T dims[1] = { 3 };

  int32_T i7;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  for (i7 = 0; i7 < 3; i7++) {
    ret[i7] = (*(real_T (*)[3])emlrtMxGetData(src))[i7];
  }

  emlrtDestroyArray(&src);
}

static void w_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[4])
{
  static const int32_T dims[2] = { 1, 4 };

  int32_T i8;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims);
  for (i8 = 0; i8 < 4; i8++) {
    ret[i8] = (*(real_T (*)[4])emlrtMxGetData(src))[i8];
  }

  emlrtDestroyArray(&src);
}

static void x_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[4])
{
  static const int32_T dims[1] = { 4 };

  int32_T i9;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims);
  for (i9 = 0; i9 < 4; i9++) {
    ret[i9] = (*(real_T (*)[4])emlrtMxGetData(src))[i9];
  }

  emlrtDestroyArray(&src);
}

static void y_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, real_T ret[12])
{
  static const int32_T dims[2] = { 3, 4 };

  int32_T i10;
  int32_T i11;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims);
  for (i10 = 0; i10 < 4; i10++) {
    for (i11 = 0; i11 < 3; i11++) {
      ret[i11 + 3 * i10] = (*(real_T (*)[12])emlrtMxGetData(src))[i11 + 3 * i10];
    }
  }

  emlrtDestroyArray(&src);
}

void cgr_create_api(const mxArray * const prhs[9], int32_T nlhs, const mxArray
                    *plhs[1])
{
  real_T (*theta)[4];
  real_T (*d)[4];
  real_T (*a)[4];
  real_T (*alpha)[4];
  real_T (*offset)[4];
  char_T type[4];
  real_T (*base)[3];
  real_T (*ub)[4];
  real_T (*lb)[4];
  const mxArray *N_DOFSMx;
  struct0_T robot;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  (void)nlhs;
  st.tls = emlrtRootTLSGlobal;

  /* Marshall function inputs */
  theta = emlrt_marshallIn(&st, emlrtAlias(prhs[0]), "theta");
  d = emlrt_marshallIn(&st, emlrtAlias(prhs[1]), "d");
  a = emlrt_marshallIn(&st, emlrtAlias(prhs[2]), "a");
  alpha = emlrt_marshallIn(&st, emlrtAlias(prhs[3]), "alpha");
  offset = emlrt_marshallIn(&st, emlrtAlias(prhs[4]), "offset");
  c_emlrt_marshallIn(&st, emlrtAliasP(prhs[5]), "type", type);
  base = e_emlrt_marshallIn(&st, emlrtAlias(prhs[6]), "base");
  ub = g_emlrt_marshallIn(&st, emlrtAlias(prhs[7]), "ub");
  lb = g_emlrt_marshallIn(&st, emlrtAlias(prhs[8]), "lb");

  /* Marshall in global variables */
  N_DOFSMx = emlrtMexGetVariablePtr("global", "N_DOFS");
  if (N_DOFSMx != NULL) {
    emlrtCheckArrayChecksumR2014a(&st, "N_DOFS", uv0, N_DOFSMx, true);
  }

  /* Invoke the target function */
  cgr_create(*theta, *d, *a, *alpha, *offset, type, *base, *ub, *lb, &robot);

  /* Marshall out global variables */
  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(&st, &robot);
}

void cgr_fkine_api(const mxArray * const prhs[2], int32_T nlhs, const mxArray
                   *plhs[1])
{
  real_T (*T)[64];
  struct0_T r;
  real_T (*q)[4];
  const mxArray *N_DOFSMx;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  (void)nlhs;
  st.tls = emlrtRootTLSGlobal;
  T = (real_T (*)[64])mxMalloc(sizeof(real_T [64]));

  /* Marshall function inputs */
  i_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "r", &r);
  q = g_emlrt_marshallIn(&st, emlrtAlias(prhs[1]), "q");

  /* Marshall in global variables */
  N_DOFSMx = emlrtMexGetVariablePtr("global", "N_DOFS");
  if (N_DOFSMx != NULL) {
    emlrtCheckArrayChecksumR2014a(&st, "N_DOFS", uv0, N_DOFSMx, true);
  }

  /* Invoke the target function */
  cgr_fkine(&st, &r, *q, *T);

  /* Marshall out global variables */
  /* Marshall function outputs */
  plhs[0] = c_emlrt_marshallOut(*T);
}

void cgr_fkine_ee_api(const mxArray * const prhs[2], int32_T nlhs, const mxArray
                      *plhs[2])
{
  real_T (*R)[9];
  real_T (*p)[3];
  struct0_T r;
  real_T (*q)[4];
  const mxArray *N_DOFSMx;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  R = (real_T (*)[9])mxMalloc(sizeof(real_T [9]));
  p = (real_T (*)[3])mxMalloc(sizeof(real_T [3]));

  /* Marshall function inputs */
  i_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "r", &r);
  q = g_emlrt_marshallIn(&st, emlrtAlias(prhs[1]), "q");

  /* Marshall in global variables */
  N_DOFSMx = emlrtMexGetVariablePtr("global", "N_DOFS");
  if (N_DOFSMx != NULL) {
    emlrtCheckArrayChecksumR2014a(&st, "N_DOFS", uv0, N_DOFSMx, true);
  }

  /* Invoke the target function */
  cgr_fkine_ee(&st, &r, *q, *R, *p);

  /* Marshall out global variables */
  /* Marshall function outputs */
  plhs[0] = d_emlrt_marshallOut(*R);
  if (nlhs > 1) {
    plhs[1] = e_emlrt_marshallOut(*p);
  }
}

void cgr_ikine1_api(const mxArray * const prhs[4], int32_T nlhs, const mxArray
                    *plhs[3])
{
  real_T (*q)[4];
  struct0_T r;
  real_T (*p)[3];
  real_T treshold;
  real_T max_iter;
  const mxArray *N_DOFSMx;
  real_T iter_taken;
  real_T err;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  q = (real_T (*)[4])mxMalloc(sizeof(real_T [4]));

  /* Marshall function inputs */
  i_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "r", &r);
  p = e_emlrt_marshallIn(&st, emlrtAlias(prhs[1]), "p");
  treshold = p_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "treshold");
  max_iter = p_emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "max_iter");

  /* Marshall in global variables */
  N_DOFSMx = emlrtMexGetVariablePtr("global", "N_DOFS");
  if (N_DOFSMx != NULL) {
    emlrtCheckArrayChecksumR2014a(&st, "N_DOFS", uv0, N_DOFSMx, true);
  }

  /* Invoke the target function */
  cgr_ikine1(&st, &r, *p, treshold, max_iter, *q, &iter_taken, &err);

  /* Marshall out global variables */
  /* Marshall function outputs */
  plhs[0] = f_emlrt_marshallOut(*q);
  if (nlhs > 1) {
    plhs[1] = g_emlrt_marshallOut(iter_taken);
  }

  if (nlhs > 2) {
    plhs[2] = g_emlrt_marshallOut(err);
  }
}

void cgr_ikine2_api(const mxArray * const prhs[5], int32_T nlhs, const mxArray
                    *plhs[3])
{
  real_T (*q)[4];
  struct0_T r;
  real_T (*p)[3];
  real_T lambda;
  real_T treshold;
  real_T max_iter;
  const mxArray *N_DOFSMx;
  real_T iter_taken;
  real_T err;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  q = (real_T (*)[4])mxMalloc(sizeof(real_T [4]));

  /* Marshall function inputs */
  i_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "r", &r);
  p = e_emlrt_marshallIn(&st, emlrtAlias(prhs[1]), "p");
  lambda = p_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "lambda");
  treshold = p_emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "treshold");
  max_iter = p_emlrt_marshallIn(&st, emlrtAliasP(prhs[4]), "max_iter");

  /* Marshall in global variables */
  N_DOFSMx = emlrtMexGetVariablePtr("global", "N_DOFS");
  if (N_DOFSMx != NULL) {
    emlrtCheckArrayChecksumR2014a(&st, "N_DOFS", uv0, N_DOFSMx, true);
  }

  /* Invoke the target function */
  cgr_ikine2(&st, &r, *p, lambda, treshold, max_iter, *q, &iter_taken, &err);

  /* Marshall out global variables */
  /* Marshall function outputs */
  plhs[0] = f_emlrt_marshallOut(*q);
  if (nlhs > 1) {
    plhs[1] = g_emlrt_marshallOut(iter_taken);
  }

  if (nlhs > 2) {
    plhs[2] = g_emlrt_marshallOut(err);
  }
}

void cgr_jac_api(const mxArray * const prhs[2], int32_T nlhs, const mxArray
                 *plhs[1])
{
  real_T (*jac)[12];
  const mxArray *prhs_copy_idx_1;
  struct0_T r;
  real_T (*q)[4];
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  (void)nlhs;
  st.tls = emlrtRootTLSGlobal;
  jac = (real_T (*)[12])mxMalloc(sizeof(real_T [12]));
  prhs_copy_idx_1 = emlrtProtectR2012b(prhs[1], 1, false, -1);

  /* Marshall function inputs */
  i_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "r", &r);
  q = g_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_1), "q");

  /* Marshall in global variables */
  prhs_copy_idx_1 = emlrtMexGetVariablePtr("global", "N_DOFS");
  if (prhs_copy_idx_1 != NULL) {
    emlrtCheckArrayChecksumR2014a(&st, "N_DOFS", uv0, prhs_copy_idx_1, true);
  }

  /* Invoke the target function */
  cgr_jac(&st, &r, *q, *jac);

  /* Marshall out global variables */
  /* Marshall function outputs */
  plhs[0] = h_emlrt_marshallOut(*jac);
}

void cgr_self_update_api(const mxArray * const prhs[3], int32_T nlhs, const
  mxArray *plhs[1])
{
  struct0_T r;
  real_T (*qc)[4];
  real_T (*base)[3];
  const mxArray *N_DOFSMx;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  (void)nlhs;
  st.tls = emlrtRootTLSGlobal;

  /* Marshall function inputs */
  i_emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "r", &r);
  qc = g_emlrt_marshallIn(&st, emlrtAlias(prhs[1]), "qc");
  base = e_emlrt_marshallIn(&st, emlrtAlias(prhs[2]), "base");

  /* Marshall in global variables */
  N_DOFSMx = emlrtMexGetVariablePtr("global", "N_DOFS");
  if (N_DOFSMx != NULL) {
    emlrtCheckArrayChecksumR2014a(&st, "N_DOFS", uv0, N_DOFSMx, true);
  }

  /* Invoke the target function */
  cgr_self_update(&st, &r, *qc, *base);

  /* Marshall out global variables */
  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(&st, &r);
}

/* End of code generation (_coder_cgr_create_mex_api.c) */
