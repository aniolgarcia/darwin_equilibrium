/*
 * cgr_create.h
 *
 * Code generation for function 'cgr_create'
 *
 */

#ifndef CGR_CREATE_H
#define CGR_CREATE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void cgr_create(const real_T theta[4], const real_T d[4], const real_T a
  [4], const real_T alpha[4], const real_T offset[4], const char_T type[4],
  const real_T base[3], const real_T ub[4], const real_T lb[4], struct0_T *robot);

#endif

/* End of code generation (cgr_create.h) */
