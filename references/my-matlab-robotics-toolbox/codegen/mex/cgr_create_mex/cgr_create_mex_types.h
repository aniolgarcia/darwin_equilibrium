/*
 * cgr_create_mex_types.h
 *
 * Code generation for function 'cgr_create'
 *
 */

#ifndef CGR_CREATE_MEX_TYPES_H
#define CGR_CREATE_MEX_TYPES_H

/* Include files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct {
  real_T base[3];
  real_T theta[4];
  real_T d[4];
  real_T a[4];
  real_T alpha[4];
  real_T offset[4];
  real_T qc[4];
  char_T type[4];
  real_T jac[12];
  real_T T[64];
  real_T ub[4];
  real_T lb[4];
} struct0_T;

#endif                                 /*typedef_struct0_T*/
#endif

/* End of code generation (cgr_create_mex_types.h) */
