/*
 * cgr_ikine1.h
 *
 * Code generation for function 'cgr_ikine1'
 *
 */

#ifndef CGR_IKINE1_H
#define CGR_IKINE1_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void cgr_ikine1(const emlrtStack *sp, const struct0_T *r, const real_T p
  [3], real_T treshold, real_T max_iter, real_T q[4], real_T *iter_taken, real_T
  *err);

#endif

/* End of code generation (cgr_ikine1.h) */
