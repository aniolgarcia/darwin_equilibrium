/*
 * xdotc.c
 *
 * Code generation for function 'xdotc'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "xdotc.h"
#include "eml_int_forloop_overflow_check.h"
#include "cgr_create_mex_data.h"

/* Variable Definitions */
static emlrtRSInfo lc_emlrtRSI = { 32, /* lineNo */
  "xdotc",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+blas/xdotc.m"/* pathName */
};

static emlrtRSInfo mc_emlrtRSI = { 35, /* lineNo */
  "xdot",                              /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+blas/xdot.m"/* pathName */
};

static emlrtRSInfo nc_emlrtRSI = { 15, /* lineNo */
  "xdot",                              /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xdot.m"/* pathName */
};

static emlrtRSInfo oc_emlrtRSI = { 42, /* lineNo */
  "xdotx",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xdotx.m"/* pathName */
};

/* Function Definitions */
real_T b_xdotc(const emlrtStack *sp, int32_T n, const real_T x[9], int32_T ix0,
               const real_T y[9], int32_T iy0)
{
  real_T d;
  int32_T ix;
  int32_T iy;
  int32_T k;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &lc_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  b_st.site = &mc_emlrtRSI;
  c_st.site = &nc_emlrtRSI;
  d = 0.0;
  if (!(n < 1)) {
    ix = ix0;
    iy = iy0;
    d_st.site = &oc_emlrtRSI;
    if (n > 2147483646) {
      e_st.site = &gc_emlrtRSI;
      check_forloop_overflow_error(&e_st, true);
    }

    for (k = 1; k <= n; k++) {
      d += x[ix - 1] * y[iy - 1];
      ix++;
      iy++;
    }
  }

  return d;
}

real_T xdotc(const emlrtStack *sp, int32_T n, const real_T x[12], int32_T ix0,
             const real_T y[12], int32_T iy0)
{
  real_T d;
  int32_T ix;
  int32_T iy;
  int32_T k;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &lc_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  b_st.site = &mc_emlrtRSI;
  c_st.site = &nc_emlrtRSI;
  d = 0.0;
  if (!(n < 1)) {
    ix = ix0;
    iy = iy0;
    d_st.site = &oc_emlrtRSI;
    if (n > 2147483646) {
      e_st.site = &gc_emlrtRSI;
      check_forloop_overflow_error(&e_st, true);
    }

    for (k = 1; k <= n; k++) {
      d += x[ix - 1] * y[iy - 1];
      ix++;
      iy++;
    }
  }

  return d;
}

/* End of code generation (xdotc.c) */
