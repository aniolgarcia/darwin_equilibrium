/*
 * pinv.c
 *
 * Code generation for function 'pinv'
 *
 */

/* Include files */
#include <math.h>
#include "mwmathutil.h"
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "pinv.h"
#include "xscal.h"
#include "eml_int_forloop_overflow_check.h"
#include "svd.h"
#include "cgr_create_mex_data.h"

/* Variable Definitions */
static emlrtRSInfo i_emlrtRSI = { 19,  /* lineNo */
  "pinv",                              /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/lib/matlab/matfun/pinv.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 46,  /* lineNo */
  "pinv",                              /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/lib/matlab/matfun/pinv.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 53,  /* lineNo */
  "pinv",                              /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/lib/matlab/matfun/pinv.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 68,  /* lineNo */
  "pinv",                              /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/lib/matlab/matfun/pinv.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 69,  /* lineNo */
  "pinv",                              /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/lib/matlab/matfun/pinv.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 72,  /* lineNo */
  "pinv",                              /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/lib/matlab/matfun/pinv.m"/* pathName */
};

static emlrtRSInfo tc_emlrtRSI = { 63, /* lineNo */
  "xgemm",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+blas/xgemm.m"/* pathName */
};

static emlrtRSInfo uc_emlrtRSI = { 64, /* lineNo */
  "xgemm",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xgemm.m"/* pathName */
};

static emlrtRSInfo vc_emlrtRSI = { 129,/* lineNo */
  "xgemm",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xgemm.m"/* pathName */
};

static emlrtRSInfo wc_emlrtRSI = { 137,/* lineNo */
  "xgemm",                             /* fcnName */
  "/home/aniol/Matlab/toolbox/eml/eml/+coder/+internal/+refblas/xgemm.m"/* pathName */
};

/* Function Definitions */
void pinv(const emlrtStack *sp, const real_T A[12], real_T X[12])
{
  int32_T k;
  boolean_T p;
  int32_T vcol;
  real_T b_A[12];
  real_T b_X[12];
  real_T U[12];
  real_T s[3];
  real_T V[9];
  real_T absxk;
  int32_T br;
  int32_T r;
  int32_T ar;
  int32_T b;
  int32_T ib;
  int32_T ia;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  emlrtStack e_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &i_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  e_st.prev = &d_st;
  e_st.tls = d_st.tls;
  for (k = 0; k < 3; k++) {
    for (vcol = 0; vcol < 4; vcol++) {
      b_A[vcol + (k << 2)] = A[k + 3 * vcol];
    }
  }

  p = true;
  for (k = 0; k < 12; k++) {
    b_X[k] = 0.0;
    if (p && ((!muDoubleScalarIsInf(b_A[k])) && (!muDoubleScalarIsNaN(b_A[k]))))
    {
      p = true;
    } else {
      p = false;
    }
  }

  if (!p) {
    for (k = 0; k < 12; k++) {
      b_X[k] = rtNaN;
    }
  } else {
    b_st.site = &j_emlrtRSI;
    svd(&b_st, b_A, U, s, V);
    b_st.site = &k_emlrtRSI;
    absxk = muDoubleScalarAbs(s[0]);
    if ((!muDoubleScalarIsInf(absxk)) && (!muDoubleScalarIsNaN(absxk))) {
      if (absxk <= 2.2250738585072014E-308) {
        absxk = 4.94065645841247E-324;
      } else {
        frexp(absxk, &br);
        absxk = ldexp(1.0, br - 53);
      }
    } else {
      absxk = rtNaN;
    }

    absxk *= 4.0;
    r = 0;
    k = 1;
    while ((k < 4) && (s[k - 1] > absxk)) {
      r++;
      k++;
    }

    if (r > 0) {
      vcol = 1;
      b_st.site = &l_emlrtRSI;
      if (r > 2147483646) {
        c_st.site = &gc_emlrtRSI;
        check_forloop_overflow_error(&c_st, true);
      }

      for (k = 1; k <= r; k++) {
        b_st.site = &m_emlrtRSI;
        b_xscal(&b_st, 1.0 / s[k - 1], V, vcol);
        vcol += 3;
      }

      b_st.site = &n_emlrtRSI;
      c_st.site = &tc_emlrtRSI;
      for (k = 0; k <= 10; k += 3) {
        d_st.site = &uc_emlrtRSI;
        for (vcol = k; vcol < k + 3; vcol++) {
          b_X[vcol] = 0.0;
        }
      }

      br = 0;
      for (k = 0; k <= 10; k += 3) {
        ar = 0;
        br++;
        b = br + ((r - 1) << 2);
        d_st.site = &vc_emlrtRSI;
        if ((!(br > b)) && (b > 2147483643)) {
          e_st.site = &gc_emlrtRSI;
          check_forloop_overflow_error(&e_st, true);
        }

        for (ib = br; ib <= b; ib += 4) {
          if (U[ib - 1] != 0.0) {
            ia = ar;
            d_st.site = &wc_emlrtRSI;
            for (vcol = k; vcol < k + 3; vcol++) {
              ia++;
              b_X[vcol] += U[ib - 1] * V[ia - 1];
            }
          }

          ar += 3;
        }
      }
    }
  }

  for (k = 0; k < 3; k++) {
    for (vcol = 0; vcol < 4; vcol++) {
      X[vcol + (k << 2)] = b_X[k + 3 * vcol];
    }
  }
}

/* End of code generation (pinv.c) */
