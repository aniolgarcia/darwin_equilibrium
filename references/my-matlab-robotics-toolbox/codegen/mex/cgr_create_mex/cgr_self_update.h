/*
 * cgr_self_update.h
 *
 * Code generation for function 'cgr_self_update'
 *
 */

#ifndef CGR_SELF_UPDATE_H
#define CGR_SELF_UPDATE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "cgr_create_mex_types.h"

/* Function Declarations */
extern void cgr_self_update(const emlrtStack *sp, struct0_T *r, const real_T qc
  [4], const real_T base[3]);

#endif

/* End of code generation (cgr_self_update.h) */
