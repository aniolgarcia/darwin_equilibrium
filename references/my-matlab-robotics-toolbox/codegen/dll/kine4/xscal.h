/*
 * File: xscal.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef XSCAL_H
#define XSCAL_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void b_xscal(double a, double x[9], int ix0);
  extern void xscal(double a, double x[12], int ix0);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for xscal.h
 *
 * [EOF]
 */
