/*
 * File: xnrm2.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef XNRM2_H
#define XNRM2_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern double b_xnrm2(int n, const double x[3], int ix0);
  extern double xnrm2(int n, const double x[12], int ix0);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for xnrm2.h
 *
 * [EOF]
 */
