/*
 * File: cgr_ikine2.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "norm.h"
#include "mldivide.h"
#include "pinv.h"

/* Function Definitions */

/*
 * Using damped least square method
 *  http://math.ucsd.edu/~sbuss/ResearchWeb/ikmethods/iksurvey.pdf
 *  See Equ. 11.
 *  r is the sructure of the robot.
 *  p (3x1) is the target cartesian position;
 *
 *  THIS DOES NOT CHANGE STRUCTURE OF THE ROBOT!
 * Arguments    : const struct0_T *r
 *                const double p[3]
 *                double lambda
 *                double treshold
 *                double max_iter
 *                double q[4]
 *                double *iter_taken
 *                double *err
 * Return Type  : void
 */
void cgr_ikine2(const struct0_T *r, const double p[3], double lambda, double
                treshold, double max_iter, double q[4], double *iter_taken,
                double *err)
{
  int i;
  struct0_T b_r;
  double T[64];
  int i6;
  double x[3];
  double c_r[4];
  double jac[12];
  double delta_x[3];
  double K[12];
  double c;
  signed char I[16];
  double d0;
  double unusedU0[9];
  double b_jac[16];
  int i7;

  /*  Get current pose. */
  for (i = 0; i < 4; i++) {
    q[i] = r->qc[i];
  }

  b_r = *r;
  cgr_fkine(&b_r, r->qc, T);
  for (i6 = 0; i6 < 3; i6++) {
    x[i6] = T[60 + i6];
  }

  for (i = 0; i < 4; i++) {
    c_r[i] = r->qc[i];
  }

  cgr_jac(r, c_r, jac);
  *iter_taken = 1.0;
  do {
    (*iter_taken)++;
    for (i = 0; i < 3; i++) {
      delta_x[i] = p[i] - x[i];
    }

    /*  Keep in mind then inverse operation fails when matrix is not full */
    /*  rank, instead we will use pinv, Therefore, when lambda = 0, this */
    /*  method is the same as the pseudo-inverse method. */
    if (lambda > 0.0) {
      c = lambda * lambda;
      for (i6 = 0; i6 < 16; i6++) {
        I[i6] = 0;
      }

      for (i = 0; i < 4; i++) {
        I[i + (i << 2)] = 1;
      }

      for (i6 = 0; i6 < 3; i6++) {
        for (i = 0; i < 4; i++) {
          K[i + (i6 << 2)] = jac[i6 + 3 * i];
        }
      }

      for (i6 = 0; i6 < 4; i6++) {
        for (i = 0; i < 4; i++) {
          d0 = 0.0;
          for (i7 = 0; i7 < 3; i7++) {
            d0 += jac[i7 + 3 * i6] * jac[i7 + 3 * i];
          }

          b_jac[i6 + (i << 2)] = d0 + c * (double)I[i6 + (i << 2)];
        }
      }

      mldivide(b_jac, K);
    } else {
      pinv(jac, K);
    }

    for (i = 0; i < 4; i++) {
      d0 = 0.0;
      for (i6 = 0; i6 < 3; i6++) {
        d0 += K[i + (i6 << 2)] * delta_x[i6];
      }

      c = q[i] + d0;
      if ((c > r->lb[i]) || rtIsNaN(r->lb[i])) {
        c_r[i] = c;
      } else {
        c_r[i] = r->lb[i];
      }
    }

    for (i = 0; i < 4; i++) {
      if ((r->ub[i] < c_r[i]) || rtIsNaN(c_r[i])) {
        q[i] = r->ub[i];
      } else {
        q[i] = c_r[i];
      }
    }

    /*  This is a saturation function. */
    cgr_fkine_ee(r, q, unusedU0, x);
    for (i = 0; i < 4; i++) {
      c_r[i] = q[i];
    }

    cgr_jac(r, c_r, jac);
    *err = norm(delta_x);
  } while (!((*err < treshold) || (*iter_taken > max_iter)));

  /* fprintf('**cgr_ikine2** breaks after %i iterations with errror %f.\n', iter_taken, err); */
}

/*
 * File trailer for cgr_ikine2.c
 *
 * [EOF]
 */
