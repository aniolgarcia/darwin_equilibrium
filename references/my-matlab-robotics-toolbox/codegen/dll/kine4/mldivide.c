/*
 * File: mldivide.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

/* Include Files */
#include <math.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "mldivide.h"

/* Function Definitions */

/*
 * Arguments    : const double A[16]
 *                double B[12]
 * Return Type  : void
 */
void mldivide(const double A[16], double B[12])
{
  double b_A[16];
  int i8;
  int j;
  signed char ipiv[4];
  int c;
  int iy;
  int jy;
  int ix;
  int k;
  double smax;
  int i;
  double s;
  memcpy(&b_A[0], &A[0], sizeof(double) << 4);
  for (i8 = 0; i8 < 4; i8++) {
    ipiv[i8] = (signed char)(1 + i8);
  }

  for (j = 0; j < 3; j++) {
    c = j * 5;
    iy = 0;
    ix = c;
    smax = fabs(b_A[c]);
    for (k = 2; k <= 4 - j; k++) {
      ix++;
      s = fabs(b_A[ix]);
      if (s > smax) {
        iy = k - 1;
        smax = s;
      }
    }

    if (b_A[c + iy] != 0.0) {
      if (iy != 0) {
        ipiv[j] = (signed char)((j + iy) + 1);
        ix = j;
        iy += j;
        for (k = 0; k < 4; k++) {
          smax = b_A[ix];
          b_A[ix] = b_A[iy];
          b_A[iy] = smax;
          ix += 4;
          iy += 4;
        }
      }

      i8 = (c - j) + 4;
      for (i = c + 1; i < i8; i++) {
        b_A[i] /= b_A[c];
      }
    }

    iy = c;
    jy = c + 4;
    for (i = 1; i <= 3 - j; i++) {
      smax = b_A[jy];
      if (b_A[jy] != 0.0) {
        ix = c + 1;
        i8 = (iy - j) + 8;
        for (k = 5 + iy; k < i8; k++) {
          b_A[k] += b_A[ix] * -smax;
          ix++;
        }
      }

      jy += 4;
      iy += 4;
    }

    if (ipiv[j] != j + 1) {
      iy = ipiv[j] - 1;
      for (jy = 0; jy < 3; jy++) {
        smax = B[j + (jy << 2)];
        B[j + (jy << 2)] = B[iy + (jy << 2)];
        B[iy + (jy << 2)] = smax;
      }
    }
  }

  for (j = 0; j < 3; j++) {
    jy = j << 2;
    for (k = 0; k < 4; k++) {
      iy = k << 2;
      if (B[k + jy] != 0.0) {
        for (i = k + 1; i + 1 < 5; i++) {
          B[i + jy] -= B[k + jy] * b_A[i + iy];
        }
      }
    }
  }

  for (j = 0; j < 3; j++) {
    jy = j << 2;
    for (k = 3; k >= 0; k--) {
      iy = k << 2;
      if (B[k + jy] != 0.0) {
        B[k + jy] /= b_A[k + iy];
        for (i = 0; i < k; i++) {
          B[i + jy] -= B[k + jy] * b_A[i + iy];
        }
      }
    }
  }
}

/*
 * File trailer for mldivide.c
 *
 * [EOF]
 */
