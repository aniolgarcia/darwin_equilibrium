/*
 * File: kine4_types.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef KINE4_TYPES_H
#define KINE4_TYPES_H

/* Include Files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct {
  double base[3];
  double theta[4];
  double d[4];
  double a[4];
  double alpha[4];
  double offset[4];
  double qc[4];
  char type[4];
  double jac[12];
  double T[64];
  double ub[4];
  double lb[4];
} struct0_T;

#endif                                 /*typedef_struct0_T*/
#endif

/*
 * File trailer for kine4_types.h
 *
 * [EOF]
 */
