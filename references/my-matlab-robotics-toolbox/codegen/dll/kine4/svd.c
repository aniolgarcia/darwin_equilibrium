/*
 * File: svd.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

/* Include Files */
#include <math.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "svd.h"
#include "xrot.h"
#include "xrotg.h"
#include "sqrt.h"
#include "xswap.h"
#include "xscal.h"
#include "xaxpy.h"
#include "xdotc.h"
#include "xnrm2.h"

/* Function Definitions */

/*
 * Arguments    : const double A[12]
 *                double U[12]
 *                double s[3]
 *                double V[9]
 * Return Type  : void
 */
void svd(const double A[12], double U[12], double s[3], double V[9])
{
  double b_A[12];
  int i;
  double b_s[3];
  double e[3];
  double work[4];
  double Vf[9];
  int q;
  int m;
  int qq;
  boolean_T apply_transform;
  double nrm;
  int k;
  int iter;
  double r;
  double snorm;
  double rt;
  int exitg1;
  boolean_T exitg2;
  double f;
  double scale;
  double sqds;
  memcpy(&b_A[0], &A[0], 12U * sizeof(double));
  for (i = 0; i < 3; i++) {
    b_s[i] = 0.0;
    e[i] = 0.0;
  }

  for (i = 0; i < 4; i++) {
    work[i] = 0.0;
  }

  memset(&U[0], 0, 12U * sizeof(double));
  memset(&Vf[0], 0, 9U * sizeof(double));
  for (q = 0; q < 3; q++) {
    qq = q + (q << 2);
    apply_transform = false;
    nrm = xnrm2(4 - q, b_A, qq + 1);
    if (nrm > 0.0) {
      apply_transform = true;
      if (b_A[qq] < 0.0) {
        b_s[q] = -nrm;
      } else {
        b_s[q] = nrm;
      }

      if (fabs(b_s[q]) >= 1.0020841800044864E-292) {
        r = 1.0 / b_s[q];
        i = (qq - q) + 4;
        for (k = qq; k < i; k++) {
          b_A[k] *= r;
        }
      } else {
        i = (qq - q) + 4;
        for (k = qq; k < i; k++) {
          b_A[k] /= b_s[q];
        }
      }

      b_A[qq]++;
      b_s[q] = -b_s[q];
    } else {
      b_s[q] = 0.0;
    }

    for (k = q + 1; k + 1 < 4; k++) {
      i = q + (k << 2);
      if (apply_transform) {
        xaxpy(4 - q, -(xdotc(4 - q, b_A, qq + 1, b_A, i + 1) / b_A[q + (q << 2)]),
              qq + 1, b_A, i + 1);
      }

      e[k] = b_A[i];
    }

    for (k = q; k + 1 < 5; k++) {
      U[k + (q << 2)] = b_A[k + (q << 2)];
    }

    if (q + 1 <= 1) {
      nrm = b_xnrm2(2, e, 2);
      if (nrm == 0.0) {
        e[0] = 0.0;
      } else {
        if (e[1] < 0.0) {
          r = -nrm;
        } else {
          r = nrm;
        }

        if (e[1] < 0.0) {
          e[0] = -nrm;
        } else {
          e[0] = nrm;
        }

        if (fabs(r) >= 1.0020841800044864E-292) {
          r = 1.0 / r;
          for (k = 1; k < 3; k++) {
            e[k] *= r;
          }
        } else {
          for (k = 1; k < 3; k++) {
            e[k] /= r;
          }
        }

        e[1]++;
        e[0] = -e[0];
        for (k = 2; k < 5; k++) {
          work[k - 1] = 0.0;
        }

        for (k = 1; k + 1 < 4; k++) {
          b_xaxpy(3, e[k], b_A, (k << 2) + 2, work, 2);
        }

        for (k = 1; k + 1 < 4; k++) {
          c_xaxpy(3, -e[k] / e[1], work, 2, b_A, (k << 2) + 2);
        }
      }

      for (k = 1; k + 1 < 4; k++) {
        Vf[k] = e[k];
      }
    }
  }

  m = 1;
  e[1] = b_A[9];
  e[2] = 0.0;
  for (q = 2; q >= 0; q--) {
    qq = q + (q << 2);
    if (b_s[q] != 0.0) {
      for (k = q + 1; k + 1 < 4; k++) {
        i = (q + (k << 2)) + 1;
        xaxpy(4 - q, -(xdotc(4 - q, U, qq + 1, U, i) / U[qq]), qq + 1, U, i);
      }

      for (k = q; k + 1 < 5; k++) {
        U[k + (q << 2)] = -U[k + (q << 2)];
      }

      U[qq]++;
      for (k = 1; k <= q; k++) {
        U[(k + (q << 2)) - 1] = 0.0;
      }
    } else {
      for (k = 0; k < 4; k++) {
        U[k + (q << 2)] = 0.0;
      }

      U[qq] = 1.0;
    }
  }

  for (q = 2; q >= 0; q--) {
    if ((q + 1 <= 1) && (e[0] != 0.0)) {
      for (k = 2; k < 4; k++) {
        i = 3 * (k - 1) + 2;
        d_xaxpy(2, -(b_xdotc(2, Vf, 2, Vf, i) / Vf[1]), 2, Vf, i);
      }
    }

    for (k = 0; k < 3; k++) {
      Vf[k + 3 * q] = 0.0;
    }

    Vf[q + 3 * q] = 1.0;
  }

  for (q = 0; q < 3; q++) {
    nrm = e[q];
    if (b_s[q] != 0.0) {
      rt = fabs(b_s[q]);
      r = b_s[q] / rt;
      b_s[q] = rt;
      if (q + 1 < 3) {
        nrm = e[q] / r;
      }

      xscal(r, U, 1 + (q << 2));
    }

    if ((q + 1 < 3) && (nrm != 0.0)) {
      rt = fabs(nrm);
      r = rt / nrm;
      nrm = rt;
      b_s[q + 1] *= r;
      b_xscal(r, Vf, 1 + 3 * (q + 1));
    }

    e[q] = nrm;
  }

  iter = 0;
  snorm = 0.0;
  for (k = 0; k < 3; k++) {
    nrm = fabs(b_s[k]);
    r = fabs(e[k]);
    if ((nrm > r) || rtIsNaN(r)) {
      r = nrm;
    }

    if (!((snorm > r) || rtIsNaN(r))) {
      snorm = r;
    }
  }

  while ((m + 2 > 0) && (!(iter >= 75))) {
    k = m;
    do {
      exitg1 = 0;
      q = k + 1;
      if (k + 1 == 0) {
        exitg1 = 1;
      } else {
        nrm = fabs(e[k]);
        if ((nrm <= 2.2204460492503131E-16 * (fabs(b_s[k]) + fabs(b_s[k + 1]))) ||
            (nrm <= 1.0020841800044864E-292) || ((iter > 20) && (nrm <=
              2.2204460492503131E-16 * snorm))) {
          e[k] = 0.0;
          exitg1 = 1;
        } else {
          k--;
        }
      }
    } while (exitg1 == 0);

    if (k + 1 == m + 1) {
      i = 4;
    } else {
      qq = m + 2;
      i = m + 2;
      exitg2 = false;
      while ((!exitg2) && (i >= k + 1)) {
        qq = i;
        if (i == k + 1) {
          exitg2 = true;
        } else {
          nrm = 0.0;
          if (i < m + 2) {
            nrm = fabs(e[i - 1]);
          }

          if (i > k + 2) {
            nrm += fabs(e[i - 2]);
          }

          r = fabs(b_s[i - 1]);
          if ((r <= 2.2204460492503131E-16 * nrm) || (r <=
               1.0020841800044864E-292)) {
            b_s[i - 1] = 0.0;
            exitg2 = true;
          } else {
            i--;
          }
        }
      }

      if (qq == k + 1) {
        i = 3;
      } else if (qq == m + 2) {
        i = 1;
      } else {
        i = 2;
        q = qq;
      }
    }

    switch (i) {
     case 1:
      f = e[m];
      e[m] = 0.0;
      for (k = m; k + 1 >= q + 1; k--) {
        xrotg(&b_s[k], &f, &nrm, &r);
        if (k + 1 > q + 1) {
          f = -r * e[0];
          e[0] *= nrm;
        }

        xrot(Vf, 1 + 3 * k, 1 + 3 * (m + 1), nrm, r);
      }
      break;

     case 2:
      f = e[q - 1];
      e[q - 1] = 0.0;
      for (k = q; k < m + 2; k++) {
        xrotg(&b_s[k], &f, &nrm, &r);
        f = -r * e[k];
        e[k] *= nrm;
        b_xrot(U, 1 + (k << 2), 1 + ((q - 1) << 2), nrm, r);
      }
      break;

     case 3:
      scale = fabs(b_s[m + 1]);
      r = fabs(b_s[m]);
      if (!((scale > r) || rtIsNaN(r))) {
        scale = r;
      }

      r = fabs(e[m]);
      if (!((scale > r) || rtIsNaN(r))) {
        scale = r;
      }

      r = fabs(b_s[q]);
      if (!((scale > r) || rtIsNaN(r))) {
        scale = r;
      }

      r = fabs(e[q]);
      if (!((scale > r) || rtIsNaN(r))) {
        scale = r;
      }

      f = b_s[m + 1] / scale;
      nrm = b_s[m] / scale;
      r = e[m] / scale;
      sqds = b_s[q] / scale;
      rt = ((nrm + f) * (nrm - f) + r * r) / 2.0;
      nrm = f * r;
      nrm *= nrm;
      if ((rt != 0.0) || (nrm != 0.0)) {
        r = rt * rt + nrm;
        b_sqrt(&r);
        if (rt < 0.0) {
          r = -r;
        }

        r = nrm / (rt + r);
      } else {
        r = 0.0;
      }

      f = (sqds + f) * (sqds - f) + r;
      rt = sqds * (e[q] / scale);
      for (k = q + 1; k <= m + 1; k++) {
        xrotg(&f, &rt, &nrm, &r);
        if (k > q + 1) {
          e[0] = f;
        }

        f = nrm * b_s[k - 1] + r * e[k - 1];
        e[k - 1] = nrm * e[k - 1] - r * b_s[k - 1];
        rt = r * b_s[k];
        b_s[k] *= nrm;
        xrot(Vf, 1 + 3 * (k - 1), 1 + 3 * k, nrm, r);
        b_s[k - 1] = f;
        xrotg(&b_s[k - 1], &rt, &nrm, &r);
        f = nrm * e[k - 1] + r * b_s[k];
        b_s[k] = -r * e[k - 1] + nrm * b_s[k];
        rt = r * e[k];
        e[k] *= nrm;
        b_xrot(U, 1 + ((k - 1) << 2), 1 + (k << 2), nrm, r);
      }

      e[m] = f;
      iter++;
      break;

     default:
      if (b_s[q] < 0.0) {
        b_s[q] = -b_s[q];
        b_xscal(-1.0, Vf, 1 + 3 * q);
      }

      i = q + 1;
      while ((q + 1 < 3) && (b_s[q] < b_s[i])) {
        rt = b_s[q];
        b_s[q] = b_s[i];
        b_s[i] = rt;
        xswap(Vf, 1 + 3 * q, 1 + 3 * (q + 1));
        b_xswap(U, 1 + (q << 2), 1 + ((q + 1) << 2));
        q = i;
        i++;
      }

      iter = 0;
      m--;
      break;
    }
  }

  for (k = 0; k < 3; k++) {
    s[k] = b_s[k];
    for (i = 0; i < 3; i++) {
      V[i + 3 * k] = Vf[i + 3 * k];
    }
  }
}

/*
 * File trailer for svd.c
 *
 * [EOF]
 */
