/*
 * File: kine4_initialize.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef KINE4_INITIALIZE_H
#define KINE4_INITIALIZE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void kine4_initialize(void);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for kine4_initialize.h
 *
 * [EOF]
 */
