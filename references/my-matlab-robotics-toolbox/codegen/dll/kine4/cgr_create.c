/*
 * File: cgr_create.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

/* Include Files */
#include <string.h>
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"

/* Function Definitions */

/*
 * Create a robot structure
 * Arguments    : const double theta[4]
 *                const double d[4]
 *                const double a[4]
 *                const double alpha[4]
 *                const double offset[4]
 *                const char type[4]
 *                const double base[3]
 *                const double ub[4]
 *                const double lb[4]
 *                struct0_T *robot
 * Return Type  : void
 */
void cgr_create(const double theta[4], const double d[4], const double a[4],
                const double alpha[4], const double offset[4], const char type[4],
                const double base[3], const double ub[4], const double lb[4],
                struct0_T *robot)
{
  int i;
  int ibtile;
  int jcol;
  int ibmat;
  int k;
  for (i = 0; i < 3; i++) {
    robot->base[i] = base[i];
  }

  for (i = 0; i < 4; i++) {
    robot->theta[i] = theta[i];
    robot->d[i] = d[i];
    robot->a[i] = a[i];
    robot->alpha[i] = alpha[i];
    robot->offset[i] = offset[i];
    robot->qc[i] = 0.0;
    robot->type[i] = type[i];
  }

  memset(&robot->jac[0], 0, 12U * sizeof(double));
  for (i = 0; i < 4; i++) {
    ibtile = i << 4;
    for (jcol = 0; jcol < 4; jcol++) {
      ibmat = ibtile + (jcol << 2);
      for (k = 0; k < 4; k++) {
        robot->T[ibmat + k] = 0.0;
      }
    }

    robot->ub[i] = ub[i];
    robot->lb[i] = lb[i];
  }
}

/*
 * File trailer for cgr_create.c
 *
 * [EOF]
 */
