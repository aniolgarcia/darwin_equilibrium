/*
 * File: cgr_fkine.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

/* Include Files */
#include <math.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"

/* Function Definitions */

/*
 * This functions calculate tip position and orientation of all links from
 *  given joint values.
 *  THIS DOES NOT CHANGE STRUCTURE OF THE ROBOT!
 *
 *  r is the structure of the serial robot.
 *  q is the joint values (radians or meters).
 *  T contains the homogenous transformation matrix of each link starting
 *  from the base T(:,:,1) to the end-effector T(:,:,n+1).
 *
 *  This gives the same result as the RVC toolbox.
 *
 *  Contact: manurung.auralius@gmail.com
 *
 *  References:
 *  https://www.cs.duke.edu/brd/Teaching/Bio/asmb/current/Papers/chap3-forward-kinematics.pdf
 *  See page 75
 * Arguments    : struct0_T *r
 *                const double q[4]
 *                double T[64]
 * Return Type  : void
 */
void cgr_fkine(struct0_T *r, const double q[4], double T[64])
{
  double temp[16];
  int i;
  double ct;
  double st;
  int i0;
  double ca;
  double sa;
  double b_ct[16];
  static const signed char iv0[4] = { 0, 0, 0, 1 };

  int i1;
  double b_temp[16];
  int i2;
  memset(&temp[0], 0, sizeof(double) << 4);
  for (i = 0; i < 4; i++) {
    temp[i + (i << 2)] = 1.0;
    if (r->type[i] == 'r') {
      r->theta[i] = q[i];
    } else {
      if (r->type[i] == 'p') {
        r->d[i] = q[i];
      }
    }
  }

  for (i = 0; i < 4; i++) {
    ct = cos(r->theta[i] + r->offset[i]);
    st = sin(r->theta[i] + r->offset[i]);
    ca = cos(r->alpha[i]);
    sa = sin(r->alpha[i]);
    b_ct[0] = ct;
    b_ct[4] = -st * ca;
    b_ct[8] = st * sa;
    b_ct[12] = r->a[i] * ct;
    b_ct[1] = st;
    b_ct[5] = ct * ca;
    b_ct[9] = -ct * sa;
    b_ct[13] = r->a[i] * st;
    b_ct[2] = 0.0;
    b_ct[6] = sa;
    b_ct[10] = ca;
    b_ct[14] = r->d[i];
    for (i0 = 0; i0 < 4; i0++) {
      b_ct[3 + (i0 << 2)] = iv0[i0];
    }

    for (i0 = 0; i0 < 4; i0++) {
      for (i1 = 0; i1 < 4; i1++) {
        b_temp[i0 + (i1 << 2)] = 0.0;
        for (i2 = 0; i2 < 4; i2++) {
          b_temp[i0 + (i1 << 2)] += temp[i0 + (i2 << 2)] * b_ct[i2 + (i1 << 2)];
        }
      }
    }

    for (i0 = 0; i0 < 4; i0++) {
      for (i1 = 0; i1 < 4; i1++) {
        temp[i1 + (i0 << 2)] = b_temp[i1 + (i0 << 2)];
        T[(i1 + (i0 << 2)) + (i << 4)] = temp[i1 + (i0 << 2)];
      }
    }
  }

  for (i = 0; i < 4; i++) {
    for (i0 = 0; i0 < 3; i0++) {
      T[12 + (i0 + (i << 4))] += r->base[i0];
    }
  }
}

/*
 * File trailer for cgr_fkine.c
 *
 * [EOF]
 */
