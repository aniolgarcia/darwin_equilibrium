/*
 * File: _coder_kine4_api.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef _CODER_KINE4_API_H
#define _CODER_KINE4_API_H

/* Include Files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_kine4_api.h"

/* Type Definitions */
#ifndef typedef_struct0_T
#define typedef_struct0_T

typedef struct {
  real_T base[3];
  real_T theta[4];
  real_T d[4];
  real_T a[4];
  real_T alpha[4];
  real_T offset[4];
  real_T qc[4];
  char_T type[4];
  real_T jac[12];
  real_T T[64];
  real_T ub[4];
  real_T lb[4];
} struct0_T;

#endif                                 /*typedef_struct0_T*/

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void cgr_create(real_T theta[4], real_T d[4], real_T a[4], real_T
    alpha[4], real_T offset[4], char_T type[4], real_T base[3], real_T ub[4],
    real_T lb[4], struct0_T *robot);
  extern void cgr_create_api(const mxArray * const prhs[9], int32_T nlhs, const
    mxArray *plhs[1]);
  extern void cgr_fkine(struct0_T *r, real_T q[4], real_T T[64]);
  extern void cgr_fkine_api(const mxArray * const prhs[2], int32_T nlhs, const
    mxArray *plhs[1]);
  extern void cgr_fkine_ee(struct0_T *r, real_T q[4], real_T R[9], real_T p[3]);
  extern void cgr_fkine_ee_api(const mxArray * const prhs[2], int32_T nlhs,
    const mxArray *plhs[2]);
  extern void cgr_ikine1(struct0_T *r, real_T p[3], real_T treshold, real_T
    max_iter, real_T q[4], real_T *iter_taken, real_T *err);
  extern void cgr_ikine1_api(const mxArray * const prhs[4], int32_T nlhs, const
    mxArray *plhs[3]);
  extern void cgr_ikine2(struct0_T *r, real_T p[3], real_T lambda, real_T
    treshold, real_T max_iter, real_T q[4], real_T *iter_taken, real_T *err);
  extern void cgr_ikine2_api(const mxArray * const prhs[5], int32_T nlhs, const
    mxArray *plhs[3]);
  extern void cgr_jac(struct0_T *r, real_T q[4], real_T jac[12]);
  extern void cgr_jac_api(const mxArray * const prhs[2], int32_T nlhs, const
    mxArray *plhs[1]);
  extern void cgr_self_update(struct0_T *r, real_T qc[4], real_T base[3]);
  extern void cgr_self_update_api(const mxArray * const prhs[3], int32_T nlhs,
    const mxArray *plhs[1]);
  extern void kine4_atexit(void);
  extern void kine4_initialize(void);
  extern void kine4_terminate(void);
  extern void kine4_xil_terminate(void);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for _coder_kine4_api.h
 *
 * [EOF]
 */
