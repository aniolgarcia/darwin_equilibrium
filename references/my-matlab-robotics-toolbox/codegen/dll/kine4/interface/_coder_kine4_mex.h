/*
 * File: _coder_kine4_mex.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef _CODER_KINE4_MEX_H
#define _CODER_KINE4_MEX_H

/* Include Files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "_coder_kine4_api.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const
    mxArray *prhs[]);
  extern emlrtCTX mexFunctionCreateRootTLS(void);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for _coder_kine4_mex.h
 *
 * [EOF]
 */
