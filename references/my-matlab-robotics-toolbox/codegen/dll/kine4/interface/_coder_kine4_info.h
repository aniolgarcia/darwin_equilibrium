/* 
 * File: _coder_kine4_info.h 
 *  
 * MATLAB Coder version            : 4.0 
 * C/C++ source code generated on  : 03-May-2018 17:39:19 
 */

#ifndef _CODER_KINE4_INFO_H
#define _CODER_KINE4_INFO_H
/* Include Files */ 
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */ 
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties(void);

#endif
/* 
 * File trailer for _coder_kine4_info.h 
 *  
 * [EOF] 
 */
