/*
 * File: xrot.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef XROT_H
#define XROT_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void b_xrot(double x[12], int ix0, int iy0, double c, double s);
  extern void xrot(double x[9], int ix0, int iy0, double c, double s);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for xrot.h
 *
 * [EOF]
 */
