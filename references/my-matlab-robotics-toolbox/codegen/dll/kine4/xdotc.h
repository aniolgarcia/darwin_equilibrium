/*
 * File: xdotc.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef XDOTC_H
#define XDOTC_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern double b_xdotc(int n, const double x[9], int ix0, const double y[9],
                        int iy0);
  extern double xdotc(int n, const double x[12], int ix0, const double y[12],
                      int iy0);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for xdotc.h
 *
 * [EOF]
 */
