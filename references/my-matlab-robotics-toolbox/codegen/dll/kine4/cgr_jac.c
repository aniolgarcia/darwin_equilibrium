/*
 * File: cgr_jac.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"

/* Function Definitions */

/*
 * Compute jacobian of a serial robot numerically, works for both prismatic
 *  and revolute joint
 *  THIS DOES NOT CHANGE STRUCTURE OF THE ROBOT!
 *
 *  r is the structure of the serial robot.
 *  q is the joint values (radians or meters).
 * Arguments    : const struct0_T *r
 *                double q[4]
 *                double jac[12]
 * Return Type  : void
 */
void cgr_jac(const struct0_T *r, double q[4], double jac[12])
{
  double unusedU0[9];
  double f0[3];
  int i;
  double qc0[4];
  int b_i;
  double f1[3];

  /*  Caclulate f0, when no perturbation happens */
  cgr_fkine_ee(r, q, unusedU0, f0);

  /*  Do perturbation */
  for (i = 0; i < 4; i++) {
    qc0[i] = q[i];
  }

  for (i = 0; i < 4; i++) {
    for (b_i = 0; b_i < 4; b_i++) {
      q[b_i] = qc0[b_i];
    }

    q[i] = qc0[i] + 1.0E-6;
    cgr_fkine_ee(r, q, unusedU0, f1);
    for (b_i = 0; b_i < 3; b_i++) {
      jac[b_i + 3 * i] = (f1[b_i] - f0[b_i]) * 1.0E+6;
    }
  }
}

/*
 * File trailer for cgr_jac.c
 *
 * [EOF]
 */
