/*
 * File: cgr_self_update.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"

/* Function Definitions */

/*
 * Arguments    : struct0_T *r
 *                const double qc[4]
 *                const double base[3]
 * Return Type  : void
 */
void cgr_self_update(struct0_T *r, const double qc[4], const double base[3])
{
  int i;
  struct0_T b_r;
  struct0_T c_r;
  struct0_T d_r;
  struct0_T e_r;
  double b_qc[4];
  for (i = 0; i < 3; i++) {
    r->base[i] = base[i];
  }

  for (i = 0; i < 4; i++) {
    r->qc[i] = qc[i];
  }

  b_r = *r;
  c_r = b_r;
  d_r = c_r;
  cgr_fkine(&d_r, qc, r->T);
  for (i = 0; i < 4; i++) {
    b_qc[i] = qc[i];
  }

  e_r = *r;
  d_r = e_r;
  cgr_jac(&d_r, b_qc, r->jac);
}

/*
 * File trailer for cgr_self_update.c
 *
 * [EOF]
 */
