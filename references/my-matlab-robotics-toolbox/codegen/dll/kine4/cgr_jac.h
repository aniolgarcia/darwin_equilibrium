/*
 * File: cgr_jac.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef CGR_JAC_H
#define CGR_JAC_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void cgr_jac(const struct0_T *r, double q[4], double jac[12]);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for cgr_jac.h
 *
 * [EOF]
 */
