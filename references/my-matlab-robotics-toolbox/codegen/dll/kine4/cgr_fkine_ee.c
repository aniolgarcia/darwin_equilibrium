/*
 * File: cgr_fkine_ee.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"

/* Function Definitions */

/*
 * This functions calculate tip position and orientation of the last link
 *  from given joint values.
 *  THIS DOES NOT CHANGE STRUCTURE OF THE ROBOT!
 *
 *  r is the structure of the serial robot.
 *  q is the joint values (radians or meters).
 *  T contains the homogenous transformation matrix of each link starting
 *  from the base T(:,:,1) to the end-effector T(:,:,n+1).
 *
 *  This gives the same result as the RVC toolbox.
 *
 *  Contact: manurung.auralius@gmail.com
 *
 *  References:
 *  https://www.cs.duke.edu/brd/Teaching/Bio/asmb/current/Papers/chap3-forward-kinematics.pdf
 *  See page 75
 * Arguments    : const struct0_T *r
 *                const double q[4]
 *                double R[9]
 *                double p[3]
 * Return Type  : void
 */
void cgr_fkine_ee(const struct0_T *r, const double q[4], double R[9], double p[3])
{
  struct0_T b_r;
  double T[64];
  int i3;
  int i4;
  b_r = *r;
  cgr_fkine(&b_r, q, T);
  for (i3 = 0; i3 < 3; i3++) {
    for (i4 = 0; i4 < 3; i4++) {
      R[i4 + 3 * i3] = T[48 + (i4 + (i3 << 2))];
    }

    p[i3] = T[60 + i3];
  }
}

/*
 * File trailer for cgr_fkine_ee.c
 *
 * [EOF]
 */
