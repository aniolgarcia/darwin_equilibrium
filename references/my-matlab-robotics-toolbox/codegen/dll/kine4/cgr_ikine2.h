/*
 * File: cgr_ikine2.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef CGR_IKINE2_H
#define CGR_IKINE2_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void cgr_ikine2(const struct0_T *r, const double p[3], double lambda,
    double treshold, double max_iter, double q[4], double *iter_taken, double
    *err);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for cgr_ikine2.h
 *
 * [EOF]
 */
