/*
 * File: cgr_ikine1.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "norm.h"
#include "pinv.h"

/* Function Definitions */

/*
 * Using pseudo-inverse method
 *  https://groups.csail.mit.edu/drl/journal_club/papers/033005/buss-2004.pdf
 *  See Equ. 7.
 *  r is the sructure of the robot.
 *  p (3x1) is the target cartesian position;
 *
 *  THIS DOES NOT CHANGE STRUCTURE OF THE ROBOT!
 * Arguments    : const struct0_T *r
 *                const double p[3]
 *                double treshold
 *                double max_iter
 *                double q[4]
 *                double *iter_taken
 *                double *err
 * Return Type  : void
 */
void cgr_ikine1(const struct0_T *r, const double p[3], double treshold, double
                max_iter, double q[4], double *iter_taken, double *err)
{
  int i;
  struct0_T b_r;
  double T[64];
  double x[3];
  double c_r[4];
  double jac[12];
  double a[12];
  double delta_x[3];
  int k;
  double b_q;
  double unusedU0[9];

  /*  Get current pose. */
  for (i = 0; i < 4; i++) {
    q[i] = r->qc[i];
  }

  b_r = *r;
  cgr_fkine(&b_r, r->qc, T);
  for (i = 0; i < 3; i++) {
    x[i] = T[60 + i];
  }

  for (i = 0; i < 4; i++) {
    c_r[i] = r->qc[i];
  }

  cgr_jac(r, c_r, jac);
  *iter_taken = 1.0;
  do {
    (*iter_taken)++;
    for (i = 0; i < 3; i++) {
      delta_x[i] = p[i] - x[i];
    }

    pinv(jac, a);
    for (k = 0; k < 4; k++) {
      b_q = 0.0;
      for (i = 0; i < 3; i++) {
        b_q += a[k + (i << 2)] * delta_x[i];
      }

      b_q += q[k];
      if ((b_q > r->lb[k]) || rtIsNaN(r->lb[k])) {
        c_r[k] = b_q;
      } else {
        c_r[k] = r->lb[k];
      }
    }

    for (k = 0; k < 4; k++) {
      if ((r->ub[k] < c_r[k]) || rtIsNaN(c_r[k])) {
        q[k] = r->ub[k];
      } else {
        q[k] = c_r[k];
      }
    }

    /*  This is a saturation function. */
    cgr_fkine_ee(r, q, unusedU0, x);
    for (i = 0; i < 4; i++) {
      c_r[i] = q[i];
    }

    cgr_jac(r, c_r, jac);
    *err = norm(delta_x);
  } while (!((*err < treshold) || (*iter_taken > max_iter)));

  /* fprintf('**cgr_ikine1** breaks after %i iterations with errror %f.\n', k, err); */
}

/*
 * File trailer for cgr_ikine1.c
 *
 * [EOF]
 */
