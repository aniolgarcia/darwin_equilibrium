/*
 * File: main.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

/*************************************************************************/
/* This automatically generated example C main file shows how to call    */
/* entry-point functions that MATLAB Coder generated. You must customize */
/* this file for your application. Do not modify this file directly.     */
/* Instead, make a copy of this file, modify it, and integrate it into   */
/* your development environment.                                         */
/*                                                                       */
/* This file initializes entry-point function arguments to a default     */
/* size and value before calling the entry-point functions. It does      */
/* not store or use any values returned from the entry-point functions.  */
/* If necessary, it does pre-allocate memory for returned values.        */
/* You can use this file as a starting point for a main function that    */
/* you can deploy in your application.                                   */
/*                                                                       */
/* After you copy the file, and before you deploy it, you must make the  */
/* following changes:                                                    */
/* * For variable-size function arguments, change the example sizes to   */
/* the sizes that your application requires.                             */
/* * Change the example values of function arguments to the values that  */
/* your application requires.                                            */
/* * If the entry-point functions return values, store these values or   */
/* otherwise use them as required by your application.                   */
/*                                                                       */
/*************************************************************************/
/* Include Files */
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "main.h"
#include "kine4_terminate.h"
#include "kine4_initialize.h"

/* Function Declarations */
static void argInit_1x4_char_T(char result[4]);
static void argInit_1x4_real_T(double result[4]);
static void argInit_3x1_real_T(double result[3]);
static void argInit_3x4_real_T(double result[12]);
static void argInit_4x1_real_T(double result[4]);
static void argInit_4x4x4_real_T(double result[64]);
static char argInit_char_T(void);
static double argInit_real_T(void);
static void argInit_struct0_T(struct0_T *result);
static void main_cgr_create(void);
static void main_cgr_fkine(void);
static void main_cgr_fkine_ee(void);
static void main_cgr_ikine1(void);
static void main_cgr_ikine2(void);
static void main_cgr_jac(void);
static void main_cgr_self_update(void);

/* Function Definitions */

/*
 * Arguments    : char result[4]
 * Return Type  : void
 */
static void argInit_1x4_char_T(char result[4])
{
  int idx1;

  /* Loop over the array to initialize each element. */
  for (idx1 = 0; idx1 < 4; idx1++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx1] = argInit_char_T();
  }
}

/*
 * Arguments    : double result[4]
 * Return Type  : void
 */
static void argInit_1x4_real_T(double result[4])
{
  int idx1;

  /* Loop over the array to initialize each element. */
  for (idx1 = 0; idx1 < 4; idx1++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx1] = argInit_real_T();
  }
}

/*
 * Arguments    : double result[3]
 * Return Type  : void
 */
static void argInit_3x1_real_T(double result[3])
{
  int idx0;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 3; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0] = argInit_real_T();
  }
}

/*
 * Arguments    : double result[12]
 * Return Type  : void
 */
static void argInit_3x4_real_T(double result[12])
{
  int idx0;
  int idx1;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 3; idx0++) {
    for (idx1 = 0; idx1 < 4; idx1++) {
      /* Set the value of the array element.
         Change this value to the value that the application requires. */
      result[idx0 + 3 * idx1] = argInit_real_T();
    }
  }
}

/*
 * Arguments    : double result[4]
 * Return Type  : void
 */
static void argInit_4x1_real_T(double result[4])
{
  int idx0;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 4; idx0++) {
    /* Set the value of the array element.
       Change this value to the value that the application requires. */
    result[idx0] = argInit_real_T();
  }
}

/*
 * Arguments    : double result[64]
 * Return Type  : void
 */
static void argInit_4x4x4_real_T(double result[64])
{
  int idx0;
  int idx1;
  int idx2;

  /* Loop over the array to initialize each element. */
  for (idx0 = 0; idx0 < 4; idx0++) {
    for (idx1 = 0; idx1 < 4; idx1++) {
      for (idx2 = 0; idx2 < 4; idx2++) {
        /* Set the value of the array element.
           Change this value to the value that the application requires. */
        result[(idx0 + (idx1 << 2)) + (idx2 << 4)] = argInit_real_T();
      }
    }
  }
}

/*
 * Arguments    : void
 * Return Type  : char
 */
static char argInit_char_T(void)
{
  return '?';
}

/*
 * Arguments    : void
 * Return Type  : double
 */
static double argInit_real_T(void)
{
  return 0.0;
}

/*
 * Arguments    : struct0_T *result
 * Return Type  : void
 */
static void argInit_struct0_T(struct0_T *result)
{
  /* Set the value of each structure field.
     Change this value to the value that the application requires. */
  argInit_3x1_real_T(result->base);
  argInit_1x4_real_T(result->theta);
  argInit_1x4_real_T(result->d);
  argInit_1x4_real_T(result->a);
  argInit_1x4_real_T(result->alpha);
  argInit_1x4_real_T(result->offset);
  argInit_4x1_real_T(result->qc);
  argInit_1x4_char_T(result->type);
  argInit_3x4_real_T(result->jac);
  argInit_4x4x4_real_T(result->T);
  argInit_4x1_real_T(result->ub);
  argInit_4x1_real_T(result->lb);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_cgr_create(void)
{
  double dv0[4];
  double dv1[4];
  double dv2[4];
  double dv3[4];
  double dv4[4];
  char cv0[4];
  double dv5[3];
  double dv6[4];
  double dv7[4];
  struct0_T robot;

  /* Initialize function 'cgr_create' input arguments. */
  /* Initialize function input argument 'theta'. */
  /* Initialize function input argument 'd'. */
  /* Initialize function input argument 'a'. */
  /* Initialize function input argument 'alpha'. */
  /* Initialize function input argument 'offset'. */
  /* Initialize function input argument 'type'. */
  /* Initialize function input argument 'base'. */
  /* Initialize function input argument 'ub'. */
  /* Initialize function input argument 'lb'. */
  /* Call the entry-point 'cgr_create'. */
  argInit_1x4_real_T(dv0);
  argInit_1x4_real_T(dv1);
  argInit_1x4_real_T(dv2);
  argInit_1x4_real_T(dv3);
  argInit_1x4_real_T(dv4);
  argInit_1x4_char_T(cv0);
  argInit_3x1_real_T(dv5);
  argInit_4x1_real_T(dv6);
  argInit_4x1_real_T(dv7);
  cgr_create(dv0, dv1, dv2, dv3, dv4, cv0, dv5, dv6, dv7, &robot);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_cgr_fkine(void)
{
  struct0_T r0;
  double dv8[4];
  double T[64];

  /* Initialize function 'cgr_fkine' input arguments. */
  /* Initialize function input argument 'r'. */
  /* Initialize function input argument 'q'. */
  /* Call the entry-point 'cgr_fkine'. */
  argInit_struct0_T(&r0);
  argInit_4x1_real_T(dv8);
  cgr_fkine(&r0, dv8, T);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_cgr_fkine_ee(void)
{
  struct0_T r1;
  double dv9[4];
  double R[9];
  double p[3];

  /* Initialize function 'cgr_fkine_ee' input arguments. */
  /* Initialize function input argument 'r'. */
  /* Initialize function input argument 'q'. */
  /* Call the entry-point 'cgr_fkine_ee'. */
  argInit_struct0_T(&r1);
  argInit_4x1_real_T(dv9);
  cgr_fkine_ee(&r1, dv9, R, p);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_cgr_ikine1(void)
{
  struct0_T r2;
  double dv10[3];
  double q[4];
  double iter_taken;
  double err;

  /* Initialize function 'cgr_ikine1' input arguments. */
  /* Initialize function input argument 'r'. */
  /* Initialize function input argument 'p'. */
  /* Call the entry-point 'cgr_ikine1'. */
  argInit_struct0_T(&r2);
  argInit_3x1_real_T(dv10);
  cgr_ikine1(&r2, dv10, argInit_real_T(), argInit_real_T(), q, &iter_taken, &err);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_cgr_ikine2(void)
{
  struct0_T r3;
  double dv11[3];
  double q[4];
  double iter_taken;
  double err;

  /* Initialize function 'cgr_ikine2' input arguments. */
  /* Initialize function input argument 'r'. */
  /* Initialize function input argument 'p'. */
  /* Call the entry-point 'cgr_ikine2'. */
  argInit_struct0_T(&r3);
  argInit_3x1_real_T(dv11);
  cgr_ikine2(&r3, dv11, argInit_real_T(), argInit_real_T(), argInit_real_T(), q,
             &iter_taken, &err);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_cgr_jac(void)
{
  struct0_T r4;
  double dv12[4];
  double jac[12];

  /* Initialize function 'cgr_jac' input arguments. */
  /* Initialize function input argument 'r'. */
  /* Initialize function input argument 'q'. */
  /* Call the entry-point 'cgr_jac'. */
  argInit_struct0_T(&r4);
  argInit_4x1_real_T(dv12);
  cgr_jac(&r4, dv12, jac);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
static void main_cgr_self_update(void)
{
  struct0_T r;
  double dv13[4];
  double dv14[3];

  /* Initialize function 'cgr_self_update' input arguments. */
  /* Initialize function input argument 'r'. */
  /* Initialize function input argument 'qc'. */
  /* Initialize function input argument 'base'. */
  /* Call the entry-point 'cgr_self_update'. */
  argInit_struct0_T(&r);
  argInit_4x1_real_T(dv13);
  argInit_3x1_real_T(dv14);
  cgr_self_update(&r, dv13, dv14);
}

/*
 * Arguments    : int argc
 *                const char * const argv[]
 * Return Type  : int
 */
int main(int argc, const char * const argv[])
{
  (void)argc;
  (void)argv;

  /* Initialize the application.
     You do not need to do this more than one time. */
  kine4_initialize();

  /* Invoke the entry-point functions.
     You can call entry-point functions multiple times. */
  main_cgr_create();
  main_cgr_fkine();
  main_cgr_fkine_ee();
  main_cgr_ikine1();
  main_cgr_ikine2();
  main_cgr_jac();
  main_cgr_self_update();

  /* Terminate the application.
     You do not need to do this more than one time. */
  kine4_terminate();
  return 0;
}

/*
 * File trailer for main.c
 *
 * [EOF]
 */
