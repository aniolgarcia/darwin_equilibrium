/*
 * File: sqrt.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef SQRT_H
#define SQRT_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void b_sqrt(double *x);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for sqrt.h
 *
 * [EOF]
 */
