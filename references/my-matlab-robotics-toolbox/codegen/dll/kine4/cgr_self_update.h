/*
 * File: cgr_self_update.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef CGR_SELF_UPDATE_H
#define CGR_SELF_UPDATE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void cgr_self_update(struct0_T *r, const double qc[4], const double
    base[3]);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for cgr_self_update.h
 *
 * [EOF]
 */
