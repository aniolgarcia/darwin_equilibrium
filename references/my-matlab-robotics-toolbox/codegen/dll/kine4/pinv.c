/*
 * File: pinv.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

/* Include Files */
#include <math.h>
#include "rt_nonfinite.h"
#include "cgr_create.h"
#include "cgr_fkine.h"
#include "cgr_fkine_ee.h"
#include "cgr_ikine1.h"
#include "cgr_ikine2.h"
#include "cgr_jac.h"
#include "cgr_self_update.h"
#include "pinv.h"
#include "svd.h"

/* Function Definitions */

/*
 * Arguments    : const double A[12]
 *                double X[12]
 * Return Type  : void
 */
void pinv(const double A[12], double X[12])
{
  int i5;
  boolean_T p;
  int vcol;
  int br;
  double b_A[12];
  double b_X[12];
  double U[12];
  double s[3];
  double V[9];
  double absxk;
  int exponent;
  int r;
  int ar;
  int ib;
  int ia;
  for (i5 = 0; i5 < 3; i5++) {
    for (vcol = 0; vcol < 4; vcol++) {
      b_A[vcol + (i5 << 2)] = A[i5 + 3 * vcol];
    }
  }

  p = true;
  for (br = 0; br < 12; br++) {
    b_X[br] = 0.0;
    if (p && ((!rtIsInf(b_A[br])) && (!rtIsNaN(b_A[br])))) {
      p = true;
    } else {
      p = false;
    }
  }

  if (!p) {
    for (i5 = 0; i5 < 12; i5++) {
      b_X[i5] = rtNaN;
    }
  } else {
    svd(b_A, U, s, V);
    absxk = fabs(s[0]);
    if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
      if (absxk <= 2.2250738585072014E-308) {
        absxk = 4.94065645841247E-324;
      } else {
        frexp(absxk, &exponent);
        absxk = ldexp(1.0, exponent - 53);
      }
    } else {
      absxk = rtNaN;
    }

    absxk *= 4.0;
    r = 0;
    br = 1;
    while ((br < 4) && (s[br - 1] > absxk)) {
      r++;
      br++;
    }

    if (r > 0) {
      vcol = 0;
      for (exponent = 1; exponent <= r; exponent++) {
        absxk = 1.0 / s[exponent - 1];
        for (br = vcol; br < vcol + 3; br++) {
          V[br] *= absxk;
        }

        vcol += 3;
      }

      for (exponent = 0; exponent <= 10; exponent += 3) {
        for (vcol = exponent; vcol < exponent + 3; vcol++) {
          b_X[vcol] = 0.0;
        }
      }

      br = -1;
      for (exponent = 0; exponent <= 10; exponent += 3) {
        ar = -1;
        br++;
        i5 = (br + ((r - 1) << 2)) + 1;
        for (ib = br; ib + 1 <= i5; ib += 4) {
          if (U[ib] != 0.0) {
            ia = ar;
            for (vcol = exponent; vcol < exponent + 3; vcol++) {
              ia++;
              b_X[vcol] += U[ib] * V[ia];
            }
          }

          ar += 3;
        }
      }
    }
  }

  for (i5 = 0; i5 < 3; i5++) {
    for (vcol = 0; vcol < 4; vcol++) {
      X[vcol + (i5 << 2)] = b_X[i5 + 3 * vcol];
    }
  }
}

/*
 * File trailer for pinv.c
 *
 * [EOF]
 */
