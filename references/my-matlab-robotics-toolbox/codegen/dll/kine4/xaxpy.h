/*
 * File: xaxpy.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef XAXPY_H
#define XAXPY_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void b_xaxpy(int n, double a, const double x[12], int ix0, double y[4],
                      int iy0);
  extern void c_xaxpy(int n, double a, const double x[4], int ix0, double y[12],
                      int iy0);
  extern void d_xaxpy(int n, double a, int ix0, double y[9], int iy0);
  extern void xaxpy(int n, double a, int ix0, double y[12], int iy0);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for xaxpy.h
 *
 * [EOF]
 */
