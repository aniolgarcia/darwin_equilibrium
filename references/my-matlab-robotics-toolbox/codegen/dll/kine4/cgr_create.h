/*
 * File: cgr_create.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef CGR_CREATE_H
#define CGR_CREATE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void cgr_create(const double theta[4], const double d[4], const double
    a[4], const double alpha[4], const double offset[4], const char type[4],
    const double base[3], const double ub[4], const double lb[4], struct0_T
    *robot);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for cgr_create.h
 *
 * [EOF]
 */
