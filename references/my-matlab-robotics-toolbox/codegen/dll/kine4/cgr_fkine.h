/*
 * File: cgr_fkine.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef CGR_FKINE_H
#define CGR_FKINE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void cgr_fkine(struct0_T *r, const double q[4], double T[64]);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for cgr_fkine.h
 *
 * [EOF]
 */
