/*
 * File: svd.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 03-May-2018 17:39:19
 */

#ifndef SVD_H
#define SVD_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "kine4_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void svd(const double A[12], double U[12], double s[3], double V[9]);

#ifdef __cplusplus

}
#endif
#endif

/*
 * File trailer for svd.h
 *
 * [EOF]
 */
