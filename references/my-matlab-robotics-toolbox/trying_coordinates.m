clc;
clear all;
g = ncgr_graphic();

global N_DOFS;
N_DOFS = 2;

theta =[0; 0];
alpha = [-pi/2; 0];
offset = [0; 0];
a = [0; 1];
d = [0; 0];
type = ['r','r'];
base = [0; 0; 0];
lb = [pi; pi];
ub = [-pi; -pi];

prob = cgr_create(theta, d, a, alpha, offset, type, base, ub, lb);
prob = cgr_self_update(prob, [0; 0]);
g = ncgr_plot(g, prob);