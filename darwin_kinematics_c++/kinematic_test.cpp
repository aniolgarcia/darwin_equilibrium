#include "darwin_robot.h"
#include "darwin_leg_kinematics.h"
#include "darwin_robot_exceptions.h"
#include "action_id.h"

#include <iostream>
#include <cmath>
#include <time.h>

#define PI 3.141592
#define FEET_DIST 0.074
#define LEG_LENGHT 0.2195
#define MAX_SPEED 50

std::string robot_device="/dev/pts/7";
std::string kin_file_r="/home/humanoide/humanoids/darwin_robot_driver/src/xml/right_leg.xml";
std::string kin_file_l="/home/humanoide/humanoids/darwin_robot_driver/src/xml/left_leg.xml";


int main(int argc,char *argv[])
{
    srand(time(NULL));
    int i;
    std::vector<unsigned char> servos;
    std::vector<double> angles,speeds,accels;
    std::vector<double> joint_ang;
    std::vector<double> start_joints_l, start_pos_l(3,0), start_rot_l(3,0), final_pos_l(3,0), final_rot_l(3,0), target_joints_l;
  

    try
    {
        //Inicialization
        CDarwinRobot darwin("Darwin",robot_device,1000000,0x02,true);
        int n_servos = darwin.mm_get_num_servos();
        unsigned int present_servos = darwin.mm_get_present_servos();
/*        darwin.mm_enable_power();
        
        for(i = 0; i < n_servos; i++)
        {
            if(present_servos&(0x00000001<<i))
            {
                darwin.mm_enable_servo(i);
                darwin.mm_assign_module(i, DARWIN_MM_ACTION);
            }
        }
        darwin.mm_start();
  */      
        CDarwinLegKinematics left_leg_kin;
        left_leg_kin.load_chain(kin_file_r);

        /* 
        start_pos_l[0] = 0.05;
        start_pos_l[1] = 0.05;
        start_pos_l[2] = 0.1;

        start_pos_l[2] -= 0.0315 + 0.251;
        
        start_rot_l[0] = 0.3;
        start_rot_l[1] = 0.2;
        start_rot_l[2] = 0.1;
        */
        /*  start_joints_l.push_back(-0.58776);
        start_joints_l.push_back(-1.34447);
        start_joints_l.push_back(-1.41636);
        start_joints_l.push_back(-0.972503);
        start_joints_l.push_back(0.0886021); 
        start_joints_l.push_back(-0.37646);
    
*/
        std::cout << "Start angles: ";
        for(i = 0; i < 6; i++)
        {
            double ran = -PI/2 + static_cast<float>(rand())/ static_cast<float>(RAND_MAX/(PI));
            start_joints_l.push_back(ran);
            std::cout << start_joints_l[i] << " ";
        }
        std::cout << std::endl;
       
        left_leg_kin.get_forward_kinematics(start_joints_l, start_pos_l, start_rot_l);

        std::cout << "Start position: ";
        for(i = 0; i < start_pos_l.size(); i++)
        {
           std::cout << start_pos_l[i] << " ";
        } 
        std::cout << "    Start orientation: ";
        for(i = 0; i < start_rot_l.size(); i++)
        {
           std::cout << start_rot_l[i] << " ";
        }
        


        std::cout << std::endl;

        
        left_leg_kin.get_inverse_kinematics(start_pos_l,start_rot_l, target_joints_l);


        //Cama dreta: posar a negetius els angles 0 i 1.
	//Cama esquerra: tots a negatius excepte 5. 
        target_joints_l[0] = -target_joints_l[0];
        target_joints_l[1] = -target_joints_l[1];
        target_joints_l[2] = target_joints_l[2];
        target_joints_l[3] = target_joints_l[3];
        target_joints_l[4] = target_joints_l[4];
      	target_joints_l[5] = target_joints_l[5]; 

        std::cout << "Final angles: ";
        for(i = 0; i < target_joints_l.size(); i++)
        {
            std::cout << target_joints_l[i] << " ";
        }
        std::cout << std::endl;
	
        left_leg_kin.get_forward_kinematics(target_joints_l, final_pos_l, final_rot_l); 

        std::cout << "Final position: ";
        for(i = 0; i < final_pos_l.size(); i++)
        {
            std::cout << final_pos_l[i] << " ";
        }
        std::cout << "    Final orientation: ";
        for(i = 0; i < final_rot_l.size(); i++)
        {
            std::cout << final_rot_l[i] << " ";
        }
        std::cout << std::endl;
	std::cout << "Correcció angles (1e-3): ";
	for(i = 0; i < target_joints_l.size(); i++)
	{
		std::cout << (std::fabs(target_joints_l[i] - start_joints_l[i]) < 1e-3) << " ";
	}
	std::cout << std::endl;	
	
	std::cout << "Correcició pos/rot (1e-3): ";
	for(i = 0; i < final_pos_l.size(); i++)
	{
		std::cout << (std::fabs(final_pos_l[i] - start_pos_l[i]) < 1e-3) << " ";
	}
	for(i = 0; i < final_rot_l.size(); i++)
	{
		std::cout << (std::fabs(final_rot_l[i] - start_rot_l[i]) < 1e-3) << " ";
	}
	std::cout << std::endl;


//        darwin.mm_stop();
//        darwin.mm_disable_power();

    }catch(CException &e){
        std::cout << e.what() << std::endl; 
    }
}
		
	
