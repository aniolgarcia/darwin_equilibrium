#include "darwin_robot.h"
#include "darwin_leg_kinematics.h"
#include "darwin_robot_exceptions.h"

#include <iostream>
#include <cmath>

#define PI 3.141592
#define FEET_DIST 0.074
#define LEG_LENGHT 0.2195
#define MAX_SPEED 50

std::string robot_device="A4008atn";
std::string kin_file_r="xml/right_leg.xml";
std::string kin_file_l="xml/left_leg.xml";


int main(int argc,char *argv[])
{
    int n_servos, i, j;
    unsigned int present_servos
    double g_x; g_y; g_z;
    double x_angle, y_angle;
    double delta_height;
    double max_delta_angle_l, max_delta_angle_r;

    std::vector<unsigned char> servos_l = {8, 10, 12, 14, 16, 18};
    std::vector<unsigned char> servos r = {7, 9, 11, 13, 15, 17};

    std::vector<double> start_joints_l(6), start_pos_l, start_rot_l, target_pos_l, target_rot_l, target_joints_l;
    std::vector<double> start_joints_r(6), start_pos_r, start_rol_r, target_pos_r, target_rot_r; target_joints_r;
    std::vector<double> delta_angles_l(6), delta_angles_r(6); 

    std::vector<double> speeds_l(6), speeds_r(6), accels_l(6), accels_r(6);
 
    try
    {
        //Inicialization
        CDarwinRobot darwin("Darwin",robot_device,1000000,0x02);
        darwin.imu_start();
        
        //Get and compute rotation of the platform
        darwin.imu_get_accel_data(&g_x, &g_y, &g_z);
        x_angle = atan2(g_z, g_y);
        y_angle = atan2(g_z, g_x);

        //Compute the height that legs have to compensate
        delta_height = FEET_DIST/2 * sin(x_angle);

        //Load kinematic chain files
        CDarwinLegKinematics left_leg_kin;
        CDarwinLegKinematics rihgt_leg_kin;

        left_leg_kin.load_chain(kin_file_l);
        right_leg_kin.load_chain(kin_file_l);
        
        //Start servos and motion manager
        n_servos = darwin.mm_get_num_servos();
        present_servos = darwin.mm_get_present_servos();
        darwin.mm_enable_power();
        
        for(i = 0; i < MAX_NUM_SERVOS; i++)
        {
            if(present_servos&(0x00000001<<i))
            {
                darwin.mm_enable_servo(i);
                darwin.mm_assign_module(i, DARWIN_MM_JOINTS);
            }
        }

        darwin.mm_start();

        //Read the current angle of the servos
        for(i = 7; i <= 18; i++)
        {
            if(i%2)
            {
                start_joints_l.push_back(darwin.mm_get_servo_angle(i));
            }
            else
            {
                start_joints_r.push_back(darwin.mm_get_servo_angle(i));
            }
        }
   
        //Compute the current position of the legs (forward kinematics)
        left_leg_kin.get_forward_kinematics(start_joints_l, start_pos_l, start_rot_l);
        right_leg_kin.get_forward_kinematics(start_joints_r, start_pos_r, start_rot_r);
         
        target_pos_l = start_pos_l; 
        target_pos_r = start_pos_r;
        target_rot_l = start_rot_l;
        target_rot_r = start_rot_r;

        //Define the target position of the legs
        target_pos_l(2) += delta_height + LEG_LENGHT;
        target_rot_l(0) = x_angle;
        target_rot_l(1) = y_angle;

        target_pos_r(2) += -delta_height + LEG_LENGHT;
        target_rot_r(0) = x_angle;
        target_rot_r(1) = y_angle;

        //Compute the position of the angles for the target position (inverse kinematics) 
        left_leg_kin.get_inverse_kinematics(target_pos_l, target_rot_l, target_joints_l);
        right_leg_kin.get_inverse_kinematics(target_pos_r, target_rot_r, target_joints_r);

        //Compute the joint velocities
        for(i = 0; i < 6; i++)
        {
            delta_angles_l.push_back(abs(target_joints_l[i] - start_joints_l[i]));
            max_delta_angle_l = max(max_delta_angle_l, abs(target_joints_l[i] - start_joints_l[i]));

            delta_angles_r.push_back(abs(target_joints_r[i] - start_joints_r[i]));
            max_delta_angle_r = max(max_delta_angle_r, abs(target_joints_r[i] - start_joints_r[i]));
        }
            

        for(i = 0; i < 6; i++)
        {
            speeds_l[i] = (delta_angles_l*MAX_SPEED/max_delta_angle_l);
            speeds_r[i] = (delta_angles_r*MAX_SPEED/max_delta_angle_r);
            accels_l[i] = 50;
            accels_r[i] = 50;
        }

        //Loading joints
        darwin.joints_load(JOINTS_GRP0,servos_l, target_joints_l, speeds_l, accels_l);
        darwin.joints_load(JOINTS_GRP1,servos_r, target_joints_r, speeds_r, accels_r);

        //Moving joints
        darwin.joints_start(JOINTS_GRP0);
        darwin.joints_start(JOINTS_GRP1);
        
        while(darwin.joints_are_moving(JOINTS_GRP0) || darwin.joints_are_moving(JOINTS_GRP1))
        {
            usleep(100000);
        }
        
        //Stopping the robot
        darwin.mm_stop();
        darwin.mm_disable_power();

         

    }catch(CException &e){
        std::cout << e.what() << std::endl; 
    }


}
