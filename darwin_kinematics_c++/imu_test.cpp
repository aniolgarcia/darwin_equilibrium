#include "darwin_robot.h"
#include "darwin_leg_kinematics.h"
#include "darwin_robot_exceptions.h"
#include "bno055_imu_driver.h"

#include <iostream>
#include <cmath>

#define PI 3.141592
#define FEET_DIST 0.074
#define LEG_LENGHT 0.2195
#define MAX_SPEED 50

std::string robot_device="/dev/pts/21";

int main(int argc,char *argv[])
{
    int i;
    std::vector<double> start_angles;
    bool accel_cal,mag_cal,gyro_cal;
 	CBNO055IMUDriver imu("darwin_imu");
	imu.open("/dev/pts/23");
	try{
		imu.load_calibration("bno055.cal");
		do{
			imu.set_operation_mode(ndof_mode);
			accel_cal = imu.is_accelerometer_calibrated();
			mag_cal = imu.is_magnetometer_calibrated();
			gyro_cal = imu.is_gyroscope_calibrated();
			sleep(1);
		}while(!accel_cal || !mag_cal || !gyro_cal);
	}catch(CException &e){
		std::cout << e.what() << std::endl;
		do{
			imu.set_operation_mode(ndof_mode);
			accel_cal = imu.is_accelerometer_calibrated();
			mag_cal = imu.is_magnetometer_calibrated();
			gyro_cal = imu.is_gyroscope_calibrated();
			sleep(1);
		}while(!accel_cal || !mag_cal || !gyro_cal);
		imu.save_calibration("bno055.cal");
	}
	//events.push_back(imu.get_new_data_event_id());
       
    //Get initial angles
    while(1)
    {
        start_angles = imu.get_orientation_euler();
        std::cout << "angles: [ ";
        for(i = 0; i < start_angles.size(); i++)
        {
            std::cout << start_angles[i] << " ";
        }
        std::cout << " ]" << std::endl;
        sleep(1);
    }
}

 
  
    
 
