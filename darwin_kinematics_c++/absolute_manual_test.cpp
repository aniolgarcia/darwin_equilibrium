#include "darwin_robot.h"
#include "darwin_leg_kinematics.h"
#include "darwin_robot_exceptions.h"
#include "bno055_imu_driver.h"
#include "action_id.h"

#include <iostream>
#include <cmath>

#define PI 3.141592
#define FEET_DIST 0.074
#define LEG_LENGHT 0.2195
#define MAX_SPEED 50

std::string robot_device="/dev/pts/21";
std::string kin_file_r="/home/humanoide/humanoids/darwin_robot_driver/src/xml/right_leg.xml";
std::string kin_file_l="/home/humanoide/humanoids/darwin_robot_driver/src/xml/left_leg.xml";

int main(int argc, char *argv[])
{
    int n_servos, i;
    unsigned int present_servos;
    double g_x, g_y, g_z;
    double x_angle, y_angle;
    double delta_height;
    double max_delta_angle_l, max_delta_angle_r;
    bool accel_cal,mag_cal,gyro_cal;
    std::vector<unsigned char> servos_l;
    std::vector<unsigned char> servos_r;
    std::vector<double> start_joints_l, start_pos_l, start_rot_l, target_pos_l, target_rot_l, target_joints_l;
    std::vector<double> start_joints_r, start_pos_r, start_rot_r, target_pos_r, target_rot_r, target_joints_r;
    std::vector<double> delta_angles_l, delta_angles_r; 
    std::vector<double> speeds_l(6), speeds_r(6), accels_l(6), accels_r(6);
    std::vector<double> start_angles, final_angles, turned_angles(3);

    servos_l.push_back(8);
    servos_l.push_back(10);
    servos_l.push_back(12);
    servos_l.push_back(14);
    servos_l.push_back(16);
    servos_l.push_back(18);

    servos_r.push_back(7); 
    servos_r.push_back(9); 
    servos_r.push_back(11); 
    servos_r.push_back(13); 
    servos_r.push_back(15); 
    servos_r.push_back(17); 


    try
    {
        //Inicialization
        CDarwinRobot darwin("Darwin",robot_device,1000000,0x02,true);
        //darwin.imu_start();
        CBNO055IMUDriver imu("darwin_imu");
        imu.open("/dev/pts/23");
        try{
            imu.load_calibration("bno055.cal");
            do{
                imu.set_operation_mode(ndof_mode);
                accel_cal = imu.is_accelerometer_calibrated();
                mag_cal = imu.is_magnetometer_calibrated();
                gyro_cal = imu.is_gyroscope_calibrated();
                sleep(1);
            }while(!accel_cal || !mag_cal || !gyro_cal);
        }catch(CException &e){
            std::cout << e.what() << std::endl;
            do{
                imu.set_operation_mode(ndof_mode);
                accel_cal = imu.is_accelerometer_calibrated();
                mag_cal = imu.is_magnetometer_calibrated();
                gyro_cal = imu.is_gyroscope_calibrated();
                sleep(1);
            }while(!accel_cal || !mag_cal || !gyro_cal);
            imu.save_calibration("bno055.cal");
        }

        for(i=0;i<MAX_NUM_SERVOS;i++)
        {
            darwin.mm_enable_servo(i);
            darwin.mm_assign_module(i,DARWIN_MM_ACTION);
        }
        darwin.mm_start();
        darwin.action_load_page(WALK_READY);
        darwin.action_start();
        while(darwin.action_is_page_running())
        {
            std::cout << "action running ... " << std::endl;
            usleep(100000);
        }

        //Get inital robot orientataion
        start_angles = imu.get_orientation_euler();

        std::cout << "Start angles: [ ";
        for(i = 0; i < start_angles.size(); i++)
        {
            std::cout << start_angles[i] << " ";
        }
        std::cout << " ]" << std::endl;


        //Get initial servo angles

        std::cout << "Starting servos and motion manager..." << std::endl;
        n_servos = darwin.mm_get_num_servos();
        present_servos = darwin.mm_get_present_servos();
        darwin.mm_enable_power();

        for(i = 0; i < MAX_NUM_SERVOS; i++)
        {
            if(present_servos&(0x00000001<<i))
            {
                darwin.mm_enable_servo(i);
                darwin.mm_assign_module(i, DARWIN_MM_JOINTS);
            }
        }

        darwin.mm_start();

        //Read the current angle of the servos
        start_joints_l.push_back(darwin.mm_get_servo_angle(8));
        start_joints_l.push_back(darwin.mm_get_servo_angle(10));
        start_joints_l.push_back(darwin.mm_get_servo_angle(12));
        start_joints_l.push_back(darwin.mm_get_servo_angle(14));
        start_joints_l.push_back(darwin.mm_get_servo_angle(16));
        start_joints_l.push_back(darwin.mm_get_servo_angle(18));

        start_joints_r.push_back(darwin.mm_get_servo_angle(7));
        start_joints_r.push_back(darwin.mm_get_servo_angle(9));
        start_joints_r.push_back(darwin.mm_get_servo_angle(11));
        start_joints_r.push_back(darwin.mm_get_servo_angle(13));
        start_joints_r.push_back(darwin.mm_get_servo_angle(15));
        start_joints_r.push_back(darwin.mm_get_servo_angle(17));

        std::cout << "Left leg angles:   [ ";
        for(i = 0; i < start_joints_l.size(); i++)
        {
            start_joints_l[i]*=PI/180;
            std::cout << start_joints_l[i]<< " ";
        }
        std::cout << "]" << std::endl;

        std::cout << "Right leg angles:  [ ";
        for(i = 0; i < start_joints_r.size(); i++)
        {
            start_joints_r[i]*=PI/180;
            std::cout << start_joints_r[i] << " ";
        }
        std::cout << "]" << std::endl << std::endl;


        //Get initial robot position

        std::cout << "Loading kinematic chains... " << std::endl;
        CDarwinLegKinematics left_leg_kin;
        CDarwinLegKinematics right_leg_kin;

        left_leg_kin.load_chain(kin_file_l);
        right_leg_kin.load_chain(kin_file_r); 


        left_leg_kin.get_forward_kinematics(start_joints_l, start_pos_l, start_rot_l);
        right_leg_kin.get_forward_kinematics(start_joints_r, start_pos_r, start_rot_r);

        std::cout << "Left leg position: [" << start_pos_l[0] << " " << start_pos_l[1] << " " << start_pos_l[2] << " " << start_rot_l[0] << " " << start_rot_l[1] << " " << start_rot_l[2] << "]" << std::endl;
        std::cout << "Right leg position:[" << start_pos_r[0] << " " << start_pos_r[1] << " " << start_pos_r[2] << " " << start_rot_r[0] << " " << start_rot_r[1] << " " << start_rot_r[2] << "]" << std::endl << std::endl;


        //Get desired positionii
        while(1)
        {

            std::cout << "Absolute x angle: ";
            std::cin >> x_angle;
            std::cout << std::endl << "Absolute y angle: ";
            std::cin >> y_angle;
            std::cout << std::endl;

            //Compute the height that legs have to compensate
            delta_height = FEET_DIST/2 * sin(x_angle);
            std::cout << "Height to correct: " << delta_height << std::endl;

            target_pos_l = start_pos_l; 
            target_pos_r = start_pos_r;
            target_rot_l = start_rot_l;
            target_rot_r = start_rot_r;

            //Define the target position of the legs
            target_pos_l[2] += delta_height;
            target_rot_l[0] += x_angle;
            target_rot_l[1] += y_angle;

            target_pos_r[2] += -delta_height;
            target_rot_r[0] += x_angle;
            target_rot_r[1] += y_angle;

            for(i = 0; i < target_pos_r.size(); i++)
            {
                std::cout << target_pos_r[i] << " ";
            }

            for(i = 0; i < target_rot_r.size(); i++)
            {
                std::cout << target_rot_r[i] << " ";
            }

            //Compute the position of the angles for the target position (inverse kinematics) 
            std::cout << "Computing inverse kinematics..." << std::endl;
            try { 
                left_leg_kin.get_inverse_kinematics(target_pos_l, target_rot_l, target_joints_l);
                right_leg_kin.get_inverse_kinematics(target_pos_r, target_rot_r, target_joints_r);
            } catch(CException &e){
                std::cout << e.what() << std::endl;
            }

            std::cout << "Left legi ik angles:   [ ";
            for(i = 0; i < target_joints_l.size(); i++)
            {
                target_joints_l[i]*=1;
                std::cout << target_joints_l[i]<< " ";
            }
            std::cout << "]" << std::endl;

            std::cout << "Right leg ik angles:  [ ";
            for(i = 0; i < target_joints_r.size(); i++)
            {
                target_joints_r[i]*=1;
                std::cout << target_joints_r[i] << " ";
            }
            std::cout << "]" << std::endl;




            //Correct known error in signs of the inverse kinematics angle 
            for(i = 0; i < 5; i++)
            {
                target_joints_l[i] *= -1;
            }
            target_joints_r[0] *= -1;
	    target_joints_r[1] *= -1;

/*
            left_leg_kin.get_forward_kinematics(target_joints_l, start_pos_l, start_rot_l);
            right_leg_kin.get_forward_kinematics(target_joints_r, start_pos_r, start_rot_r);

            std::cout << "Left leg expected pos: [" << start_pos_l[0] << " " << start_pos_l[1] << " " << start_pos_l[2] << " " << start_rot_l[0] << " " << start_rot_l[1] << " " << start_rot_l[2] << "]" << std::endl;
            std::cout << "Right leg expected pos:[" << start_pos_r[0] << " " << start_pos_r[1] << " " << start_pos_r[2] << " " << start_rot_r[0] << " " << start_rot_r[1] << " " << start_rot_r[2] << "]" << std::endl << std::endl;
*/

            for(i = 0; i < 6; i++)
            {
                target_joints_l[i] *= 180/PI;
                target_joints_r[i] *= 180/PI;
            }
            //Compute the joint velocities
            for(i = 0; i < 6; i++)
            {
                delta_angles_l.push_back(abs(target_joints_l[i] - start_joints_l[i]));
                max_delta_angle_l = std::max(max_delta_angle_l, std::abs(target_joints_l[i] - start_joints_l[i]));

                delta_angles_r.push_back(abs(target_joints_r[i] - start_joints_r[i]));
                max_delta_angle_r = std::max(max_delta_angle_r, std::abs(target_joints_r[i] - start_joints_r[i]));
            }


            for(i = 0; i < 6; i++)
            {
                speeds_l[i] = 50;//(delta_angles_l[i]*MAX_SPEED/max_delta_angle_l);
                speeds_r[i] = 50;//(delta_angles_r[i]*MAX_SPEED/max_delta_angle_r);
                accels_l[i] = 50;
                accels_r[i] = 50;
            }

            //Moving right leg
            darwin.joints_load(JOINTS_GRP1,servos_r, target_joints_r, speeds_r, accels_r);
            darwin.joints_start(JOINTS_GRP1);
            while(darwin.joints_are_moving(JOINTS_GRP1))
            {
                usleep(100000);
            }

            //Moving left leg
            darwin.joints_load(JOINTS_GRP0,servos_l, target_joints_l, speeds_l, accels_l);
            darwin.joints_start(JOINTS_GRP0);       
            while(darwin.joints_are_moving(JOINTS_GRP0))
            {
                usleep(100000);
            }

            start_angles = imu.get_orientation_euler();
            std::cout << "Final angles: [ ";
            for(i = 0; i < start_angles.size(); i++)
            {
                std::cout << start_angles[i] << " ";
            }
            std::cout << " ]" << std::endl;


            //Stopping the robot
            darwin.mm_stop();
            darwin.mm_disable_power();




        }

    }catch(CException &e){
        std::cout << e.what() << std::endl; 
    }

}
