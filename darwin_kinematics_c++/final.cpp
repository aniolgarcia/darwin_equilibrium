#include "darwin_robot.h"
#include "darwin_leg_kinematics.h"
#include "darwin_robot_exceptions.h"
#include "bno055_imu_driver.h"
#include "action_id.h"

#include <iostream>
#include <cmath>

#define PI 3.141592
#define FEET_DIST 0.074
#define LEG_LENGHT 0.2195
#define MAX_SPEED 50

std::string robot_device="/dev/pts/7";
std::string kin_file_r="/home/humanoide/humanoids/darwin_robot_driver/src/xml/right_leg.xml";
std::string kin_file_l="/home/humanoide/humanoids/darwin_robot_driver/src/xml/left_leg.xml";

int main(int argc, char *argv[])
{
    int n_servos, i;
    unsigned int present_servos;
