function [ angles_left angles_right ] = equilibrium(x_angle,y_angle)
    %Returns the position of the legs given the angles of the slope in x
    %   and y axis.

    feet_dist = 0.074; %Separation between darwin feet in mm
    delta_height = feet_dist/2 * sin(x_angle); %Difference in heigth
    
    %Calculate the position of each leg given its angles
    start_pos_left = left_leg_kin3([0, 0, -1.0901, 2.1803, -1.0901, 0])
    start_pos_right= left_leg_kin3([0, 0, -1.0901, 2.1803, -1.0901, 0])
		
    %start_pos_left = left_leg_kin3([-0, -0, -0.75074, 1.50148,-0.75074, 0])
    %start_pos_right = left_leg_kin3([-0, -0, -0.75074, 1.50148,-0.75074, 0])

    % Decompose the angle
    rot = axang2rotm([0, cos(x_angle), sin(x_angle), y_angle])    
    angle = rotm2eul(rot)
    
    %Calculate the position where the legs should be
		
    final_pos_left = start_pos_left + [0 0 delta_height+0.2195 x_angle y_angle 0];
    final_pos_right = start_pos_right + [0 0 -delta_height+0.2195 x_angle y_angle 0];
    
    %Calculate and adjust the angles of the leg in respect of the IK
    %   reference frame
    angles_left = geometric_ik(final_pos_left(1), final_pos_left(2), final_pos_left(3), final_pos_left(4), final_pos_left(5), final_pos_left(6));
    angles_left(6) = -angles_left(6);
    angles_right = geometric_ik(final_pos_right(1), final_pos_right(2), final_pos_right(3), final_pos_right(4), final_pos_right(5), final_pos_right(6));
    angles_right(6) = -angles_right(6);
    
    % Uncomment the following three lines if you plan to put the angles on the
    % simulator:
    % angles_left(3) = -angles_left(3);
    % angles_left(4) = -angles_left(4);
    % angles_right(5) = -angles_right(5);
    
    % For debugging purposes
    final_pos_left = left_leg_kin3(angles_left)
    final_pos_right = left_leg_kin3(angles_right)
    
    %feet_angle_x = atan((final_pos_left(3) - final_pos_right(3))/(0.074*cos(x_angle)))
end

