function [ angles_left angles_right ] = equilibrium_c_test(start_pos_left, start_pos_right, x_angle,y_angle)
    %Returns the position of the legs given the angles of the slope in x
    %   and y axis.

    feet_dist = 0.074; %Separation between darwin feet in mm
    delta_height = feet_dist/2 * sin(x_angle); %Difference in heigth
    
 
    
    %start_pos_left =  [2.9935e-02   1.0587e-17  -2.2579e-01   1.0000e-01  -2.2348e-01   3.3743e-17]
    %start_pos_right =  [2.9935e-02   1.4253e-17  -2.3318e-01   1.0000e-01  -2.2348e-01   3.0620e-17]

    
    %Calculate the position where the legs should be
		
    final_pos_left = start_pos_left + [0 0 delta_height x_angle y_angle 0]
    final_pos_right = start_pos_right + [0 0 -delta_height x_angle y_angle 0]
    
    %Calculate and adjust the angles of the leg in respect of the IK
    %   reference frame
    angles_left = geometric_ik2(final_pos_left(1), final_pos_left(2), final_pos_left(3), final_pos_left(4), final_pos_left(5), final_pos_left(6));
    angles_left = -angles_left;
    angles_left(6) = -angles_left(6);
    angles_left
    
    angles_right = geometric_ik2(final_pos_right(1), final_pos_right(2), final_pos_right(3), final_pos_right(4), final_pos_right(5), final_pos_right(6));
    %angles_right =-angles_right;
    angles_right(6) =-angles_right(6);
    angles_right
    % Uncomment the following three lines if you plan to put the angles on the
    % simulator:
    % angles_left(3) = -angles_left(3);
    % angles_left(4) = -angles_left(4);
    % angles_right(5) = -angles_right(5);
    
    % For debugging purposes
    final_pos_left = left_leg_kin_c(angles_left)
    final_pos_right = right_leg_kin_c(angles_right)
    %abs(start_position - final_position) <= 0.0001
    %feet_angle_x = atan((final_pos_left(3) - final_pos_right(3))/(0.074*cos(x_angle)))
end


