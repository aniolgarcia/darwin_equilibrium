%Trying to put imu reference system with foot
function transformation = imu_reference(vec)


    rad = [0, 0, -1.0901, 2.1803, -1.0901, 0] %Joint angles for the default position

    %Computing direct kinematics from imu or base to end effector (foot)
    base_to_imu = homogeneous_complete(0, 0, -0.05, 0, 0, pi/2);
    base_to_py = [0 1 0 0.037; 0 0 1 -0.122; 1 0 0 -0.005; 0 0 0 1];    
    py_to_pr = denavit_hartenberg(0, -0.0315, -pi/2, -rad(1,1)+pi/2);
    pr_to_pp = denavit_hartenberg(0, 0, -pi/2, rad(1,2)+pi/2);
    pp_to_k = denavit_hartenberg(0.093, 0, 0, rad(1,3));
    k_to_fp = denavit_hartenberg(0.093, 0, pi, rad(1,4)); 
    fp_to_fr = denavit_hartenberg(0, 0, pi/2, rad(1,5));
    fr_to_endeff = denavit_hartenberg(0.0335, 0, 0, rad(1,6));
        
    %imu_to_base = inv(base_to_imu); %Euler angles given by imu are in the base reference system
    rot = eul2rotm_nsa(vec(1), vec(2), vec(3)); %Rotation matrix for the given euler angles
    rot = resize(rot, 4); %Resize rot matrix to a 4x4 and add a 1
    rot(4,4) = 1;
    dh_trans = rot*base_to_py*py_to_pr*pr_to_pp*pp_to_k*k_to_fp*fp_to_fr*fr_to_endeff %Transfomration from the rotated base to foot
    rotm2eul_nsa(dh_trans); %Obtaining euler angles form foot
    inv(dh_trans(1:3,1:3))*vec'; %Nothing useful.s
end