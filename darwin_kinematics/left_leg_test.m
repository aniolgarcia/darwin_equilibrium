%Comprovaci´o de que la cinem`atica funciona
%Generem 6 angles de manera aleatòria entre -pi/2 i pi/2
min = -pi/2;
max = pi/2;
r = (max - min).*rand(6,1) + min;
random_angles = r.'

start_position = left_leg_kin_c(random_angles) %Calculem pos i orientació amb aquests angles

%start_position = [0.1, 0, -0.1195, 0, 0, 0]
start_position(3) = start_position(3); %Passem la posició al frame de cinematica inversa

inverse_angles = geometric_ik2(start_position(1), start_position(2), start_position(3), start_position(4), start_position(5), start_position(6)); %Cinemàtica inversa donada posició i orientació
inverse_angles = -inverse_angles;
inverse_angles(6) = -inverse_angles(6); %No sé perquè però sempre canvia el signe. És consistent, almenys 
inverse_angles

final_position = left_leg_kin_c(inverse_angles) 

start_position(3) = start_position(3); %Tornem la posció inicial en funció del base frame
abs(start_position - final_position) <= 0.0001 %Comprovem si l'inicial i final són iguals (o molt semblants)