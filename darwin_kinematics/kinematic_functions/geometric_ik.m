function out = geometric_ik(x, y, z, a, b,  c)
%Returns the joint angles given the end effector translation and rotation
%   in respect to the base frame

    tad = homogeneous_complete(x, y, z-0.2195, a, b, c);
    
    vec = [x+tad(1,3)*0.0335 y+tad(2, 3)*0.0335 (z-0.2195)+tad(3,3)*0.0335];
    
    %Get knee angle
    rac = sqrt(vec(1,1).^2 + vec(1,2).^2 + vec(1,3).^2);
    arccos = acos(((rac.^2) -2*(0.093.^2))/(2*(0.093.^2)));
    out(1, 4) = arccos;
    
    %Get ankle roll
    tda = inv(tad);
    k = sqrt(tda(2, 4).^2 + tda(3, 4).^2);
    l = sqrt(tda(2, 4).^2 + (tda(3,4)-0.0335).^2);
    m = (k.^2 - l.^2 - 0.0335.^2)/(2*l*0.0335);
    if(m > 1)
        m = 1;
    end
    if(m < -1)
        m = -1;
    end
    arccos = acos(m);
    %He invertit els signes perquè sempre els donava al revés
    if(tda(2, 4) < 0)
        out(1, 6) = -arccos;
    else
        out(1, 6) = arccos;
    end
    
    %Get hip yaw
    tcd = homogeneous_complete(0, 0, -0.0335, out(1,6), 0, 0);
    tac = tad/tcd;
    arctan = atan2(-tac(1,2), tac(2,2));
    out(1, 1) = arctan;
    
    %Get hip roll
    arctan = atan2(tac(3,2), - tac(1,2)*sin(out(1,1))+tac(2,2)*cos(out(1,1)));
    out(1, 2) = arctan;
    
    %Get hip pitch and ankle pitch
    arctan = atan2(tac(1,3)*cos(out(1,1)) +tac(2, 3)*sin(out(1,1)), tac(1,1)*cos(out(1,1))+tac(2,1)*sin(out(1,1)));
    theta = arctan;
    k = sin(out(1,4))*0.093;
    l = -0.093 -cos(out(1,4))*0.093;
    m = cos(out(1,1)) * vec(1,1) + sin(out(1,1)) * vec(1, 2);
    n = cos(out(1,2))*vec(1,3) + sin(out(1,1)) * sin(out(1,2)) * vec(1,1) - cos(out(1,1)) * sin(out(1,2))*vec(1, 2);
    s = (k*n + l*m)/(k.^2 + l.^2);
    c = (n - k*s)/l;
    arctan = atan2(s, c);
    out(1,3) = arctan;
    out(1,5) = theta - out(1,4) -out(1,3);
    out;
end