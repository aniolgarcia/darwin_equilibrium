function dh = denavit_hartenberg(a, d, alpha_rad, theta_rad)
%Returns a Denavit-Hartenberg matrix given its parameters
    dh = [ cos(theta_rad), -sin(theta_rad)*cos(alpha_rad), sin(theta_rad)*sin(alpha_rad), a*cos(theta_rad);
           sin(theta_rad), cos(theta_rad)*cos(alpha_rad), -cos(theta_rad)*sin(alpha_rad), a*sin(theta_rad);
           0, sin(alpha_rad), cos(alpha_rad), d;
           0, 0, 0, 1;
         ];
end
