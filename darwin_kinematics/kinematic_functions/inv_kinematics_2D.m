function angles = inv_kinematics_2D(link1, link2, goal_x, goal_y)
% Calculates the angles of a 2D 2 joint manipulator given the link length and desired
% position of end effector.
    distance = dist_2D(0,0,goal_x,goal_y);
    angles = [atan(goal_x/goal_y)+cos_theorem(link1, distance, link2), cos_theorem(link1,link2,distance)]
end
