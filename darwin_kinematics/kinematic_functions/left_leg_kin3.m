function transformation = left_leg_kin3(rad)
%Test left leg kinematics that work on my inverse kinematics

    %base_to_py = [0 1 0 0.037; 0 0 1 -0.122; 1 0 0 -0.005];
    %base_to_py = denavit_hartenberg(-0.037, 0.122 , pi, rad(1,1));
    
    py_to_pr = denavit_hartenberg(0, 0 , pi/2, rad(1,1)+pi/2);
    pr_to_pp = denavit_hartenberg(0, 0, pi/2, rad(1,2)+pi/2);
    pp_to_k = denavit_hartenberg(-0.093, 0, 0, rad(1,3));
    k_to_fp = denavit_hartenberg(-0.093, 0, 0, rad(1,4)); 
    fp_to_fr = denavit_hartenberg(0, 0, pi/2, rad(1,5));
    fr_to_endeff = denavit_hartenberg(-0.0335, 0, 0, rad(1,6));
    
    trans = [0 0 1 0; 0 1 0 0; -1 0 0 0; 0 0 0 1];
    
    dh_trans = py_to_pr*pr_to_pp*pp_to_k*k_to_fp*fp_to_fr*fr_to_endeff*trans;
    
    x = dh_trans(1, 4);
    y = dh_trans(2, 4);
    z = dh_trans(3, 4);
    translation = [x y z];

    if(dh_trans(3,1) == 1 || dh_trans(3,1) == -1)
        theta_z = 0; 
        i = dh_trans(3,1);
        theta_y = -i*(pi/2);
        theta_x = -i*theta_z *atan2(-i*dh_trans(1,2), -i*dh_trans(1,3));
    else
        theta_y = [-asin(dh_trans(3,1)); (pi)+asin(dh_trans(3,1))];
        theta_x = [atan2(dh_trans(3,2)/cos(theta_y(1,1)), dh_trans(3,3)/cos(theta_y(1,1))); atan2(dh_trans(3,2)/cos(theta_y(2,1)), dh_trans(3,3)/cos(theta_y(2,1)))];
        theta_z = [atan2(dh_trans(2,1)/cos(theta_y(1,1)), dh_trans(1,1)/cos(theta_y(1,1))); atan2(dh_trans(2,1)/cos(theta_y(2,1)), dh_trans(1,1)/cos(theta_y(2,1)))];  
    end
    rotation = [(theta_x) (theta_y) (theta_z)]; %Euler angles from canonical reference frame.
    transformation = [translation, [theta_x(1,1), theta_y(1,1), theta_z(1,1)]];
end