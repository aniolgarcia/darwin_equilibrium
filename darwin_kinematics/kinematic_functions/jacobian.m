function [ J ] = jacobian(FK)
%Returns the jacobian matrix given all the forward kinematics
%transformations
    Z0 = [0; 0; 1]; O = [0; 0; 0]; O7 = FK(1:3, 28);
    J1 = cross(Z0, (O7-O));

    Z1 = (FK(1:3, 3)); O1 = FK(1:3, 4);
    J2 = cross(Z1, (O7-O1));
    
    Z2 = (FK(1:3, 7)); O2 = FK(1:3, 8);
    J3 = cross(Z2, (O7-O2));
    
    Z3 = (FK(1:3, 11)); O3 = FK(1:3, 12);
    J4 = cross(Z3, (O7-O3));
    
    Z4 = (FK(1:3, 15)); O4 = FK(1:3, 16);
    J5 = cross(Z4, (O7-O4));
   
    Z5 = (FK(1:3, 19)); O5 = FK(1:3, 20);
    J6 = cross(Z5, (O7-O5));
    
    Z6 = (FK(1:3, 23)); O6 = FK(1:3, 24);
    J7 = cross(Z6, (O7-O6));
    
    J = [J1 J2 J3 J4 J5 J6 J7];
end


