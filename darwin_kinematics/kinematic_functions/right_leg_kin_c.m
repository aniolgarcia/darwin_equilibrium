function transformation = right_leg_kin_c(rad)
%Left leg direct kinematics according to Darwin c++ library (see darwin_robot_dirver/src/xml/left_leg_kin.xml)
% Gives a very similar result to left_leg_in3, with a slight diference in knee angle and some signs 

    py_to_pr = denavit_hartenberg(0, -0.0315, -pi/2, -rad(1,1)+pi/2);
    pr_to_pp = denavit_hartenberg(0, 0, pi/2, rad(1,2)+pi/2);
    pp_to_k = denavit_hartenberg(0.093, 0, 0, rad(1,3));
    k_to_fp = denavit_hartenberg(0.093, 0, pi, rad(1,4)); 
    fp_to_fr = denavit_hartenberg(0, 0, -pi/2, rad(1,5));
    fr_to_endeff = denavit_hartenberg(0.0335, 0, 0, rad(1,6));

    rotate = [0,0,-1,0;0,1,0,0;1,0,0,0;0,0,0,1];
    dh_trans = py_to_pr*pr_to_pp*pp_to_k*k_to_fp*fp_to_fr*fr_to_endeff;
    dh_trans = dh_trans*rotate;
    x = dh_trans(1, 4);
    y = dh_trans(2, 4);
    z = dh_trans(3, 4);
    translation = [x y z];
    
    if(abs(dh_trans(1, 1)) < 0.001 && abs(dh_trans(2,1)) < 0.001)
        theta_x = atan2(dh_trans(1,2), dh_trans(2,2));
        theta_y = asin(-dh_trans(3,1));
        theta_z = 0;
    else
        theta_x = atan2(dh_trans(3,2), dh_trans(3,3));
        theta_y = atan2(-dh_trans(3,1), sqrt(dh_trans(1,1).^2 + dh_trans(2, 1).^2));
        theta_z = atan2(dh_trans(2, 1), dh_trans(1, 1));
    end
    transformation = [translation, [theta_x, theta_y, theta_z]];
end




