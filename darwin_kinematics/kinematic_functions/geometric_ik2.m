function out = geometric_ik2(x,y,z,a,b,c)
%Geometric inverse_kinematics according to darwin c++ library
    pelvis = 0.0315;
    thigh = 0.093;
    calf = 0.093;
    ankle = 0.0335;
    tad = homogeneous_complete(x, y, z+pelvis, a, b, c);
    vec = [x+tad(1,3)*ankle, y+tad(2,3)*ankle, (z+pelvis)+tad(3,3)*ankle];
    
    %Get knee
    rac = norm(vec);
        
    tmp = (rac.^2 - thigh.^2 - calf.^2)/(2*thigh*calf);
    if(tmp > 1)
        tmp = 1;
    else
        if(tmp < -1)
            tmp = 1;
        end
    end
    out(1, 4) = acos(tmp);
    
    %Get ankle roll
    tda = inv(tad);
    k = sqrt(tda(2, 4).^2 + tda(3, 4).^2);
    l = sqrt(tda(2, 4).^2 + (tda(3, 4) - ankle).^2);
    m = (k.^2 - l.^2 - ankle.^2)/(2*l*ankle);
    if(m > 1)
        m = 1;
    end
    if(m < -1)
        m = -1;
    end
    arccos = acos(m);
    if(tda(2, 4) < 0)
        out(1, 6) = -arccos;
    else
        out(1, 6) = arccos;
    end
    
    %Get hip yaw
    tcd = homogeneous_complete(0, 0, -ankle, out(1, 6), 0, 0);
    tac = tad/tcd;
    
    arctan = atan2(-tac(1, 2), tac(2, 2));
    out(1, 1) = arctan;
    
    %Get hip roll
    arctan = atan2(tac(3, 2), -tac(1, 2)*sin(out(1, 1)) + tac(2, 2)*cos(out(1, 1)));
    out(1, 2) = arctan;
    
    %Get hip pitch and ankle pitch
    arctan = atan2(tac(1, 3)*cos(out(1, 1)) + tac(2, 3)*sin(out(1, 1)), tac(1, 1)*cos(out(1,1)) + tac(2, 1)*sin(out(1,1)));
    theta = arctan;
    k = sin(out(1, 4))*calf;
    l = -thigh - cos(out(1, 4))*calf;
    m = cos(out(1, 1))*vec(1, 1) + sin(out(1, 1))*vec(1, 2);
    n = cos(out(1, 2))*vec(1, 3) + sin(out(1, 1))*sin(out(1, 2))*vec(1, 1) - cos(out(1, 1))*sin(out(1, 2))*vec(1, 2);
    s = (k*n + l*m)/(k.^2 + l.^2);
    c = (n - k*s)/l;
    arctan = atan2(s, c);
    out(1, 3) = arctan;
    out(1, 5) = -(theta - out(1, 4) - out(1, 3));
end
