function [ FK ] = left_leg_transformations(rad)
%Returns an array of matrices containing each one of the partial transformations    
    T01 = [0 1 0 0.037; 0 0 1 -0.122; 1 0 0 -0.005];
    T12 = denavit_hartenberg(0,0,pi/2, rad(1,1)+pi/2);
    T23 = denavit_hartenberg(0,0,pi/2,-rad(1,2)+pi/2);
    T34 = denavit_hartenberg(-0.093,0,0,-rad(1,3));
    T45 = denavit_hartenberg(-0.093,0,0, rad(1,4)); 
    T56 = denavit_hartenberg(0,0,pi/2, rad(1,5));
    T67 = denavit_hartenberg(0,0,0,rad(1,6));
    
    T02 = T01*T12;
    T03 = T02*T23;
    T04 = T03*T34;
    T05 = T04*T45;
    T06 = T05*T56;
    T07 = T06*T67;
    
    FK = [T01 T02 T03 T04 T05 T06 T07];
end