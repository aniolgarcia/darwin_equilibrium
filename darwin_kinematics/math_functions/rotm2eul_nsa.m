%Function to convert rotation matrix to euler angles according to the "NASA Standard Aeroplane" standards.
% By these standards, the euler rotations are applied by the following order: Y, Z, X in a right hand reference system. 
function eul = rotm2eul_new(mat)
  if(mat(3,1) == 1 || mat(3,1) == -1)
    y = atan2d(mat(1,3), mat(3,3));
    z = -1
    x = 0
  else 
    y = atan2d(-mat(3,1), mat(1,1));
    z = asind(mat(2,1));
    x = atan2d(-mat(2,3), mat(2,2));
  end
  eul = [x,y,z]
end