function rotm = axang2rotm(a)
    c = cos(a(4));
    s = sin(a(4));
    t = 1 -c;

    modul = sqrt(a(1).^2 + a(2).^2 + a(3).^2);
    a(1) = a(1)/modul;
    a(2) = a(2)/modul;
    a(3) = a(3)/modul;

    rotm(1,1) = c + a(1).^2 * t;
    rotm(2,2) = c + a(2).^2 * t;
    rotm(3,3) = c + a(3).^2 * t;

    tmp1 = a(1)*a(2)*t;
    tmp2 = a(3)*s;

    rotm(2,1) = tmp1 + tmp2;
    rotm(1,2) = tmp1 - tmp2;

    tmp1 = a(1)*a(3)*t;
    tmp2 = a(2)*s;

    rotm(3,1) = tmp1 - tmp2;
    rotm(1,3) = tmp1 + tmp2;

    tmp2 = a(1)*s;

    rotm(3,2) = tmp1 + tmp2;
    rotm(2,3) = tmp1 - tmp2;
end


		