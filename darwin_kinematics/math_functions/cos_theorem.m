%Given the length of the sides a,b and c of a triangle, calculates the angle oposite to c (in radians).
function gamma = cos_theorem(a,b,c) 
    gamma = acos((a.^2 +b.^2 -c.^2)/(2*a*b));
end

