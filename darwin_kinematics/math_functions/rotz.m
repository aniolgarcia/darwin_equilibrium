function rot = rotz(rad)
    rot = zeros(3, 3);
    rot(1, 1) = cos(rad);
    rot(1, 2) = -sin(rad);
    rot(2, 1) = sin(rad);
    rot(2, 2) = cos(rad);
    rot(3, 3) = 1;
end