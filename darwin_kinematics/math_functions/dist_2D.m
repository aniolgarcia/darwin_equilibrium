% Calculates the distance between two points in the plane
function z = dist_2D(origin_x,origin_y, final_x, final_y)
    z = sqrt((abs(final_x-origin_x)).^2 +(abs(final_y-origin_y)).^2);
end