%Function to convert euler angles to rotation matrix according to the "NASA Standard Aeroplane" standards.
% By these standards, the euler rotations are applied by the following order: Y, Z, X in a right hand reference system. 
function rotm = eul2rotm_nsa(x, y, z)
  rotm = (roty(y*pi/180)*rotz(z*pi/180))*rotx(x*pi/180);
end