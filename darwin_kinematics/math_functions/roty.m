function rot = roty(rad)
    rot = zeros(3, 3);
    rot(1, 1) = cos(rad);
    rot(1, 3) = sin(rad);
    rot(2, 2) = 1;
    rot(3, 1) = -sin(rad);
    rot(3, 3) = cos(rad);
end
