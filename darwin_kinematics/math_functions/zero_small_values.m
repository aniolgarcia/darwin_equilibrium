function out = zero_small_values(vec)
		out = vec;
		for i = 1:size(vec)
				if(i < 0.0001)
						out(1) = 0;
				end
		end
end

		