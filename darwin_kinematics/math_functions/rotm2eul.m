function ang = rotm2eul(rotm)
     %ang(1) = atan2(rotm(3,2), rotm(3,3));
     %ang(2) = atan2(-rotm(3,1), sqrt(rotm(3,2).^2 + rotm(3,3).^2);
     %ang(3) = atan2(rotm(2,1), rotm(1,1));
		 
	if(rotm(3,1) == 1 || rotm(3,1) == -1)
        theta_z = 0; 
        i = rotm(3,1);
        theta_y = -i*(pi/2);
        theta_x = -i*theta_z *atan2(-i*rotm(1,2), -i*rotm(1,3));
    else
        theta_y = [-asin(rotm(3,1)); (pi)+asin(rotm(3,1))];
        theta_x = [atan2(rotm(3,2)/cos(theta_y(1,1)), rotm(3,3)/cos(theta_y(1,1))); atan2(rotm(3,2)/cos(theta_y(2,1)), rotm(3,3)/cos(theta_y(2,1)))];
        theta_z = [atan2(rotm(2,1)/cos(theta_y(1,1)), rotm(1,1)/cos(theta_y(1,1))); atan2(rotm(2,1)/cos(theta_y(2,1)), rotm(1,1)/cos(theta_y(2,1)))];  
    end
    ang = [(theta_x) (theta_y) (theta_z)]; 
 end
 