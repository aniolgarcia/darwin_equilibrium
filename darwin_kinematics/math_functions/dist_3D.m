% calculates the distance between two points in space
function z = dist_3D(o_x, o_y, o_z, f_x, f_y, f_z)
    z = sqrt((f_x - o_x).^2 + (f_y - o_y).^2 + (f_z - o_z).^2);
end