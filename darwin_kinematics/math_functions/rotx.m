function rot = rotx(rad)
    rot = zeros(3, 3);
    rot(1, 1) = 1;
    rot(2, 2) = cos(rad);
    rot(2, 3) = -sin(rad);
    rot(3, 2) = sin(rad);
    rot(3, 3) = cos(rad);
end