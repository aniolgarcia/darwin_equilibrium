%Returns a homogeneous transformation matrix given a rotation matrix and
% the three components of the transposition

function trans = homogeneous(rot, x, y, z)
    trans = zeros(4, 4);
    trans(1:3, 1:3) = rot;
    trans(4, 1:3) = zeros(1, 3); 
    trans(1, 4) = x;
    trans(2, 4) = y;
    trans(3, 4) = z;
    trans(4, 4) = 1;
end

