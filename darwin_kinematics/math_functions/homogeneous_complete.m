%Returns a homogeneous transformation matrix given position and
%  orientation (in radians)
function trans = homogeneous_complete(x, y, z, a, b, c)
    trans = zeros(4, 4);
    trans(1, 1) = cos(c)*cos(b);
    trans(1 ,2) = cos(c)*sin(b)*sin(a) - sin(c)*cos(a);
    trans(1, 3) = cos(c)*sin(b)*cos(a) + sin(c)*sin(a);
    trans(1, 4) = x;
    trans(2, 1) = sin(c)*cos(b);
    trans(2, 2) = sin(c)*sin(b)*sin(a) + cos(c)*cos(a);
    trans(2, 3) = sin(c)*sin(b)*cos(a) - cos(c)*sin(a);
    trans(2, 4) = y;
    trans(3, 1) = -sin(b);
    trans(3, 2) = cos(b)*sin(a);
    trans(3, 3) = cos(b)*cos(a);
    trans(3, 4) = z;
    trans(4, 4) = 1;
end
