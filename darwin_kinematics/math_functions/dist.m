% Calculates the distance between two points in a cartesian n-space.
function z = dist(p1, p2)
    z = sqrt(sum((p1-p2).^2));
end

