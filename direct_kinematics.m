1;

%% Definition of joint angles
fr = rad2deg(0);
fp = rad2deg(0);
k = rad2deg(0);
pp = rad2deg(0);
pr = rad2deg(0);
py = rad2deg(1.58);

%% Direct transformations using simulation reference frames

fr_fp = homogeneous(rotz(fr),0,0,0);
fp_k = homogeneous(rotx(fp),0,-0.093,0);
k_pp = homogeneous(rotx(k),0,-0.093,0);
pp_pr = homogeneous(rotx(pp),0,0,0);
pr_py = homogeneous(rotz(pr),0,0,0);
py_base = homogeneous(roty(py),0.037,-0.122,0);

sim_transformation = pr_py*pp_pr*k_pp*fp_k*fr_fp;

%% Main script
start_position = [0,0,0.1,0,0,0];

%start_position = left_leg_kin([0, 0, 0, 0,0,0]) %Cinemàtica directa donats
%els valors dels angles

inverse_angles = geometric_ik(start_position(1,1), start_position(1,2), start_position(1,3), start_position(1,4), start_position(1,5), start_position(1,6)) %Cinemàtica inversa donada posició i orientació
final_position = left_leg_kin(inverse_angles) %Cinemàtica directa donats els angles calculats per la cinemàtica inversa%% Function definition
left_leg_kin3([0,0,-1.0901, 2.1803, -1.0901, 0])


%% Functions
%Rotation matrix functions given the angle in radians

function rot = rotx(rad)
    rot = zeros(3, 3);
    rot(1, 1) = 1;
    rot(2, 2) = cos(rad);
    rot(2, 3) = -sin(rad);
    rot(3, 2) = sin(rad);
    rot(3, 3) = cos(rad);
end

function rot = roty(rad)
    rot = zeros(3, 3);
    rot(1, 1) = cos(rad);
    rot(1, 3) = sin(rad);
    rot(2, 2) = 1;
    rot(3, 1) = -sin(rad);
    rot(3, 3) = cos(rad);
end

function rot = rotz(rad)
    rot = zeros(3, 3);
    rot(1, 1) = cos(rad);
    rot(1, 2) = -sin(rad);
    rot(2, 1) = sin(rad);
    rot(2, 2) = cos(rad);
    rot(3, 3) = 1;
end

%Returns a homogeneous transformation matrix given a rotation matrix and
% the three components of the transposition

function trans = homogeneous(rot, x, y, z)
    trans = zeros(4, 4);
    trans(1:3, 1:3) = rot;
    trans(4, 1:3) = zeros(1, 3); 
    trans(1, 4) = x;
    trans(2, 4) = y;
    trans(3, 4) = z;
    trans(4, 4) = 1;
end

%Returns a homogeneous transformation matrix given position and
%  orientation (in radians)
function trans = homogeneous_complete(x, y, z, a, b, c)
    trans = zeros(4, 4);
    trans(1, 1) = cos(c)*cos(b);
    trans(1 ,2) = cos(c)*sin(b)*sin(a) - sin(c)*cos(a);
    trans(1, 3) = cos(c)*sin(b)*cos(a) + sin(c)*sin(a);
    trans(1, 4) = x;
    trans(2, 1) = sin(c)*cos(b);
    trans(2, 2) = sin(c)*sin(b)*sin(a) + cos(c)*cos(a);
    trans(2, 3) = sin(c)*sin(b)*cos(a) - cos(c)*sin(a);
    trans(2, 4) = y;
    trans(3, 1) = -sin(b);
    trans(3, 2) = cos(b)*sin(a);
    trans(3, 3) = cos(b)*cos(a);
    trans(3, 4) = z;
    trans(4, 4) = 1;
end

%Returns a Denavit-Hartenberg matrix given its parameters
function dh = denavit_hartenberg(a, d, alpha_rad, theta_rad)
    dh = [ cos(theta_rad), -sin(theta_rad)*cos(alpha_rad), sin(theta_rad)*sin(alpha_rad), a*cos(theta_rad);
           sin(theta_rad), cos(theta_rad)*cos(alpha_rad), -cos(theta_rad)*sin(alpha_rad), a*sin(theta_rad);
           0, sin(alpha_rad), cos(alpha_rad), d;
           0, 0, 0, 1;
         ];
end

%Given the length of the sides a,b and c of a triangle, calculates the angle oposite to c (in radians).
function gamma = cos_theorem(a,b,c) 
    gamma = acos((a.^2 +b.^2 -c.^2)/(2*a*b));
end

% Calculates the distance between two points in the plane
function z = dist_2D(origin_x,origin_y, final_x, final_y)
    z = sqrt((abs(final_x-origin_x)).^2 +(abs(final_y-origin_y)).^2);
end

% calculates the distance between two points in space
function z = dist_3D(o_x, o_y, o_z, f_x, f_y, f_z)
    z = sqrt((f_x - o_x).^2 + (f_y - o_y).^2 + (f_z - o_z).^2);
end

% Calculates the distance between two points in a cartesian n-space.
function z = dist(p1, p2)
    z = sqrt(sum((p1-p2).^2));
end
  
% Calculates the angles of a 2D 2 joint manipulator given the link length and desired
% position of end effector.
function angles = inv_kinematics(link1, link2, goal_x, goal_y)
    distance = dist_2D(0,0,goal_x,goal_y);
    angles = [atan(goal_x/goal_y)+cos_theorem(link1, distance, link2), cos_theorem(link1,link2,distance)]
end

function transformation = left_leg_kin(rad)

    %base_to_py = [0 1 0 0.037; 0 0 1 -0.122; 1 0 0 -0.005];
    py_to_pr = denavit_hartenberg(0,0,pi/2, rad(1,1)+pi/2);
    pr_to_pp = denavit_hartenberg(0,0,pi/2,-rad(1,2)+pi/2);
    pp_to_k = denavit_hartenberg(-0.093,0,0,-rad(1,3));
    k_to_fp = denavit_hartenberg(-0.093,0,0, -rad(1,4)); 
    fp_to_fr = denavit_hartenberg(0,0,pi/2, rad(1,5));
    fr_to_endeff = denavit_hartenberg(-0.0335,0,0,rad(1,6));

    dh_trans = py_to_pr*pr_to_pp*pp_to_k*k_to_fp*fp_to_fr*fr_to_endeff;
    x = dh_trans(1, 4);
    y = dh_trans(2, 4);
    z = dh_trans(3, 4);
    translation = [x y z];

    if(dh_trans(3,1) == 1 || dh_trans(3,1) == -1)
        theta_z = 0; 
        i = dh_trans(3,1);
        theta_y = -i*(pi/2);
        theta_x = -i*theta_z *atan2(-i*dh_trans(1,2), -i*dh_trans(1,3));
    else
        theta_y = [-asin(dh_trans(3,1)); (pi)+asin(dh_trans(3,1))];
        theta_x = [atan2(dh_trans(3,2)/cos(theta_y(1,1)), dh_trans(3,3)/cos(theta_y(1,1))); atan2(dh_trans(3,2)/cos(theta_y(2,1)), dh_trans(3,3)/cos(theta_y(2,1)))];
        theta_z = [atan2(dh_trans(2,1)/cos(theta_y(1,1)), dh_trans(1,1)/cos(theta_y(1,1))); atan2(dh_trans(2,1)/cos(theta_y(2,1)), dh_trans(1,1)/cos(theta_y(2,1)))];  
    
    end
    rotation = [(theta_x) (theta_y) (theta_z)]; %Euler angles from canonical reference frame.
    transformation = [translation, [theta_x(1,1), theta_y(1,1), theta_z(1,1)]];
end
function transformation = left_leg_kin3(rad)

    %base_to_py = [0 1 0 0.037; 0 0 1 -0.122; 1 0 0 -0.005];
    center_to_base = denavit_hartenberg(-0.005, 0, 0, -pi/2);  
    base_to_py = denavit_hartenberg(-0.037, 0.122 , pi, rad(1,1));
    py_to_pr = denavit_hartenberg(0, 0 , pi/2, rad(1,2));
    pr_to_pp = denavit_hartenberg(0, 0, pi/2, rad(1,3));
    pp_to_k = denavit_hartenberg(-0.093, 0, 0, rad(1,4));
    k_to_fp = denavit_hartenberg(-0.093, 0, 0, rad(1,5)); 
    fp_to_fr = denavit_hartenberg(0, 0, pi/2, rad(1,6));
    fr_to_endeff = denavit_hartenberg(-0.0335, 0, pi, 0);

    dh_trans = py_to_pr*pr_to_pp*pp_to_k*k_to_fp*fp_to_fr*fr_to_endeff;
    x = dh_trans(1, 4);
    y = dh_trans(2, 4);
    z = dh_trans(3, 4);
    translation = [x y z];

    if(dh_trans(3,1) == 1 || dh_trans(3,1) == -1)
        theta_z = 0; 
        i = dh_trans(3,1);
        theta_y = -i*(pi/2);
        theta_x = -i*theta_z *atan2(-i*dh_trans(1,2), -i*dh_trans(1,3));
    else
        theta_y = [-asin(dh_trans(3,1)); (pi)+asin(dh_trans(3,1))];
        theta_x = [atan2(dh_trans(3,2)/cos(theta_y(1,1)), dh_trans(3,3)/cos(theta_y(1,1))); atan2(dh_trans(3,2)/cos(theta_y(2,1)), dh_trans(3,3)/cos(theta_y(2,1)))];
        theta_z = [atan2(dh_trans(2,1)/cos(theta_y(1,1)), dh_trans(1,1)/cos(theta_y(1,1))); atan2(dh_trans(2,1)/cos(theta_y(2,1)), dh_trans(1,1)/cos(theta_y(2,1)))];  
    
    end
    rotation = [(theta_x) (theta_y) (theta_z)]; %Euler angles from canonical reference frame.
    transformation = [translation, [theta_x(1,1), theta_y(1,1), theta_z(1,1)]];
end


function [ FK ] = left_leg_transformations(rad)
    
    T01 = [0 1 0 0.037; 0 0 1 -0.122; 1 0 0 -0.005];
    T12 = denavit_hartenberg(0,0,pi/2, rad(1,1)+pi/2);
    T23 = denavit_hartenberg(0,0,pi/2,-rad(1,2)+pi/2);
    T34 = denavit_hartenberg(-0.093,0,0,-rad(1,3));
    T45 = denavit_hartenberg(-0.093,0,0, rad(1,4)); 
    T56 = denavit_hartenberg(0,0,pi/2, rad(1,5));
    T67 = denavit_hartenberg(0,0,0,rad(1,6));
    
    T02 = T01*T12;
    T03 = T02*T23;
    T04 = T03*T34;
    T05 = T04*T45;
    T06 = T05*T56;
    T07 = T06*T67;
    
    FK = [T01 T02 T03 T04 T05 T06 T07];
end

function [ J ] = jacobian(FK)
    Z0 = [0; 0; 1]; O = [0; 0; 0]; O7 = FK(1:3, 28);
    J1 = cross(Z0, (O7-O));

    Z1 = (FK(1:3, 3)); O1 = FK(1:3, 4);
    J2 = cross(Z1, (O7-O1));
    
    Z2 = (FK(1:3, 7)); O2 = FK(1:3, 8);
    J3 = cross(Z2, (O7-O2));
    
    Z3 = (FK(1:3, 11)); O3 = FK(1:3, 12);
    J4 = cross(Z3, (O7-O3));
    
    Z4 = (FK(1:3, 15)); O4 = FK(1:3, 16);
    J5 = cross(Z4, (O7-O4));
   
    Z5 = (FK(1:3, 19)); O5 = FK(1:3, 20);
    J6 = cross(Z5, (O7-O5));
    
    Z6 = (FK(1:3, 23)); O6 = FK(1:3, 24);
    J7 = cross(Z6, (O7-O6));
    
    J = [J1 J2 J3 J4 J5 J6 J7];
end

function [ velo ] = divelo(pos_start,pos_target)
    dXYZ=pos_target-pos_start;
    dX=dXYZ(1,1);
    dY=dXYZ(2,1);
    dZ=dXYZ(3,1);
    EucXY=sqrt(dX^2+dY^2);
    EucXYZ=sqrt(EucXY^2+dZ^2);
    XYang=atan2d(dY,dX);
    Zang=atan2d(dZ,EucXY);
    dx=cosd(XYang)*EucXY;
    dy=sind(XYang)*EucXY;
    dz=sind(Zang)*EucXYZ;
    velo=[dx;dy;dz;EucXY;EucXYZ;Zang];
end


function j = create_jacobian()
    j = zeros(3,6);
    vect = zeros(1,6);
    vect(1,1) = 0.05;
    
    for i=1:6
        j(1:3,i)= (left_leg_kin(vect)-left_leg_kin(zeros(1,6)))/0.05;
        vect = circshift(vect, 1);
    end
end

function out = geometric_ik(x, y, z, a, b,  c)
    tad = homogeneous_complete(x, y, z-0.2195, a, b, c);
    
    vec = [x+tad(1,3)*0.0335 y+tad(2, 3)*0.0335 (z-0.2195)+tad(3,3)*0.0335];
    
    %Get knee angle
    rac = sqrt(vec(1,1).^2 + vec(1,2).^2 + vec(1,3).^2);
    arccos = acos(((rac.^2) -2*(0.093.^2))/(2*(0.093.^2)));
    out(1, 4) = arccos;
    
    %Get ankle roll
    tda = inv(tad);
    k = sqrt(tda(2, 4).^2 + tda(3, 4).^2);
    l = sqrt(tda(2, 4).^2 + (tda(3,4)-0.0335).^2);
    m = (k.^2 - l.^2 - 0.0335.^2)/(2*l*0.0335);
    if(m > 1)
        m = 1;
    end
    if(m < -1)
        m = -1;
    end
    arccos = acos(m);
    if(tda(2, 4) < 0)
        out(1, 6) = -arccos;
    else
        out(1, 6) = arccos;
    end
    
    %Get hip yaw
    tcd = homogeneous_complete(0, 0, -0.0335, out(1,6), 0, 0);
    tac = tad/tcd;
    arctan = atan2(-tac(1,2), tac(2,2));
    out(1, 1) = arctan;
    
    %Get hip roll
    arctan = atan2(tac(3,2), - tac(1,2)*sin(out(1,1))+tac(2,2)*cos(out(1,1)));
    out(1, 2) = arctan;
    
    %Get hip pitch and ankle pitch
    arctan = atan2(tac(1,3)*cos(out(1,1)) +tac(2, 3)*sin(out(1,1)), tac(1,1)*cos(out(1,1))+tac(2,1)*sin(out(1,1)));
    theta = arctan;
    k = sin(out(1,4))*0.093;
    l = -0.093 -cos(out(1,4))*0.093;
    m = cos(out(1,1)) * vec(1,1) + sin(out(1,1)) * vec(1, 2);
    n = cos(out(1,2))*vec(1,3) + sin(out(1,1)) * sin(out(1,2)) * vec(1,1) - cos(out(1,1)) * sin(out(1,2))*vec(1, 2);
    s = (k*n + l*m)/(k.^2 + l.^2);
    c = (n - k*s)/l;
    arctan = atan2(s, c);
    out(1,3) = arctan;
    out(1,5) = theta - out(1,4) -out(1,3);
    out;
end

function out = geometric_ik2(x,y,z,a,b,c)
    pelvis = 0.0315;
    thigh = 0.093;
    calf = 0.093;
    ankle = 0.0335;
    tad = homogeneous_complete(x, y, z+pelvis, a, b, c);
    vec = [x+tad(1,3)*ankle, y+tad(2,3)*ankle, (z+pelvis)+tad(3,3)*ankle];
    
    %Get knee
    rac = norm(vec);
        
    tmp = (rac.^2 - thigh.^2 - calf.^2)/(2*thigh*calf);
    if(tmp > 1)
        tmp = 1;
    else
        if(tmp < -1)
            tmp = 1;
        end
    end
    out(1, 4) = acos(tmp);
    
    %Get ankle roll
    tda = inv(tad);
    k = sqrt(tda(2, 4).^2 + tda(3, 4).^2);
    l = sqrt(tda(2, 4).^2 + (tda(3, 4) - ankle).^2);
    m = (k.^2 - l.^2 - ankle.^2)/(2*l*ankle);
    if(m > 1)
        m = 1;
    end
    if(m < -1)
        m = -1;
    end
    arccos = acos(m);
    if(tda(2, 4) < 0)
        out(1, 6) = -arccos;
    else
        out(1, 6) = arccos;
    end
    
    %Get hip yaw
    tcd = homogeneous_complete(0, 0, -ankle, out(1, 6), 0, 0);
    tac = tad/tcd;
    
    arctan = atan2(-tac(1, 2), tac(2, 2));
    out(1, 1) = arctan;
    
    %Get hip roll
    arctan = atan2(tac(3, 2), -tac(1, 2)*sin(out(1, 1)) + tac(2, 2)*cos(out(1, 1)));
    out(1, 2) = arctan;
    
    %Get hip pitch and ankle pitch
    arctan = atan2(tac(1, 3)*cos(out(1, 1)) + tac(2, 3)*sin(out(1, 1)), tac(1, 1)*cos(out(1,1)) + tac(2, 1)*sin(out(1,1)));
    theta = arctan;
    k = sin(out(1, 4))*calf;
    l = -thigh - cos(out(1, 4))*calf;
    m = cos(out(1, 1))*vec(1, 1) + sin(out(1, 1))*vec(1, 2);
    n = cos(out(1, 2))*vec(1, 3) + sin(out(1, 1))*sin(out(1, 2))*vec(1, 1) - cos(out(1, 1))*sin(out(1, 2))*vec(1, 2);
    s = (k*n + l*m)/(k.^2 + l.^2);
    c = (n - k*s)/l;
    arctan = atan2(s, c);
    out(1, 3) = arctan;
    out(1, 5) = -(theta - out(1, 4) - out(1, 3));
end



function transformation = left_leg_kin2(rad)
    py_to_pr = denavit_hartenberg(0, -0.0315, -pi/2, -rad(1,1)+pi/2);
    pr_to_pp = denavit_hartenberg(0, 0, -pi/2, rad(1,2)+pi/2);
    pp_to_k = denavit_hartenberg(0.093, 0, 0, rad(1,3));
    k_to_fp = denavit_hartenberg(0.093, 0, pi, rad(1,4)); 
    fp_to_fr = denavit_hartenberg(0, 0, pi/2, rad(1,5));
    fr_to_endeff = denavit_hartenberg(0.0335, 0, 0, rad(1,6));

    rotate = [0,0,-1,0;0,1,0,0;1,0,0,0;0,0,0,1];
    dh_trans = py_to_pr*pr_to_pp*pp_to_k*k_to_fp*fp_to_fr*fr_to_endeff;
    dh_trans = dh_trans*rotate;
    x = dh_trans(1, 4);
    y = dh_trans(2, 4);
    z = dh_trans(3, 4);
    translation = [x y z];
    
    if(abs(dh_trans(1, 1)) < 0.001 && abs(dh_trans(2,1)) < 0.001)
        theta_x = atan2(dh_trans(1,2), dh_trans(2,2));
        theta_y = asin(-dh_trans(3,1));
        theta_z = 0;
    else
        theta_x = atan2(dh_trans(3,2), dh_trans(3,3));
        theta_y = atan2(-dh_trans(3,1), sqrt(dh_trans(1,1).^2 + dh_trans(2, 1).^2));
        theta_z = atan2(dh_trans(2, 1), dh_trans(1, 1));
    end
    transformation = [translation, [theta_x, theta_y, theta_z]];
end



